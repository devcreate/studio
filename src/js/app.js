import './modules/sitePreloader';
import documentReady from './utils/documentReady';
import documentLoaded from './utils/documentLoaded';
import jqueryValidate from './utils/jqueryValidate';

import cssVars from './modules/cssVars';
import resize from './modules/resize';
import lazyload from './modules/lazyload';
import menu from './modules/menu';
import scrollTo from './modules/scrollTo';
import servicesSlider from './modules/servicesSlider';
import servicesFancybox from './modules/servicesFancybox';
import map from './modules/map';
import reviewsSlider from './modules/reviewsSlider';
import overlay from './modules/overlay';
import subscribe from './modules/subscribe';
import blogSeeMore from './modules/blogSeeMore';





documentReady(() => {
  cssVars.init();
  resize.init();
  lazyload.init();
  menu.init();
  scrollTo.init();
  jqueryValidate.init();





});

documentLoaded(() => {

});

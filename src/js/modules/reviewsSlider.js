// import Swiper bundle with all modules installed
import Swiper from 'swiper/bundle';


var swiper = new Swiper(".s-reviews__swiper", {
  slidesPerView: 3,
  spaceBetween: 30,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
    },
    590: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 3,
    }
  },
});

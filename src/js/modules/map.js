document.addEventListener('click', function(e) {
  var map = document.querySelector('#s-map_wrap iframe')
  if(e.target.id === 's-map_wrap') {
    map.style.pointerEvents = 'all';
  } else {
    map.style.pointerEvents = 'none';
  }
});

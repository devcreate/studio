$(function () {
  $('.s-blog__button').click(function () {
    $('.s-blog__items .s-blog__item:hidden').slice(0, 3).show().css('display', 'flex');
    if ($('.s-blog__items .s-blog__item').length == $('.s-blog__items .s-blog__item:visible').length) {
      $('.s-blog__button').hide();
    }
  });
});


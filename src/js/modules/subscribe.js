
function initSubscribeValidate(){

  $('#subscribe__form').validate({
    submitHandler: function() {
      let subscribeName = $('#subscribe__name').val();
      let subscribeEmail = $('#subscribe__email').val();
      let subscribeNumber = $('#subscribe__number').val();
      let subscribeTextarea = $('#subscribe__textarea').val();
      let overlayClose = $('#overlay');
      console.log(subscribeName);
      console.log(subscribeEmail);
      console.log(subscribeNumber);
      console.log(subscribeTextarea);
      alert("Thanks. The application is accepted. Our manager will contact you shortly.");
      overlayClose.style.display='none';
    },
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: "Please enter a valid email address"
    }
  });

  let inp = document.querySelector('#subscribe__number');
  inp.addEventListener('keypress', e => {
    // Отменяем ввод не цифр
    if(!/\d/.test(e.key))
      e.preventDefault();
  });
}

$(document).ready(function(){
  initSubscribeValidate();
});

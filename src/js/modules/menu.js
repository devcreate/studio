import throttle from '../utils/throttle';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import { lowDesktopMin } from './media';
import dispatcher from './dispatcher';

export default {
  classes: {
    menuOpen: '_site-menu-open',
  },
  el: {
    body: $('body'),
    siteMenuContainer: $('.site-menu__container'),
  },
  init() {
    this.handlers();
    this.handleDispatcher();
  },
  handlers() {
    const self = this;
    const { classes, el } = self;

    $(document)
      .on('click', '.burger', throttle(500, false, () => {
        el.body.hasClass(classes.menuOpen) ? self.close() : self.open();
      }))
      .on('click', '.site-menu__item', throttle(500, false, () => {
        el.body.hasClass(classes.menuOpen) ? self.close() : self.open();
      }))
      .on('click', '.site-menu__bg', throttle(500, false, () => {
        self.close();
      }))
      .on('click', '.site-menu__mobile-button', throttle(500, false, () => {
        self.close();
      }));
  },
  open() {
    this.el.body.addClass(this.classes.menuOpen);
  },
  close() {
    this.el.body.removeClass(this.classes.menuOpen);
  },
  handleDispatcher() {
    dispatcher.subscribe(({ type }) => {
      if (type === 'site-menu:open') {
        this.open();
      }

      if (type === 'site-menu:close') {
        this.close();
      }

      if ((type === 'resize:width') && (lowDesktopMin())) {
        this.close();
      }
    });
  },
};

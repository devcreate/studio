// import Swiper bundle with all modules installed
import Swiper from 'swiper/bundle';

let swiper = new Swiper('.mySwiper', {
  spaceBetween: 0,
  slidesPerView: 2,
  freeMode: true,
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
  slidesPerColumn: 2,
  observer: true,
  observeParents: true,
  observeSlideChildren: true,

  breakpoints: {
    320: {
      slidesPerView: 2,
      slidesPerColumn: 1,
    },
    360: {
      slidesPerView: 3,
      slidesPerColumn: 1,
    },
    490: {
      slidesPerView: 4,
      slidesPerColumn: 1,
    },
    768: {
      slidesPerColumn: 2,
    }
  },

  navigation: {
    nextEl: '.s-services__nav-right',
    prevEl: '.s-services__nav-left',
  },
  thumbs: {
    swiper: swiper,
  },

});
let swiper2 = new Swiper('.mySwiper2', {
  spaceBetween: 10,
  observer:true,
  observeParents: true,
  observeSlideChildren: true,

  navigation: {
    nextEl: '.s-services__mobile-button-next',
    prevEl: '.s-services__mobile-button-prev',
  },
  thumbs: {
    swiper: swiper,
  },
});

$('.s-services__direction span').on('click', function () {
  let filter = $(this).html().toLowerCase();
  let slidesxcol;
  $('.s-services__direction span').removeClass('s-services_direction-active');
  $(this).addClass('s-services_direction-active');
  if (filter == 'all') {

    $('[data-filter]')
      .removeClass('non-swiper-slide')
      .addClass('swiper-slide')
      .show();
    if($('.swiper-slide').length > 2)
      slidesxcol = 2;
    else slidesxcol = 1;
    swiper.destroy();
    swiper = new Swiper('.mySwiper', {
      slidesPerView: 2,
      slidesPerColumn:2,
      slidesPerColumn: slidesxcol,
      paginationClickable: true,
      spaceBetween: 0,
      breakpoints: {
        320: {
          slidesPerView: 2,
          slidesPerColumn:1,
        },
        360: {
          slidesPerView: 3,
          slidesPerColumn:1,
        },
        490: {
          slidesPerView: 4,
          slidesPerColumn:1,
        },
        768: {
          slidesPerColumn:slidesxcol,
        }
      },
      navigation: {
        nextEl: '.s-services__nav-right',
        prevEl: '.s-services__nav-left',
      }
    });
    swiper2.destroy();
    swiper2 = new Swiper('.mySwiper2', {
      spaceBetween: 10,
      thumbs: {
        swiper: swiper,
      },
      navigation: {
        nextEl: '.s-services__mobile-button-next',
        prevEl: '.s-services__mobile-button-prev',
      },
    });
    /*nav-box-hide*/
    if ($('.swiper-wrapper-thumbs .swiper-slide').length === 0) {
      $('.s-services__nav-box').addClass('s-services_nav-box-hide');
    } else if ($('.swiper-wrapper-thumbs .swiper-slide').length < 2) {
      $('.s-services__nav-box')
        .addClass('s-services_nav-box-mt')
        .removeClass('s-services_nav-box-hide');
    } else {
      $('.s-services__nav-box')
        .removeClass('s-services_nav-box-mt')
        .removeClass('s-services_nav-box-hide');
    }
    /*nav-box-hide*/
  }
  else {

    $('.s-services-swiper-slide')
      .not("[data-filter='"+filter+"']")
      .addClass('non-swiper-slide')
      .removeClass('swiper-slide')
      .hide();
    $("[data-filter='"+filter+"']")
      .removeClass('non-swiper-slide')
      .addClass('swiper-slide')
      .attr('style', null)
      .show();
    if($('.swiper-slide').length > 2)
      slidesxcol = 2;
    else slidesxcol = 1;
    swiper.destroy();
    swiper = new Swiper('.mySwiper', {
      slidesPerView: 2,
      slidesPerColumn: slidesxcol,
      paginationClickable: true,
      spaceBetween: 0,
      breakpoints: {
        320: {
          slidesPerView: 2,
          slidesPerColumn: 1,
        },
        360: {
          slidesPerView: 3,
          slidesPerColumn: 1,
        },
        490: {
          slidesPerView: 4,
          slidesPerColumn: 1,
        },
        768: {
          slidesPerColumn: slidesxcol,
        }
      },
      navigation: {
        nextEl: '.s-services__nav-right',
        prevEl: '.s-services__nav-left',
      }
    });
    swiper2.destroy();
    swiper2 = new Swiper('.mySwiper2', {
      spaceBetween: 10,
      thumbs: {
        swiper: swiper,
      },
      navigation: {
        nextEl: ".s-services__mobile-button-next",
        prevEl: ".s-services__mobile-button-prev",
      },
    });
    /*nav-box-hide*/
    if ($('.swiper-wrapper-thumbs .swiper-slide').length === 0) {
      $('.s-services__nav-box').addClass('s-services_nav-box-hide');
    }
    else if ($('.swiper-wrapper-thumbs .swiper-slide').length < 2) {
      $('.s-services__nav-box')
        .addClass('s-services_nav-box-mt')
        .removeClass('s-services_nav-box-hide');
    } else {
      $('.s-services__nav-box')
        .removeClass('s-services_nav-box-mt')
        .removeClass('s-services_nav-box-hide');
    }
    /*nav-box-hide*/
  }
})
$('.s-services__fancybox-link').on('click', function () {
  let items = document.getElementsByClassName('s-services__fancybox-link');
  let activeAll = $('#s-services_direction-active').hasClass('s-services_direction-active');
  if (activeAll) {
    for (let i = 0; i < items.length; i++) {
      items[i].dataset.fancybox = 'all';
    }
  } else {
    for (let i = 0; i < items.length; i++) {
      items[i].dataset.fancybox = items[i].rel;
    }
  }
})





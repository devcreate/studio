const width = {
  wideDesktop: 1920,
  desktop: 1280,
  lowDesktop: 1024,
  tablet: 768,
};

function wideDesktopMin() {
  return window.matchMedia(`(min-width: ${ width.wideDesktop }px)`).matches;
}

function lowDesktopMin() {
  return window.matchMedia(`(min-width: ${ width.lowDesktop }px)`).matches;
}

function tablet() {
  return window.matchMedia(`(max-width: ${ width.lowDesktop - 1 }px)`).matches;
}

function mobile() {
  return window.matchMedia(`(max-width: ${ width.tablet - 1 }px)`).matches;
}

export {
  width as screenWidth,
  wideDesktopMin,
  lowDesktopMin,
  tablet,
  mobile,
};

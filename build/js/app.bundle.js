/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"app": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "js/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app.js":
/*!****************!*\
  !*** ./app.js ***!
  \****************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_sitePreloader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/sitePreloader */ "./modules/sitePreloader.js");
/* harmony import */ var _utils_documentReady__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils/documentReady */ "./utils/documentReady.js");
/* harmony import */ var _utils_documentLoaded__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/documentLoaded */ "./utils/documentLoaded.js");
/* harmony import */ var _utils_jqueryValidate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utils/jqueryValidate */ "./utils/jqueryValidate.js");
/* harmony import */ var _modules_cssVars__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/cssVars */ "./modules/cssVars/index.js");
/* harmony import */ var _modules_resize__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/resize */ "./modules/resize.js");
/* harmony import */ var _modules_lazyload__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/lazyload */ "./modules/lazyload.js");
/* harmony import */ var _modules_menu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modules/menu */ "./modules/menu.js");
/* harmony import */ var _modules_scrollTo__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./modules/scrollTo */ "./modules/scrollTo.js");
/* harmony import */ var _modules_servicesSlider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./modules/servicesSlider */ "./modules/servicesSlider.js");
/* harmony import */ var _modules_servicesFancybox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./modules/servicesFancybox */ "./modules/servicesFancybox.js");
/* harmony import */ var _modules_servicesFancybox__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_modules_servicesFancybox__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _modules_map__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./modules/map */ "./modules/map.js");
/* harmony import */ var _modules_map__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_modules_map__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _modules_reviewsSlider__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./modules/reviewsSlider */ "./modules/reviewsSlider.js");
/* harmony import */ var _modules_overlay__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./modules/overlay */ "./modules/overlay.js");
/* harmony import */ var _modules_overlay__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_modules_overlay__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _modules_subscribe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./modules/subscribe */ "./modules/subscribe.js");
/* harmony import */ var _modules_subscribe__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_modules_subscribe__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _modules_blogSeeMore__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./modules/blogSeeMore */ "./modules/blogSeeMore.js");
















Object(_utils_documentReady__WEBPACK_IMPORTED_MODULE_1__["default"])(function () {
  _modules_cssVars__WEBPACK_IMPORTED_MODULE_4__["default"].init();
  _modules_resize__WEBPACK_IMPORTED_MODULE_5__["default"].init();
  _modules_lazyload__WEBPACK_IMPORTED_MODULE_6__["default"].init();
  _modules_menu__WEBPACK_IMPORTED_MODULE_7__["default"].init();
  _modules_scrollTo__WEBPACK_IMPORTED_MODULE_8__["default"].init();
  _utils_jqueryValidate__WEBPACK_IMPORTED_MODULE_3__["default"].init();
});
Object(_utils_documentLoaded__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {});

/***/ }),

/***/ "./modules/blogSeeMore.js":
/*!********************************!*\
  !*** ./modules/blogSeeMore.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "../../node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_0__);

$(function () {
  $('.s-blog__button').click(function () {
    $('.s-blog__items .s-blog__item:hidden').slice(0, 3).show().css('display', 'flex');

    if ($('.s-blog__items .s-blog__item').length == $('.s-blog__items .s-blog__item:visible').length) {
      $('.s-blog__button').hide();
    }
  });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js")))

/***/ }),

/***/ "./modules/cssVars/index.js":
/*!**********************************!*\
  !*** ./modules/cssVars/index.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vh__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vh */ "./modules/cssVars/vh.js");
/* harmony import */ var _scrollWidth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scrollWidth */ "./modules/cssVars/scrollWidth.js");


/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    _vh__WEBPACK_IMPORTED_MODULE_0__["default"].init();
    _scrollWidth__WEBPACK_IMPORTED_MODULE_1__["default"].init();
  }
});

/***/ }),

/***/ "./modules/cssVars/scrollWidth.js":
/*!****************************************!*\
  !*** ./modules/cssVars/scrollWidth.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_getScrollWidth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../utils/getScrollWidth */ "./utils/getScrollWidth.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    this.calculate();
  },
  calculate: function calculate() {
    var scrollWidth = Object(_utils_getScrollWidth__WEBPACK_IMPORTED_MODULE_0__["default"])();
    document.documentElement.style.setProperty('--scroll-width', "".concat(scrollWidth, "px"));
  }
});

/***/ }),

/***/ "./modules/cssVars/vh.js":
/*!*******************************!*\
  !*** ./modules/cssVars/vh.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dispatcher__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../dispatcher */ "./modules/dispatcher.js");
/* harmony import */ var _utils_isTouchDevice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/isTouchDevice */ "./utils/isTouchDevice.js");


/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    this.calculate();
    this.handleResize();
  },
  calculate: function calculate() {
    var vh = document.documentElement.clientHeight * 0.01;
    document.documentElement.style.setProperty('--vh', "".concat(vh, "px"));
  },
  handleResize: function handleResize() {
    var _this = this;

    _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].subscribe(function (_ref) {
      var type = _ref.type;
      var targetType = Object(_utils_isTouchDevice__WEBPACK_IMPORTED_MODULE_1__["default"])() ? 'resize:both' : 'resize:height';

      if (type === targetType) {
        _this.calculate();
      }
    });
  }
});

/***/ }),

/***/ "./modules/dispatcher.js":
/*!*******************************!*\
  !*** ./modules/dispatcher.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_EventEmitter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/EventEmitter */ "./utils/EventEmitter.js");

/* harmony default export */ __webpack_exports__["default"] = (new _utils_EventEmitter__WEBPACK_IMPORTED_MODULE_0__["default"]());

/***/ }),

/***/ "./modules/lazyload.js":
/*!*****************************!*\
  !*** ./modules/lazyload.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "../../node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "../../node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "../../node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "../../node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.from */ "../../node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.is-array */ "../../node_modules/core-js/modules/es.array.is-array.js");
/* harmony import */ var core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "../../node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "../../node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.date.to-string */ "../../node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.function.name */ "../../node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "../../node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "../../node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "../../node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "../../node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "../../node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var core_js_features_object_assign__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! core-js/features/object/assign */ "../../node_modules/core-js/features/object/assign.js");
/* harmony import */ var core_js_features_object_assign__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(core_js_features_object_assign__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var intersection_observer__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! intersection-observer */ "../../node_modules/intersection-observer/intersection-observer.js");
/* harmony import */ var intersection_observer__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(intersection_observer__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var lozad__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! lozad */ "../../node_modules/lozad/dist/lozad.min.js");
/* harmony import */ var lozad__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(lozad__WEBPACK_IMPORTED_MODULE_17__);
















function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


 // polyfill


/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    var root = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document;
    var options = {
      rootMargin: "".concat(document.documentElement.clientHeight, "px 0px")
    };
    var pictures = root.querySelectorAll('.js-lazy-img:not([data-loaded])');
    var backgrounds = root.querySelectorAll('.js-lazy-bg:not([data-loaded])');

    if (pictures.length) {
      var pictureObserver = lozad__WEBPACK_IMPORTED_MODULE_17___default()(pictures, options);
      pictureObserver.observe();
    }

    if (backgrounds.length) {
      var backgroundObserver = lozad__WEBPACK_IMPORTED_MODULE_17___default()(backgrounds, options);
      backgroundObserver.observe();
    }

    this.lazyVideo(root);
  },
  lazyVideo: function lazyVideo() {
    var root = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document;

    var lazyVideos = _toConsumableArray(root.querySelectorAll('video.js-lazy-video:not([data-loaded])'));

    var observerOptions = {
      rootMargin: "".concat(document.documentElement.clientHeight, "px 0px")
    };

    if ('IntersectionObserver' in window) {
      var lazyVideoObserver = new IntersectionObserver(function (entries) {
        entries.forEach(function (video) {
          if (video.isIntersecting) {
            // eslint-disable-next-line no-restricted-syntax,guard-for-in
            for (var source in video.target.children) {
              // eslint-disable-next-line prefer-destructuring
              var videoSource = video.target.children[source];

              if (typeof videoSource.tagName === 'string' && videoSource.tagName === 'SOURCE') {
                // eslint-disable-next-line prefer-destructuring
                videoSource.src = videoSource.dataset.src;
              }
            }

            video.target.load();
            video.target.setAttribute('data-loaded', 'true');
            lazyVideoObserver.unobserve(video.target);
          }
        });
      }, observerOptions);
      lazyVideos.forEach(function (lazyVideo) {
        lazyVideoObserver.observe(lazyVideo);
      });
    }
  }
});

/***/ }),

/***/ "./modules/map.js":
/*!************************!*\
  !*** ./modules/map.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

document.addEventListener('click', function (e) {
  var map = document.querySelector('#s-map_wrap iframe');

  if (e.target.id === 's-map_wrap') {
    map.style.pointerEvents = 'all';
  } else {
    map.style.pointerEvents = 'none';
  }
});

/***/ }),

/***/ "./modules/media.js":
/*!**************************!*\
  !*** ./modules/media.js ***!
  \**************************/
/*! exports provided: screenWidth, wideDesktopMin, lowDesktopMin, tablet, mobile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "screenWidth", function() { return width; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "wideDesktopMin", function() { return wideDesktopMin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lowDesktopMin", function() { return lowDesktopMin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tablet", function() { return tablet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mobile", function() { return mobile; });
var width = {
  wideDesktop: 1920,
  desktop: 1280,
  lowDesktop: 1024,
  tablet: 768
};

function wideDesktopMin() {
  return window.matchMedia("(min-width: ".concat(width.wideDesktop, "px)")).matches;
}

function lowDesktopMin() {
  return window.matchMedia("(min-width: ".concat(width.lowDesktop, "px)")).matches;
}

function tablet() {
  return window.matchMedia("(max-width: ".concat(width.lowDesktop - 1, "px)")).matches;
}

function mobile() {
  return window.matchMedia("(max-width: ".concat(width.tablet - 1, "px)")).matches;
}



/***/ }),

/***/ "./modules/menu.js":
/*!*************************!*\
  !*** ./modules/menu.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _utils_throttle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/throttle */ "./utils/throttle.js");
/* harmony import */ var body_scroll_lock__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! body-scroll-lock */ "../../node_modules/body-scroll-lock/lib/bodyScrollLock.esm.js");
/* harmony import */ var _media__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./media */ "./modules/media.js");
/* harmony import */ var _dispatcher__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dispatcher */ "./modules/dispatcher.js");




/* harmony default export */ __webpack_exports__["default"] = ({
  classes: {
    menuOpen: '_site-menu-open'
  },
  el: {
    body: $('body'),
    siteMenuContainer: $('.site-menu__container')
  },
  init: function init() {
    this.handlers();
    this.handleDispatcher();
  },
  handlers: function handlers() {
    var self = this;
    var classes = self.classes,
        el = self.el;
    $(document).on('click', '.burger', Object(_utils_throttle__WEBPACK_IMPORTED_MODULE_0__["default"])(500, false, function () {
      el.body.hasClass(classes.menuOpen) ? self.close() : self.open();
    })).on('click', '.site-menu__item', Object(_utils_throttle__WEBPACK_IMPORTED_MODULE_0__["default"])(500, false, function () {
      el.body.hasClass(classes.menuOpen) ? self.close() : self.open();
    })).on('click', '.site-menu__bg', Object(_utils_throttle__WEBPACK_IMPORTED_MODULE_0__["default"])(500, false, function () {
      self.close();
    })).on('click', '.site-menu__mobile-button', Object(_utils_throttle__WEBPACK_IMPORTED_MODULE_0__["default"])(500, false, function () {
      self.close();
    }));
  },
  open: function open() {
    this.el.body.addClass(this.classes.menuOpen);
  },
  close: function close() {
    this.el.body.removeClass(this.classes.menuOpen);
  },
  handleDispatcher: function handleDispatcher() {
    var _this = this;

    _dispatcher__WEBPACK_IMPORTED_MODULE_3__["default"].subscribe(function (_ref) {
      var type = _ref.type;

      if (type === 'site-menu:open') {
        _this.open();
      }

      if (type === 'site-menu:close') {
        _this.close();
      }

      if (type === 'resize:width' && Object(_media__WEBPACK_IMPORTED_MODULE_2__["lowDesktopMin"])()) {
        _this.close();
      }
    });
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js")))

/***/ }),

/***/ "./modules/overlay.js":
/*!****************************!*\
  !*** ./modules/overlay.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

var popup = document.querySelector('#overlay');
var menuButton = document.querySelector('.site-header__icons-button');
var menuMobileButton = document.querySelector('.site-menu__mobile-button');
menuButton.addEventListener("click", function (e) {
  popup.style.display = 'block';
});
menuMobileButton.addEventListener("click", function (e) {
  popup.style.display = 'block';
});

/***/ }),

/***/ "./modules/resize.js":
/*!***************************!*\
  !*** ./modules/resize.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dispatcher__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dispatcher */ "./modules/dispatcher.js");
/* harmony import */ var _utils_throttle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/throttle */ "./utils/throttle.js");


/* harmony default export */ __webpack_exports__["default"] = ({
  size: {
    width: window.innerWidth,
    height: window.innerHeight
  },
  init: function init() {
    var self = this;
    window.addEventListener('resize', Object(_utils_throttle__WEBPACK_IMPORTED_MODULE_1__["default"])(300, false, function () {
      self.handleResize();
    }), false);
    window.addEventListener('orientationchange', Object(_utils_throttle__WEBPACK_IMPORTED_MODULE_1__["default"])(300, false, function () {
      self.handleResize();
    }), false);
  },
  handleResize: function handleResize() {
    /* eslint-disable prefer-destructuring */
    var width = window.innerWidth;
    var height = window.innerHeight;
    /* eslint-enable prefer-destructuring */

    var widthChanged = width !== this.size.width;
    var heightChanged = height !== this.size.height;

    if (widthChanged) {
      _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
        type: 'resize:width'
      });
    }

    if (heightChanged) {
      _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
        type: 'resize:height'
      });
    }

    if (widthChanged && heightChanged) {
      _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
        type: 'resize:both'
      });
    }

    _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
      type: 'resize:default'
    });
    this.size = {
      width: width,
      height: height
    };
  }
});

/***/ }),

/***/ "./modules/reviewsSlider.js":
/*!**********************************!*\
  !*** ./modules/reviewsSlider.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var swiper_bundle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swiper/bundle */ "../../node_modules/swiper/swiper-bundle.esm.js");
// import Swiper bundle with all modules installed

var swiper = new swiper_bundle__WEBPACK_IMPORTED_MODULE_0__["default"](".s-reviews__swiper", {
  slidesPerView: 3,
  spaceBetween: 30,
  pagination: {
    el: ".swiper-pagination",
    clickable: true
  },
  breakpoints: {
    320: {
      slidesPerView: 1
    },
    590: {
      slidesPerView: 2
    },
    768: {
      slidesPerView: 3
    }
  }
});

/***/ }),

/***/ "./modules/scrollTo.js":
/*!*****************************!*\
  !*** ./modules/scrollTo.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _dispatcher__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dispatcher */ "./modules/dispatcher.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    this.scrollByHash();
    this.handleEvents();
    this.handleDispatcher();
  },
  handleDispatcher: function handleDispatcher() {
    var _this = this;

    _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].subscribe(function (e) {
      if (e.type === 'scrollTo:scroll') {
        _this.scrollTo(e.selector, {
          offset: e.offset
        });
      }
    });
  },
  handleEvents: function handleEvents() {
    var self = this;
    $('.js-scroll-to').on('click', function (e) {
      e.preventDefault();
      var selector = $(this).attr('data-target');
      var speed = $(this).attr('data-speed');
      var offset = $(this).attr('data-offset');
      self.scrollTo(selector, {
        speed: speed,
        offset: offset
      });
    });
  },
  scrollTo: function scrollTo(selector) {
    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref$speed = _ref.speed,
        speed = _ref$speed === void 0 ? 800 : _ref$speed,
        _ref$offset = _ref.offset,
        offset = _ref$offset === void 0 ? 0 : _ref$offset;

    var $target = $(selector);
    if (!$target.length) return;
    _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
      type: 'sidebar:close'
    });
    $([document.documentElement, document.body]).animate({
      scrollTop: $target.offset().top - offset
    }, speed);
  },
  scrollByHash: function scrollByHash() {
    // eslint-disable-next-line prefer-destructuring
    var hash = window.location.hash;

    if (hash.length) {
      try {
        this.scrollTo(hash);
      } catch (e) {
        console.warn(e.message);
      }
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js")))

/***/ }),

/***/ "./modules/servicesFancybox.js":
/*!*************************************!*\
  !*** ./modules/servicesFancybox.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery) {// Require jQuery (Fancybox dependency)
window.$ = __webpack_provided_window_dot_jQuery = __webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js"); // Fancybox

var fancybox = __webpack_require__(/*! @fancyapps/fancybox */ "../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js");
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js")))

/***/ }),

/***/ "./modules/servicesSlider.js":
/*!***********************************!*\
  !*** ./modules/servicesSlider.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.define-property */ "../../node_modules/core-js/modules/es.object.define-property.js");
/* harmony import */ var core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var swiper_bundle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! swiper/bundle */ "../../node_modules/swiper/swiper-bundle.esm.js");


function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// import Swiper bundle with all modules installed

var swiper = new swiper_bundle__WEBPACK_IMPORTED_MODULE_1__["default"]('.mySwiper', {
  spaceBetween: 0,
  slidesPerView: 2,
  freeMode: true,
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
  slidesPerColumn: 2,
  observer: true,
  observeParents: true,
  observeSlideChildren: true,
  breakpoints: {
    320: {
      slidesPerView: 2,
      slidesPerColumn: 1
    },
    360: {
      slidesPerView: 3,
      slidesPerColumn: 1
    },
    490: {
      slidesPerView: 4,
      slidesPerColumn: 1
    },
    768: {
      slidesPerColumn: 2
    }
  },
  navigation: {
    nextEl: '.s-services__nav-right',
    prevEl: '.s-services__nav-left'
  },
  thumbs: {
    swiper: swiper
  }
});
var swiper2 = new swiper_bundle__WEBPACK_IMPORTED_MODULE_1__["default"]('.mySwiper2', {
  spaceBetween: 10,
  observer: true,
  observeParents: true,
  observeSlideChildren: true,
  navigation: {
    nextEl: '.s-services__mobile-button-next',
    prevEl: '.s-services__mobile-button-prev'
  },
  thumbs: {
    swiper: swiper
  }
});
$('.s-services__direction span').on('click', function () {
  var filter = $(this).html().toLowerCase();
  var slidesxcol;
  $('.s-services__direction span').removeClass('s-services_direction-active');
  $(this).addClass('s-services_direction-active');

  if (filter == 'all') {
    var _Swiper;

    $('[data-filter]').removeClass('non-swiper-slide').addClass('swiper-slide').show();
    if ($('.swiper-slide').length > 2) slidesxcol = 2;else slidesxcol = 1;
    swiper.destroy();
    swiper = new swiper_bundle__WEBPACK_IMPORTED_MODULE_1__["default"]('.mySwiper', (_Swiper = {
      slidesPerView: 2,
      slidesPerColumn: 2
    }, _defineProperty(_Swiper, "slidesPerColumn", slidesxcol), _defineProperty(_Swiper, "paginationClickable", true), _defineProperty(_Swiper, "spaceBetween", 0), _defineProperty(_Swiper, "breakpoints", {
      320: {
        slidesPerView: 2,
        slidesPerColumn: 1
      },
      360: {
        slidesPerView: 3,
        slidesPerColumn: 1
      },
      490: {
        slidesPerView: 4,
        slidesPerColumn: 1
      },
      768: {
        slidesPerColumn: slidesxcol
      }
    }), _defineProperty(_Swiper, "navigation", {
      nextEl: '.s-services__nav-right',
      prevEl: '.s-services__nav-left'
    }), _Swiper));
    swiper2.destroy();
    swiper2 = new swiper_bundle__WEBPACK_IMPORTED_MODULE_1__["default"]('.mySwiper2', {
      spaceBetween: 10,
      thumbs: {
        swiper: swiper
      },
      navigation: {
        nextEl: '.s-services__mobile-button-next',
        prevEl: '.s-services__mobile-button-prev'
      }
    });
    /*nav-box-hide*/

    if ($('.swiper-wrapper-thumbs .swiper-slide').length === 0) {
      $('.s-services__nav-box').addClass('s-services_nav-box-hide');
    } else if ($('.swiper-wrapper-thumbs .swiper-slide').length < 2) {
      $('.s-services__nav-box').addClass('s-services_nav-box-mt').removeClass('s-services_nav-box-hide');
    } else {
      $('.s-services__nav-box').removeClass('s-services_nav-box-mt').removeClass('s-services_nav-box-hide');
    }
    /*nav-box-hide*/

  } else {
    $('.s-services-swiper-slide').not("[data-filter='" + filter + "']").addClass('non-swiper-slide').removeClass('swiper-slide').hide();
    $("[data-filter='" + filter + "']").removeClass('non-swiper-slide').addClass('swiper-slide').attr('style', null).show();
    if ($('.swiper-slide').length > 2) slidesxcol = 2;else slidesxcol = 1;
    swiper.destroy();
    swiper = new swiper_bundle__WEBPACK_IMPORTED_MODULE_1__["default"]('.mySwiper', {
      slidesPerView: 2,
      slidesPerColumn: slidesxcol,
      paginationClickable: true,
      spaceBetween: 0,
      breakpoints: {
        320: {
          slidesPerView: 2,
          slidesPerColumn: 1
        },
        360: {
          slidesPerView: 3,
          slidesPerColumn: 1
        },
        490: {
          slidesPerView: 4,
          slidesPerColumn: 1
        },
        768: {
          slidesPerColumn: slidesxcol
        }
      },
      navigation: {
        nextEl: '.s-services__nav-right',
        prevEl: '.s-services__nav-left'
      }
    });
    swiper2.destroy();
    swiper2 = new swiper_bundle__WEBPACK_IMPORTED_MODULE_1__["default"]('.mySwiper2', {
      spaceBetween: 10,
      thumbs: {
        swiper: swiper
      },
      navigation: {
        nextEl: ".s-services__mobile-button-next",
        prevEl: ".s-services__mobile-button-prev"
      }
    });
    /*nav-box-hide*/

    if ($('.swiper-wrapper-thumbs .swiper-slide').length === 0) {
      $('.s-services__nav-box').addClass('s-services_nav-box-hide');
    } else if ($('.swiper-wrapper-thumbs .swiper-slide').length < 2) {
      $('.s-services__nav-box').addClass('s-services_nav-box-mt').removeClass('s-services_nav-box-hide');
    } else {
      $('.s-services__nav-box').removeClass('s-services_nav-box-mt').removeClass('s-services_nav-box-hide');
    }
    /*nav-box-hide*/

  }
});
$('.s-services__fancybox-link').on('click', function () {
  var items = document.getElementsByClassName('s-services__fancybox-link');
  var activeAll = $('#s-services_direction-active').hasClass('s-services_direction-active');

  if (activeAll) {
    for (var i = 0; i < items.length; i++) {
      items[i].dataset.fancybox = 'all';
    }
  } else {
    for (var _i = 0; _i < items.length; _i++) {
      items[_i].dataset.fancybox = items[_i].rel;
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js")))

/***/ }),

/***/ "./modules/sitePreloader.js":
/*!**********************************!*\
  !*** ./modules/sitePreloader.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "../../node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "../../node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "../../node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "../../node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "../../node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.from */ "../../node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.is-array */ "../../node_modules/core-js/modules/es.array.is-array.js");
/* harmony import */ var core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "../../node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.array.map */ "../../node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "../../node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.date.to-string */ "../../node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.function.bind */ "../../node_modules/core-js/modules/es.function.bind.js");
/* harmony import */ var core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.function.name */ "../../node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "../../node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "../../node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "../../node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "../../node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "../../node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "../../node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "../../node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! core-js/modules/web.timers */ "../../node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _dispatcher__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./dispatcher */ "./modules/dispatcher.js");
/* harmony import */ var _utils_documentReady__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../utils/documentReady */ "./utils/documentReady.js");






















function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }



var preloader = {
  el: null,
  images: [],
  backgroundEls: [],
  imagesNumber: 0,
  imagesLoaded: 0,
  transition: 1000,
  init: function init() {
    var _this = this;

    this.el = document.querySelector('#site-preloader');
    this.images = _toConsumableArray(document.images);
    this.backgroundEls = _toConsumableArray(document.querySelectorAll('.js-preloader-bg'));
    var imagesPaths = this.images.map(function (image) {
      return image.src;
    });
    var backgroundPaths = this.backgroundEls.map(function (elem) {
      var _window$getComputedSt = window.getComputedStyle(elem, false),
          backgroundImage = _window$getComputedSt.backgroundImage;

      return backgroundImage.slice(4, -1).replace(/"/g, '');
    });
    var allPaths = [].concat(_toConsumableArray(imagesPaths), _toConsumableArray(backgroundPaths)); // eslint-disable-next-line prefer-destructuring

    this.imagesNumber = allPaths.length;

    if (this.imagesNumber) {
      allPaths.forEach(function (imagesPath) {
        var clone = new Image();
        clone.addEventListener('load', _this.imageLoaded.bind(_this));
        clone.addEventListener('error', _this.imageLoaded.bind(_this));
        clone.src = imagesPath;
      });
    } else {
      this.preloaderHide();
    }
  },
  preloaderHide: function preloaderHide() {
    var transition = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.transition;
    var preloader = this.el;
    if (!preloader) return;
    _dispatcher__WEBPACK_IMPORTED_MODULE_21__["default"].dispatch({
      type: 'site-preloader:hiding'
    });
    preloader.style.transition = "opacity ".concat(transition, "ms ease, visibility ").concat(transition, "ms ease");
    preloader.classList.add('_loaded');
    document.body.classList.add('_site-loaded');
    setTimeout(function () {
      _dispatcher__WEBPACK_IMPORTED_MODULE_21__["default"].dispatch({
        type: 'site-preloader:removed'
      });
      preloader.remove();
      document.body.classList.add('_site-preloader-hidden');
    }, transition);
  },
  imageLoaded: function imageLoaded() {
    this.imagesLoaded += 1;

    if (this.imagesLoaded >= this.imagesNumber) {
      this.preloaderHide();
    }
  }
};
Object(_utils_documentReady__WEBPACK_IMPORTED_MODULE_22__["default"])(function () {
  preloader.init();
});
/* harmony default export */ __webpack_exports__["default"] = (preloader);

/***/ }),

/***/ "./modules/subscribe.js":
/*!******************************!*\
  !*** ./modules/subscribe.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {function initSubscribeValidate() {
  $('#subscribe__form').validate({
    submitHandler: function submitHandler() {
      var subscribeName = $('#subscribe__name').val();
      var subscribeEmail = $('#subscribe__email').val();
      var subscribeNumber = $('#subscribe__number').val();
      var subscribeTextarea = $('#subscribe__textarea').val();
      var overlayClose = $('#overlay');
      console.log(subscribeName);
      console.log(subscribeEmail);
      console.log(subscribeNumber);
      console.log(subscribeTextarea);
      alert("Thanks. The application is accepted. Our manager will contact you shortly.");
      overlayClose.style.display = 'none';
    },
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: "Please enter a valid email address"
    }
  });
  var inp = document.querySelector('#subscribe__number');
  inp.addEventListener('keypress', function (e) {
    // Отменяем ввод не цифр
    if (!/\d/.test(e.key)) e.preventDefault();
  });
}

$(document).ready(function () {
  initSubscribeValidate();
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js")))

/***/ }),

/***/ "./polyfills.js":
/*!**********************!*\
  !*** ./polyfills.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_features_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/features/promise */ "../../node_modules/core-js/features/promise/index.js");
/* harmony import */ var core_js_features_promise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_features_promise__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_features_number_is_nan__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/features/number/is-nan */ "../../node_modules/core-js/features/number/is-nan.js");
/* harmony import */ var core_js_features_number_is_nan__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_features_number_is_nan__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_features_array_find__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/features/array/find */ "../../node_modules/core-js/features/array/find.js");
/* harmony import */ var core_js_features_array_find__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_features_array_find__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_features_string_starts_with__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/features/string/starts-with */ "../../node_modules/core-js/features/string/starts-with.js");
/* harmony import */ var core_js_features_string_starts_with__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_features_string_starts_with__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vendor_polyfills_js_requestAnimationFrame__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vendor/_polyfills/js/requestAnimationFrame */ "../vendor/_polyfills/js/requestAnimationFrame.js");
/* harmony import */ var vendor_polyfills_js_DOM_Element_matches__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Element.matches */ "../vendor/_polyfills/js/DOM/Element.matches.js");
/* harmony import */ var vendor_polyfills_js_DOM_Element_closest__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Element.closest */ "../vendor/_polyfills/js/DOM/Element.closest.js");
/* harmony import */ var vendor_polyfills_js_DOM_Element_closest__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vendor_polyfills_js_DOM_Element_closest__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var vendor_polyfills_js_DOM_Node_remove__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Node.remove */ "../vendor/_polyfills/js/DOM/Node.remove.js");
/* harmony import */ var vendor_polyfills_js_DOM_Node_after__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Node.after */ "../vendor/_polyfills/js/DOM/Node.after.js");
/* harmony import */ var vendor_polyfills_js_DOM_Node_before__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Node.before */ "../vendor/_polyfills/js/DOM/Node.before.js");
/* harmony import */ var vendor_polyfills_js_DOM_Node_replaceWith__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Node.replaceWith */ "../vendor/_polyfills/js/DOM/Node.replaceWith.js");
/* harmony import */ var vendor_polyfills_js_DOM_ParentNode_append__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/ParentNode.append */ "../vendor/_polyfills/js/DOM/ParentNode.append.js");
/* harmony import */ var vendor_polyfills_js_DOM_ParentNode_prepend__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/ParentNode.prepend */ "../vendor/_polyfills/js/DOM/ParentNode.prepend.js");
// JS




 // DOM


 // зависимость - Element.matches








if (!SVGElement.prototype.contains) {
  SVGElement.prototype.contains = HTMLDivElement.prototype.contains;
} // customElements
// import 'vendor/_polyfills/customElements/document-register-element.max';

/***/ }),

/***/ "./utils/EventEmitter.js":
/*!*******************************!*\
  !*** ./utils/EventEmitter.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EventEmitter; });
/* harmony import */ var core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.filter */ "../../node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "../../node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.index-of */ "../../node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.object.define-property */ "../../node_modules/core-js/modules/es.object.define-property.js");
/* harmony import */ var core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "../../node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.string.split */ "../../node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "../../node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_6__);








function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* eslint-disable no-param-reassign */
var EventEmitter = /*#__PURE__*/function () {
  function EventEmitter() {
    _classCallCheck(this, EventEmitter);

    this._handlers = {
      all: []
    };
    this._frozen = false;
  }

  _createClass(EventEmitter, [{
    key: "dispatch",
    value: function dispatch(channel, event) {
      if (!event) {
        event = channel;
        channel = 'all';
      }

      if (event && event.type.indexOf(':')) {
        channel = event.type.split(':')[0];
      }

      if (!Object.prototype.hasOwnProperty.call(this._handlers, channel)) {
        this._handlers[channel] = [];
      }

      this._frozen = true;

      this._handlers[channel].forEach(function (handler) {
        return handler(event);
      });

      if (channel !== 'all') {
        this._handlers.all.forEach(function (handler) {
          return handler(event);
        });
      }

      this._frozen = false;
    }
  }, {
    key: "subscribe",
    value: function subscribe(channel, handler) {
      if (!handler) {
        handler = channel;
        channel = 'all';
      }

      if (this._frozen) {
        console.error('trying to subscribe to EventEmitter while dispatch is working');
      }

      if (typeof handler !== 'function') {
        console.error('handler has to be a function');
        return;
      }

      if (!Object.prototype.hasOwnProperty.call(this._handlers, channel)) {
        this._handlers[channel] = [];
      }

      if (this._handlers[channel].indexOf(handler) === -1) {
        this._handlers[channel].push(handler);
      } else {
        console.error('handler already set');
      }
    }
  }, {
    key: "unsubscribe",
    value: function unsubscribe(channel, handler) {
      if (!handler) {
        handler = channel;
        channel = 'all';
      }

      if (this._frozen) {
        console.error('trying to unsubscribe from EventEmitter while dispatch is working');
      }

      if (typeof handler !== 'function') {
        console.error('handler has to be a function');
      }

      if (!this._handlers[channel]) {
        console.error("channel ".concat(channel, " does not exist"));
        return;
      }

      if (this._handlers[channel].indexOf(handler) === -1) {
        console.log(handler);
        console.error('trying to unsubscribe unexisting handler');
        return;
      }

      this._handlers[channel] = this._handlers[channel].filter(function (h) {
        return h !== handler;
      });
    }
  }]);

  return EventEmitter;
}();



/***/ }),

/***/ "./utils/documentLoaded.js":
/*!*********************************!*\
  !*** ./utils/documentLoaded.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return documentLoaded; });
/**
 * Wait until document is loaded to run method
 * @param  {Function} fn Method to run
 */
function documentLoaded(fn) {
  // Sanity check
  if (typeof fn !== 'function') return; // If document is already loaded, run method

  if (document.readyState === 'complete') {
    return fn();
  } // Otherwise, wait until document is loaded


  window.addEventListener('load', fn, false);
}

/***/ }),

/***/ "./utils/documentReady.js":
/*!********************************!*\
  !*** ./utils/documentReady.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return documentReady; });
/**
 * Wait until document is ready to run method
 * @param  {Function} fn Method to run
 */
function documentReady(fn) {
  // Sanity check
  if (typeof fn !== 'function') return; // If document is already loaded, run method

  if (document.readyState === 'interactive' || document.readyState === 'complete') {
    return fn();
  } // Otherwise, wait until document is loaded


  document.addEventListener('DOMContentLoaded', fn, false);
}

/***/ }),

/***/ "./utils/getScrollWidth.js":
/*!*********************************!*\
  !*** ./utils/getScrollWidth.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getScrollWidth; });
/* harmony import */ var core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.assign */ "../../node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_0__);

function getScrollWidth() {
  var element = document.createElement('div');
  Object.assign(element.style, {
    overflowY: 'scroll',
    height: '50px',
    width: '50px',
    visibility: 'hidden'
  });
  document.body.append(element);
  var scrollWidth = element.offsetWidth - element.clientWidth;
  element.remove();
  return scrollWidth;
}

/***/ }),

/***/ "./utils/isTouchDevice.js":
/*!********************************!*\
  !*** ./utils/isTouchDevice.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return isTouchDevice; });
function isTouchDevice() {
  return 'ontouchstart' in window || navigator.maxTouchPoints;
}

/***/ }),

/***/ "./utils/jqueryValidate.js":
/*!*********************************!*\
  !*** ./utils/jqueryValidate.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module, jQuery) {/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "../../node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "../../node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "../../node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "../../node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.filter */ "../../node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.find */ "../../node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "../../node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_array_join__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.array.join */ "../../node_modules/core-js/modules/es.array.join.js");
/* harmony import */ var core_js_modules_es_array_join__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_join__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_array_last_index_of__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.array.last-index-of */ "../../node_modules/core-js/modules/es.array.last-index-of.js");
/* harmony import */ var core_js_modules_es_array_last_index_of__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_last_index_of__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.array.map */ "../../node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "../../node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.date.to-string */ "../../node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.function.name */ "../../node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_es_number_constructor__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/es.number.constructor */ "../../node_modules/core-js/modules/es.number.constructor.js");
/* harmony import */ var core_js_modules_es_number_constructor__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_number_constructor__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "../../node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var core_js_modules_es_regexp_constructor__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! core-js/modules/es.regexp.constructor */ "../../node_modules/core-js/modules/es.regexp.constructor.js");
/* harmony import */ var core_js_modules_es_regexp_constructor__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_constructor__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "../../node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "../../node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "../../node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! core-js/modules/es.string.match */ "../../node_modules/core-js/modules/es.string.match.js");
/* harmony import */ var core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_match__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "../../node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! core-js/modules/es.string.split */ "../../node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "../../node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_22__);
























function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * jQuery Validation Plugin v1.19.2
 *
 * https://jqueryvalidation.org/
 *
 * Copyright (c) 2020 Jörn Zaefferer
 * Released under the MIT license
 */
(function (factory) {
  if (typeof define === "function" && __webpack_require__(/*! !webpack amd options */ "../../node_modules/webpack/buildin/amd-options.js")) {
    define(["jquery"], factory);
  } else if (( false ? undefined : _typeof(module)) === "object" && module.exports) {
    module.exports = factory(__webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js"));
  } else {
    factory(jQuery);
  }
})(function ($) {
  $.extend($.fn, {
    // https://jqueryvalidation.org/validate/
    validate: function validate(options) {
      // If nothing is selected, return nothing; can't chain anyway
      if (!this.length) {
        if (options && options.debug && window.console) {
          console.warn("Nothing selected, can't validate, returning nothing.");
        }

        return;
      } // Check if a validator for this form was already created


      var validator = $.data(this[0], "validator");

      if (validator) {
        return validator;
      } // Add novalidate tag if HTML5.


      this.attr("novalidate", "novalidate");
      validator = new $.validator(options, this[0]);
      $.data(this[0], "validator", validator);

      if (validator.settings.onsubmit) {
        this.on("click.validate", ":submit", function (event) {
          // Track the used submit button to properly handle scripted
          // submits later.
          validator.submitButton = event.currentTarget; // Allow suppressing validation by adding a cancel class to the submit button

          if ($(this).hasClass("cancel")) {
            validator.cancelSubmit = true;
          } // Allow suppressing validation by adding the html5 formnovalidate attribute to the submit button


          if ($(this).attr("formnovalidate") !== undefined) {
            validator.cancelSubmit = true;
          }
        }); // Validate the form on submit

        this.on("submit.validate", function (event) {
          if (validator.settings.debug) {
            // Prevent form submit to be able to see console output
            event.preventDefault();
          }

          function handle() {
            var hidden, result; // Insert a hidden input as a replacement for the missing submit button
            // The hidden input is inserted in two cases:
            //   - A user defined a `submitHandler`
            //   - There was a pending request due to `remote` method and `stopRequest()`
            //     was called to submit the form in case it's valid

            if (validator.submitButton && (validator.settings.submitHandler || validator.formSubmitted)) {
              hidden = $("<input type='hidden'/>").attr("name", validator.submitButton.name).val($(validator.submitButton).val()).appendTo(validator.currentForm);
            }

            if (validator.settings.submitHandler && !validator.settings.debug) {
              result = validator.settings.submitHandler.call(validator, validator.currentForm, event);

              if (hidden) {
                // And clean up afterwards; thanks to no-block-scope, hidden can be referenced
                hidden.remove();
              }

              if (result !== undefined) {
                return result;
              }

              return false;
            }

            return true;
          } // Prevent submit for invalid forms or custom submit handlers


          if (validator.cancelSubmit) {
            validator.cancelSubmit = false;
            return handle();
          }

          if (validator.form()) {
            if (validator.pendingRequest) {
              validator.formSubmitted = true;
              return false;
            }

            return handle();
          } else {
            validator.focusInvalid();
            return false;
          }
        });
      }

      return validator;
    },
    // https://jqueryvalidation.org/valid/
    valid: function valid() {
      var valid, validator, errorList;

      if ($(this[0]).is("form")) {
        valid = this.validate().form();
      } else {
        errorList = [];
        valid = true;
        validator = $(this[0].form).validate();
        this.each(function () {
          valid = validator.element(this) && valid;

          if (!valid) {
            errorList = errorList.concat(validator.errorList);
          }
        });
        validator.errorList = errorList;
      }

      return valid;
    },
    // https://jqueryvalidation.org/rules/
    rules: function rules(command, argument) {
      var element = this[0],
          isContentEditable = typeof this.attr("contenteditable") !== "undefined" && this.attr("contenteditable") !== "false",
          settings,
          staticRules,
          existingRules,
          data,
          param,
          filtered; // If nothing is selected, return empty object; can't chain anyway

      if (element == null) {
        return;
      }

      if (!element.form && isContentEditable) {
        element.form = this.closest("form")[0];
        element.name = this.attr("name");
      }

      if (element.form == null) {
        return;
      }

      if (command) {
        settings = $.data(element.form, "validator").settings;
        staticRules = settings.rules;
        existingRules = $.validator.staticRules(element);

        switch (command) {
          case "add":
            $.extend(existingRules, $.validator.normalizeRule(argument)); // Remove messages from rules, but allow them to be set separately

            delete existingRules.messages;
            staticRules[element.name] = existingRules;

            if (argument.messages) {
              settings.messages[element.name] = $.extend(settings.messages[element.name], argument.messages);
            }

            break;

          case "remove":
            if (!argument) {
              delete staticRules[element.name];
              return existingRules;
            }

            filtered = {};
            $.each(argument.split(/\s/), function (index, method) {
              filtered[method] = existingRules[method];
              delete existingRules[method];
            });
            return filtered;
        }
      }

      data = $.validator.normalizeRules($.extend({}, $.validator.classRules(element), $.validator.attributeRules(element), $.validator.dataRules(element), $.validator.staticRules(element)), element); // Make sure required is at front

      if (data.required) {
        param = data.required;
        delete data.required;
        data = $.extend({
          required: param
        }, data);
      } // Make sure remote is at back


      if (data.remote) {
        param = data.remote;
        delete data.remote;
        data = $.extend(data, {
          remote: param
        });
      }

      return data;
    }
  }); // JQuery trim is deprecated, provide a trim method based on String.prototype.trim

  var trim = function trim(str) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/trim#Polyfill
    return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
  }; // Custom selectors


  $.extend($.expr.pseudos || $.expr[":"], {
    // '|| $.expr[ ":" ]' here enables backwards compatibility to jQuery 1.7. Can be removed when dropping jQ 1.7.x support
    // https://jqueryvalidation.org/blank-selector/
    blank: function blank(a) {
      return !trim("" + $(a).val());
    },
    // https://jqueryvalidation.org/filled-selector/
    filled: function filled(a) {
      var val = $(a).val();
      return val !== null && !!trim("" + val);
    },
    // https://jqueryvalidation.org/unchecked-selector/
    unchecked: function unchecked(a) {
      return !$(a).prop("checked");
    }
  }); // Constructor for validator

  $.validator = function (options, form) {
    this.settings = $.extend(true, {}, $.validator.defaults, options);
    this.currentForm = form;
    this.init();
  }; // https://jqueryvalidation.org/jQuery.validator.format/


  $.validator.format = function (source, params) {
    if (arguments.length === 1) {
      return function () {
        var args = $.makeArray(arguments);
        args.unshift(source);
        return $.validator.format.apply(this, args);
      };
    }

    if (params === undefined) {
      return source;
    }

    if (arguments.length > 2 && params.constructor !== Array) {
      params = $.makeArray(arguments).slice(1);
    }

    if (params.constructor !== Array) {
      params = [params];
    }

    $.each(params, function (i, n) {
      source = source.replace(new RegExp("\\{" + i + "\\}", "g"), function () {
        return n;
      });
    });
    return source;
  };

  $.extend($.validator, {
    defaults: {
      messages: {},
      groups: {},
      rules: {},
      errorClass: "error",
      pendingClass: "pending",
      validClass: "valid",
      errorElement: "label",
      focusCleanup: false,
      focusInvalid: true,
      errorContainer: $([]),
      errorLabelContainer: $([]),
      onsubmit: true,
      ignore: ":hidden",
      ignoreTitle: false,
      onfocusin: function onfocusin(element) {
        this.lastActive = element; // Hide error label and remove error class on focus if enabled

        if (this.settings.focusCleanup) {
          if (this.settings.unhighlight) {
            this.settings.unhighlight.call(this, element, this.settings.errorClass, this.settings.validClass);
          }

          this.hideThese(this.errorsFor(element));
        }
      },
      onfocusout: function onfocusout(element) {
        if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) {
          this.element(element);
        }
      },
      onkeyup: function onkeyup(element, event) {
        // Avoid revalidate the field when pressing one of the following keys
        // Shift       => 16
        // Ctrl        => 17
        // Alt         => 18
        // Caps lock   => 20
        // End         => 35
        // Home        => 36
        // Left arrow  => 37
        // Up arrow    => 38
        // Right arrow => 39
        // Down arrow  => 40
        // Insert      => 45
        // Num lock    => 144
        // AltGr key   => 225
        var excludedKeys = [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225];

        if (event.which === 9 && this.elementValue(element) === "" || $.inArray(event.keyCode, excludedKeys) !== -1) {
          return;
        } else if (element.name in this.submitted || element.name in this.invalid) {
          this.element(element);
        }
      },
      onclick: function onclick(element) {
        // Click on selects, radiobuttons and checkboxes
        if (element.name in this.submitted) {
          this.element(element); // Or option elements, check parent select in that case
        } else if (element.parentNode.name in this.submitted) {
          this.element(element.parentNode);
        }
      },
      highlight: function highlight(element, errorClass, validClass) {
        if (element.type === "radio") {
          this.findByName(element.name).addClass(errorClass).removeClass(validClass);
        } else {
          $(element).addClass(errorClass).removeClass(validClass);
        }
      },
      unhighlight: function unhighlight(element, errorClass, validClass) {
        if (element.type === "radio") {
          this.findByName(element.name).removeClass(errorClass).addClass(validClass);
        } else {
          $(element).removeClass(errorClass).addClass(validClass);
        }
      }
    },
    // https://jqueryvalidation.org/jQuery.validator.setDefaults/
    setDefaults: function setDefaults(settings) {
      $.extend($.validator.defaults, settings);
    },
    messages: {
      required: "This field is required.",
      remote: "Please fix this field.",
      email: "Please enter a valid email address.",
      url: "Please enter a valid URL.",
      date: "Please enter a valid date.",
      dateISO: "Please enter a valid date (ISO).",
      number: "Please enter a valid number.",
      digits: "Please enter only digits.",
      equalTo: "Please enter the same value again.",
      maxlength: $.validator.format("Please enter no more than {0} characters."),
      minlength: $.validator.format("Please enter at least {0} characters."),
      rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
      range: $.validator.format("Please enter a value between {0} and {1}."),
      max: $.validator.format("Please enter a value less than or equal to {0}."),
      min: $.validator.format("Please enter a value greater than or equal to {0}."),
      step: $.validator.format("Please enter a multiple of {0}.")
    },
    autoCreateRanges: false,
    prototype: {
      init: function init() {
        this.labelContainer = $(this.settings.errorLabelContainer);
        this.errorContext = this.labelContainer.length && this.labelContainer || $(this.currentForm);
        this.containers = $(this.settings.errorContainer).add(this.settings.errorLabelContainer);
        this.submitted = {};
        this.valueCache = {};
        this.pendingRequest = 0;
        this.pending = {};
        this.invalid = {};
        this.reset();
        var currentForm = this.currentForm,
            groups = this.groups = {},
            rules;
        $.each(this.settings.groups, function (key, value) {
          if (typeof value === "string") {
            value = value.split(/\s/);
          }

          $.each(value, function (index, name) {
            groups[name] = key;
          });
        });
        rules = this.settings.rules;
        $.each(rules, function (key, value) {
          rules[key] = $.validator.normalizeRule(value);
        });

        function delegate(event) {
          var isContentEditable = typeof $(this).attr("contenteditable") !== "undefined" && $(this).attr("contenteditable") !== "false"; // Set form expando on contenteditable

          if (!this.form && isContentEditable) {
            this.form = $(this).closest("form")[0];
            this.name = $(this).attr("name");
          } // Ignore the element if it belongs to another form. This will happen mainly
          // when setting the `form` attribute of an input to the id of another form.


          if (currentForm !== this.form) {
            return;
          }

          var validator = $.data(this.form, "validator"),
              eventType = "on" + event.type.replace(/^validate/, ""),
              settings = validator.settings;

          if (settings[eventType] && !$(this).is(settings.ignore)) {
            settings[eventType].call(validator, this, event);
          }
        }

        $(this.currentForm).on("focusin.validate focusout.validate keyup.validate", ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], " + "[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], " + "[type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], " + "[type='radio'], [type='checkbox'], [contenteditable], [type='button']", delegate) // Support: Chrome, oldIE
        // "select" is provided as event.target when clicking a option
        .on("click.validate", "select, option, [type='radio'], [type='checkbox']", delegate);

        if (this.settings.invalidHandler) {
          $(this.currentForm).on("invalid-form.validate", this.settings.invalidHandler);
        }
      },
      // https://jqueryvalidation.org/Validator.form/
      form: function form() {
        this.checkForm();
        $.extend(this.submitted, this.errorMap);
        this.invalid = $.extend({}, this.errorMap);

        if (!this.valid()) {
          $(this.currentForm).triggerHandler("invalid-form", [this]);
        }

        this.showErrors();
        return this.valid();
      },
      checkForm: function checkForm() {
        this.prepareForm();

        for (var i = 0, elements = this.currentElements = this.elements(); elements[i]; i++) {
          this.check(elements[i]);
        }

        return this.valid();
      },
      // https://jqueryvalidation.org/Validator.element/
      element: function element(_element) {
        var cleanElement = this.clean(_element),
            checkElement = this.validationTargetFor(cleanElement),
            v = this,
            result = true,
            rs,
            group;

        if (checkElement === undefined) {
          delete this.invalid[cleanElement.name];
        } else {
          this.prepareElement(checkElement);
          this.currentElements = $(checkElement); // If this element is grouped, then validate all group elements already
          // containing a value

          group = this.groups[checkElement.name];

          if (group) {
            $.each(this.groups, function (name, testgroup) {
              if (testgroup === group && name !== checkElement.name) {
                cleanElement = v.validationTargetFor(v.clean(v.findByName(name)));

                if (cleanElement && cleanElement.name in v.invalid) {
                  v.currentElements.push(cleanElement);
                  result = v.check(cleanElement) && result;
                }
              }
            });
          }

          rs = this.check(checkElement) !== false;
          result = result && rs;

          if (rs) {
            this.invalid[checkElement.name] = false;
          } else {
            this.invalid[checkElement.name] = true;
          }

          if (!this.numberOfInvalids()) {
            // Hide error containers on last error
            this.toHide = this.toHide.add(this.containers);
          }

          this.showErrors(); // Add aria-invalid status for screen readers

          $(_element).attr("aria-invalid", !rs);
        }

        return result;
      },
      // https://jqueryvalidation.org/Validator.showErrors/
      showErrors: function showErrors(errors) {
        if (errors) {
          var validator = this; // Add items to error list and map

          $.extend(this.errorMap, errors);
          this.errorList = $.map(this.errorMap, function (message, name) {
            return {
              message: message,
              element: validator.findByName(name)[0]
            };
          }); // Remove items from success list

          this.successList = $.grep(this.successList, function (element) {
            return !(element.name in errors);
          });
        }

        if (this.settings.showErrors) {
          this.settings.showErrors.call(this, this.errorMap, this.errorList);
        } else {
          this.defaultShowErrors();
        }
      },
      // https://jqueryvalidation.org/Validator.resetForm/
      resetForm: function resetForm() {
        if ($.fn.resetForm) {
          $(this.currentForm).resetForm();
        }

        this.invalid = {};
        this.submitted = {};
        this.prepareForm();
        this.hideErrors();
        var elements = this.elements().removeData("previousValue").removeAttr("aria-invalid");
        this.resetElements(elements);
      },
      resetElements: function resetElements(elements) {
        var i;

        if (this.settings.unhighlight) {
          for (i = 0; elements[i]; i++) {
            this.settings.unhighlight.call(this, elements[i], this.settings.errorClass, "");
            this.findByName(elements[i].name).removeClass(this.settings.validClass);
          }
        } else {
          elements.removeClass(this.settings.errorClass).removeClass(this.settings.validClass);
        }
      },
      numberOfInvalids: function numberOfInvalids() {
        return this.objectLength(this.invalid);
      },
      objectLength: function objectLength(obj) {
        /* jshint unused: false */
        var count = 0,
            i;

        for (i in obj) {
          // This check allows counting elements with empty error
          // message as invalid elements
          if (obj[i] !== undefined && obj[i] !== null && obj[i] !== false) {
            count++;
          }
        }

        return count;
      },
      hideErrors: function hideErrors() {
        this.hideThese(this.toHide);
      },
      hideThese: function hideThese(errors) {
        errors.not(this.containers).text("");
        this.addWrapper(errors).hide();
      },
      valid: function valid() {
        return this.size() === 0;
      },
      size: function size() {
        return this.errorList.length;
      },
      focusInvalid: function focusInvalid() {
        if (this.settings.focusInvalid) {
          try {
            $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").trigger("focus") // Manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
            .trigger("focusin");
          } catch (e) {// Ignore IE throwing errors when focusing hidden elements
          }
        }
      },
      findLastActive: function findLastActive() {
        var lastActive = this.lastActive;
        return lastActive && $.grep(this.errorList, function (n) {
          return n.element.name === lastActive.name;
        }).length === 1 && lastActive;
      },
      elements: function elements() {
        var validator = this,
            rulesCache = {}; // Select all valid inputs inside the form (no submit or reset buttons)

        return $(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function () {
          var name = this.name || $(this).attr("name"); // For contenteditable

          var isContentEditable = typeof $(this).attr("contenteditable") !== "undefined" && $(this).attr("contenteditable") !== "false";

          if (!name && validator.settings.debug && window.console) {
            console.error("%o has no name assigned", this);
          } // Set form expando on contenteditable


          if (isContentEditable) {
            this.form = $(this).closest("form")[0];
            this.name = name;
          } // Ignore elements that belong to other/nested forms


          if (this.form !== validator.currentForm) {
            return false;
          } // Select only the first element for each name, and only those with rules specified


          if (name in rulesCache || !validator.objectLength($(this).rules())) {
            return false;
          }

          rulesCache[name] = true;
          return true;
        });
      },
      clean: function clean(selector) {
        return $(selector)[0];
      },
      errors: function errors() {
        var errorClass = this.settings.errorClass.split(" ").join(".");
        return $(this.settings.errorElement + "." + errorClass, this.errorContext);
      },
      resetInternals: function resetInternals() {
        this.successList = [];
        this.errorList = [];
        this.errorMap = {};
        this.toShow = $([]);
        this.toHide = $([]);
      },
      reset: function reset() {
        this.resetInternals();
        this.currentElements = $([]);
      },
      prepareForm: function prepareForm() {
        this.reset();
        this.toHide = this.errors().add(this.containers);
      },
      prepareElement: function prepareElement(element) {
        this.reset();
        this.toHide = this.errorsFor(element);
      },
      elementValue: function elementValue(element) {
        var $element = $(element),
            type = element.type,
            isContentEditable = typeof $element.attr("contenteditable") !== "undefined" && $element.attr("contenteditable") !== "false",
            val,
            idx;

        if (type === "radio" || type === "checkbox") {
          return this.findByName(element.name).filter(":checked").val();
        } else if (type === "number" && typeof element.validity !== "undefined") {
          return element.validity.badInput ? "NaN" : $element.val();
        }

        if (isContentEditable) {
          val = $element.text();
        } else {
          val = $element.val();
        }

        if (type === "file") {
          // Modern browser (chrome & safari)
          if (val.substr(0, 12) === "C:\\fakepath\\") {
            return val.substr(12);
          } // Legacy browsers
          // Unix-based path


          idx = val.lastIndexOf("/");

          if (idx >= 0) {
            return val.substr(idx + 1);
          } // Windows-based path


          idx = val.lastIndexOf("\\");

          if (idx >= 0) {
            return val.substr(idx + 1);
          } // Just the file name


          return val;
        }

        if (typeof val === "string") {
          return val.replace(/\r/g, "");
        }

        return val;
      },
      check: function check(element) {
        element = this.validationTargetFor(this.clean(element));
        var rules = $(element).rules(),
            rulesCount = $.map(rules, function (n, i) {
          return i;
        }).length,
            dependencyMismatch = false,
            val = this.elementValue(element),
            result,
            method,
            rule,
            normalizer; // Prioritize the local normalizer defined for this element over the global one
        // if the former exists, otherwise user the global one in case it exists.

        if (typeof rules.normalizer === "function") {
          normalizer = rules.normalizer;
        } else if (typeof this.settings.normalizer === "function") {
          normalizer = this.settings.normalizer;
        } // If normalizer is defined, then call it to retreive the changed value instead
        // of using the real one.
        // Note that `this` in the normalizer is `element`.


        if (normalizer) {
          val = normalizer.call(element, val); // Delete the normalizer from rules to avoid treating it as a pre-defined method.

          delete rules.normalizer;
        }

        for (method in rules) {
          rule = {
            method: method,
            parameters: rules[method]
          };

          try {
            result = $.validator.methods[method].call(this, val, element, rule.parameters); // If a method indicates that the field is optional and therefore valid,
            // don't mark it as valid when there are no other rules

            if (result === "dependency-mismatch" && rulesCount === 1) {
              dependencyMismatch = true;
              continue;
            }

            dependencyMismatch = false;

            if (result === "pending") {
              this.toHide = this.toHide.not(this.errorsFor(element));
              return;
            }

            if (!result) {
              this.formatAndAdd(element, rule);
              return false;
            }
          } catch (e) {
            if (this.settings.debug && window.console) {
              console.log("Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.", e);
            }

            if (e instanceof TypeError) {
              e.message += ".  Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.";
            }

            throw e;
          }
        }

        if (dependencyMismatch) {
          return;
        }

        if (this.objectLength(rules)) {
          this.successList.push(element);
        }

        return true;
      },
      // Return the custom message for the given element and validation method
      // specified in the element's HTML5 data attribute
      // return the generic message if present and no method specific message is present
      customDataMessage: function customDataMessage(element, method) {
        return $(element).data("msg" + method.charAt(0).toUpperCase() + method.substring(1).toLowerCase()) || $(element).data("msg");
      },
      // Return the custom message for the given element name and validation method
      customMessage: function customMessage(name, method) {
        var m = this.settings.messages[name];
        return m && (m.constructor === String ? m : m[method]);
      },
      // Return the first defined argument, allowing empty strings
      findDefined: function findDefined() {
        for (var i = 0; i < arguments.length; i++) {
          if (arguments[i] !== undefined) {
            return arguments[i];
          }
        }

        return undefined;
      },
      // The second parameter 'rule' used to be a string, and extended to an object literal
      // of the following form:
      // rule = {
      //     method: "method name",
      //     parameters: "the given method parameters"
      // }
      //
      // The old behavior still supported, kept to maintain backward compatibility with
      // old code, and will be removed in the next major release.
      defaultMessage: function defaultMessage(element, rule) {
        if (typeof rule === "string") {
          rule = {
            method: rule
          };
        }

        var message = this.findDefined(this.customMessage(element.name, rule.method), this.customDataMessage(element, rule.method), // 'title' is never undefined, so handle empty string as undefined
        !this.settings.ignoreTitle && element.title || undefined, $.validator.messages[rule.method], "<strong>Warning: No message defined for " + element.name + "</strong>"),
            theregex = /\$?\{(\d+)\}/g;

        if (typeof message === "function") {
          message = message.call(this, rule.parameters, element);
        } else if (theregex.test(message)) {
          message = $.validator.format(message.replace(theregex, "{$1}"), rule.parameters);
        }

        return message;
      },
      formatAndAdd: function formatAndAdd(element, rule) {
        var message = this.defaultMessage(element, rule);
        this.errorList.push({
          message: message,
          element: element,
          method: rule.method
        });
        this.errorMap[element.name] = message;
        this.submitted[element.name] = message;
      },
      addWrapper: function addWrapper(toToggle) {
        if (this.settings.wrapper) {
          toToggle = toToggle.add(toToggle.parent(this.settings.wrapper));
        }

        return toToggle;
      },
      defaultShowErrors: function defaultShowErrors() {
        var i, elements, error;

        for (i = 0; this.errorList[i]; i++) {
          error = this.errorList[i];

          if (this.settings.highlight) {
            this.settings.highlight.call(this, error.element, this.settings.errorClass, this.settings.validClass);
          }

          this.showLabel(error.element, error.message);
        }

        if (this.errorList.length) {
          this.toShow = this.toShow.add(this.containers);
        }

        if (this.settings.success) {
          for (i = 0; this.successList[i]; i++) {
            this.showLabel(this.successList[i]);
          }
        }

        if (this.settings.unhighlight) {
          for (i = 0, elements = this.validElements(); elements[i]; i++) {
            this.settings.unhighlight.call(this, elements[i], this.settings.errorClass, this.settings.validClass);
          }
        }

        this.toHide = this.toHide.not(this.toShow);
        this.hideErrors();
        this.addWrapper(this.toShow).show();
      },
      validElements: function validElements() {
        return this.currentElements.not(this.invalidElements());
      },
      invalidElements: function invalidElements() {
        return $(this.errorList).map(function () {
          return this.element;
        });
      },
      showLabel: function showLabel(element, message) {
        var place,
            group,
            errorID,
            v,
            error = this.errorsFor(element),
            elementID = this.idOrName(element),
            describedBy = $(element).attr("aria-describedby");

        if (error.length) {
          // Refresh error/success class
          error.removeClass(this.settings.validClass).addClass(this.settings.errorClass); // Replace message on existing label

          error.html(message);
        } else {
          // Create error element
          error = $("<" + this.settings.errorElement + ">").attr("id", elementID + "-error").addClass(this.settings.errorClass).html(message || ""); // Maintain reference to the element to be placed into the DOM

          place = error;

          if (this.settings.wrapper) {
            // Make sure the element is visible, even in IE
            // actually showing the wrapped element is handled elsewhere
            place = error.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
          }

          if (this.labelContainer.length) {
            this.labelContainer.append(place);
          } else if (this.settings.errorPlacement) {
            this.settings.errorPlacement.call(this, place, $(element));
          } else {
            place.insertAfter(element);
          } // Link error back to the element


          if (error.is("label")) {
            // If the error is a label, then associate using 'for'
            error.attr("for", elementID); // If the element is not a child of an associated label, then it's necessary
            // to explicitly apply aria-describedby
          } else if (error.parents("label[for='" + this.escapeCssMeta(elementID) + "']").length === 0) {
            errorID = error.attr("id"); // Respect existing non-error aria-describedby

            if (!describedBy) {
              describedBy = errorID;
            } else if (!describedBy.match(new RegExp("\\b" + this.escapeCssMeta(errorID) + "\\b"))) {
              // Add to end of list if not already present
              describedBy += " " + errorID;
            }

            $(element).attr("aria-describedby", describedBy); // If this element is grouped, then assign to all elements in the same group

            group = this.groups[element.name];

            if (group) {
              v = this;
              $.each(v.groups, function (name, testgroup) {
                if (testgroup === group) {
                  $("[name='" + v.escapeCssMeta(name) + "']", v.currentForm).attr("aria-describedby", error.attr("id"));
                }
              });
            }
          }
        }

        if (!message && this.settings.success) {
          error.text("");

          if (typeof this.settings.success === "string") {
            error.addClass(this.settings.success);
          } else {
            this.settings.success(error, element);
          }
        }

        this.toShow = this.toShow.add(error);
      },
      errorsFor: function errorsFor(element) {
        var name = this.escapeCssMeta(this.idOrName(element)),
            describer = $(element).attr("aria-describedby"),
            selector = "label[for='" + name + "'], label[for='" + name + "'] *"; // 'aria-describedby' should directly reference the error element

        if (describer) {
          selector = selector + ", #" + this.escapeCssMeta(describer).replace(/\s+/g, ", #");
        }

        return this.errors().filter(selector);
      },
      // See https://api.jquery.com/category/selectors/, for CSS
      // meta-characters that should be escaped in order to be used with JQuery
      // as a literal part of a name/id or any selector.
      escapeCssMeta: function escapeCssMeta(string) {
        return string.replace(/([\\!"#$%&'()*+,./:;<=>?@\[\]^`{|}~])/g, "\\$1");
      },
      idOrName: function idOrName(element) {
        return this.groups[element.name] || (this.checkable(element) ? element.name : element.id || element.name);
      },
      validationTargetFor: function validationTargetFor(element) {
        // If radio/checkbox, validate first element in group instead
        if (this.checkable(element)) {
          element = this.findByName(element.name);
        } // Always apply ignore filter


        return $(element).not(this.settings.ignore)[0];
      },
      checkable: function checkable(element) {
        return /radio|checkbox/i.test(element.type);
      },
      findByName: function findByName(name) {
        return $(this.currentForm).find("[name='" + this.escapeCssMeta(name) + "']");
      },
      getLength: function getLength(value, element) {
        switch (element.nodeName.toLowerCase()) {
          case "select":
            return $("option:selected", element).length;

          case "input":
            if (this.checkable(element)) {
              return this.findByName(element.name).filter(":checked").length;
            }

        }

        return value.length;
      },
      depend: function depend(param, element) {
        return this.dependTypes[_typeof(param)] ? this.dependTypes[_typeof(param)](param, element) : true;
      },
      dependTypes: {
        "boolean": function boolean(param) {
          return param;
        },
        "string": function string(param, element) {
          return !!$(param, element.form).length;
        },
        "function": function _function(param, element) {
          return param(element);
        }
      },
      optional: function optional(element) {
        var val = this.elementValue(element);
        return !$.validator.methods.required.call(this, val, element) && "dependency-mismatch";
      },
      startRequest: function startRequest(element) {
        if (!this.pending[element.name]) {
          this.pendingRequest++;
          $(element).addClass(this.settings.pendingClass);
          this.pending[element.name] = true;
        }
      },
      stopRequest: function stopRequest(element, valid) {
        this.pendingRequest--; // Sometimes synchronization fails, make sure pendingRequest is never < 0

        if (this.pendingRequest < 0) {
          this.pendingRequest = 0;
        }

        delete this.pending[element.name];
        $(element).removeClass(this.settings.pendingClass);

        if (valid && this.pendingRequest === 0 && this.formSubmitted && this.form()) {
          $(this.currentForm).submit(); // Remove the hidden input that was used as a replacement for the
          // missing submit button. The hidden input is added by `handle()`
          // to ensure that the value of the used submit button is passed on
          // for scripted submits triggered by this method

          if (this.submitButton) {
            $("input:hidden[name='" + this.submitButton.name + "']", this.currentForm).remove();
          }

          this.formSubmitted = false;
        } else if (!valid && this.pendingRequest === 0 && this.formSubmitted) {
          $(this.currentForm).triggerHandler("invalid-form", [this]);
          this.formSubmitted = false;
        }
      },
      previousValue: function previousValue(element, method) {
        method = typeof method === "string" && method || "remote";
        return $.data(element, "previousValue") || $.data(element, "previousValue", {
          old: null,
          valid: true,
          message: this.defaultMessage(element, {
            method: method
          })
        });
      },
      // Cleans up all forms and elements, removes validator-specific events
      destroy: function destroy() {
        this.resetForm();
        $(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur").find(".validate-lessThan-blur").off(".validate-lessThan").removeClass("validate-lessThan-blur").find(".validate-lessThanEqual-blur").off(".validate-lessThanEqual").removeClass("validate-lessThanEqual-blur").find(".validate-greaterThanEqual-blur").off(".validate-greaterThanEqual").removeClass("validate-greaterThanEqual-blur").find(".validate-greaterThan-blur").off(".validate-greaterThan").removeClass("validate-greaterThan-blur");
      }
    },
    classRuleSettings: {
      required: {
        required: true
      },
      email: {
        email: true
      },
      url: {
        url: true
      },
      date: {
        date: true
      },
      dateISO: {
        dateISO: true
      },
      number: {
        number: true
      },
      digits: {
        digits: true
      },
      creditcard: {
        creditcard: true
      }
    },
    addClassRules: function addClassRules(className, rules) {
      if (className.constructor === String) {
        this.classRuleSettings[className] = rules;
      } else {
        $.extend(this.classRuleSettings, className);
      }
    },
    classRules: function classRules(element) {
      var rules = {},
          classes = $(element).attr("class");

      if (classes) {
        $.each(classes.split(" "), function () {
          if (this in $.validator.classRuleSettings) {
            $.extend(rules, $.validator.classRuleSettings[this]);
          }
        });
      }

      return rules;
    },
    normalizeAttributeRule: function normalizeAttributeRule(rules, type, method, value) {
      // Convert the value to a number for number inputs, and for text for backwards compability
      // allows type="date" and others to be compared as strings
      if (/min|max|step/.test(method) && (type === null || /number|range|text/.test(type))) {
        value = Number(value); // Support Opera Mini, which returns NaN for undefined minlength

        if (isNaN(value)) {
          value = undefined;
        }
      }

      if (value || value === 0) {
        rules[method] = value;
      } else if (type === method && type !== "range") {
        // Exception: the jquery validate 'range' method
        // does not test for the html5 'range' type
        rules[method] = true;
      }
    },
    attributeRules: function attributeRules(element) {
      var rules = {},
          $element = $(element),
          type = element.getAttribute("type"),
          method,
          value;

      for (method in $.validator.methods) {
        // Support for <input required> in both html5 and older browsers
        if (method === "required") {
          value = element.getAttribute(method); // Some browsers return an empty string for the required attribute
          // and non-HTML5 browsers might have required="" markup

          if (value === "") {
            value = true;
          } // Force non-HTML5 browsers to return bool


          value = !!value;
        } else {
          value = $element.attr(method);
        }

        this.normalizeAttributeRule(rules, type, method, value);
      } // 'maxlength' may be returned as -1, 2147483647 ( IE ) and 524288 ( safari ) for text inputs


      if (rules.maxlength && /-1|2147483647|524288/.test(rules.maxlength)) {
        delete rules.maxlength;
      }

      return rules;
    },
    dataRules: function dataRules(element) {
      var rules = {},
          $element = $(element),
          type = element.getAttribute("type"),
          method,
          value;

      for (method in $.validator.methods) {
        value = $element.data("rule" + method.charAt(0).toUpperCase() + method.substring(1).toLowerCase()); // Cast empty attributes like `data-rule-required` to `true`

        if (value === "") {
          value = true;
        }

        this.normalizeAttributeRule(rules, type, method, value);
      }

      return rules;
    },
    staticRules: function staticRules(element) {
      var rules = {},
          validator = $.data(element.form, "validator");

      if (validator.settings.rules) {
        rules = $.validator.normalizeRule(validator.settings.rules[element.name]) || {};
      }

      return rules;
    },
    normalizeRules: function normalizeRules(rules, element) {
      // Handle dependency check
      $.each(rules, function (prop, val) {
        // Ignore rule when param is explicitly false, eg. required:false
        if (val === false) {
          delete rules[prop];
          return;
        }

        if (val.param || val.depends) {
          var keepRule = true;

          switch (_typeof(val.depends)) {
            case "string":
              keepRule = !!$(val.depends, element.form).length;
              break;

            case "function":
              keepRule = val.depends.call(element, element);
              break;
          }

          if (keepRule) {
            rules[prop] = val.param !== undefined ? val.param : true;
          } else {
            $.data(element.form, "validator").resetElements($(element));
            delete rules[prop];
          }
        }
      }); // Evaluate parameters

      $.each(rules, function (rule, parameter) {
        rules[rule] = $.isFunction(parameter) && rule !== "normalizer" ? parameter(element) : parameter;
      }); // Clean number parameters

      $.each(["minlength", "maxlength"], function () {
        if (rules[this]) {
          rules[this] = Number(rules[this]);
        }
      });
      $.each(["rangelength", "range"], function () {
        var parts;

        if (rules[this]) {
          if ($.isArray(rules[this])) {
            rules[this] = [Number(rules[this][0]), Number(rules[this][1])];
          } else if (typeof rules[this] === "string") {
            parts = rules[this].replace(/[\[\]]/g, "").split(/[\s,]+/);
            rules[this] = [Number(parts[0]), Number(parts[1])];
          }
        }
      });

      if ($.validator.autoCreateRanges) {
        // Auto-create ranges
        if (rules.min != null && rules.max != null) {
          rules.range = [rules.min, rules.max];
          delete rules.min;
          delete rules.max;
        }

        if (rules.minlength != null && rules.maxlength != null) {
          rules.rangelength = [rules.minlength, rules.maxlength];
          delete rules.minlength;
          delete rules.maxlength;
        }
      }

      return rules;
    },
    // Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
    normalizeRule: function normalizeRule(data) {
      if (typeof data === "string") {
        var transformed = {};
        $.each(data.split(/\s/), function () {
          transformed[this] = true;
        });
        data = transformed;
      }

      return data;
    },
    // https://jqueryvalidation.org/jQuery.validator.addMethod/
    addMethod: function addMethod(name, method, message) {
      $.validator.methods[name] = method;
      $.validator.messages[name] = message !== undefined ? message : $.validator.messages[name];

      if (method.length < 3) {
        $.validator.addClassRules(name, $.validator.normalizeRule(name));
      }
    },
    // https://jqueryvalidation.org/jQuery.validator.methods/
    methods: {
      // https://jqueryvalidation.org/required-method/
      required: function required(value, element, param) {
        // Check if dependency is met
        if (!this.depend(param, element)) {
          return "dependency-mismatch";
        }

        if (element.nodeName.toLowerCase() === "select") {
          // Could be an array for select-multiple or a string, both are fine this way
          var val = $(element).val();
          return val && val.length > 0;
        }

        if (this.checkable(element)) {
          return this.getLength(value, element) > 0;
        }

        return value !== undefined && value !== null && value.length > 0;
      },
      // https://jqueryvalidation.org/email-method/
      email: function email(value, element) {
        // From https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
        // Retrieved 2014-01-14
        // If you have a problem with this implementation, report a bug against the above spec
        // Or use custom methods to implement your own email validation
        return this.optional(element) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(value);
      },
      // https://jqueryvalidation.org/url-method/
      url: function url(value, element) {
        // Copyright (c) 2010-2013 Diego Perini, MIT licensed
        // https://gist.github.com/dperini/729294
        // see also https://mathiasbynens.be/demo/url-regex
        // modified to allow protocol-relative URLs
        return this.optional(element) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
      },
      // https://jqueryvalidation.org/date-method/
      date: function () {
        var called = false;
        return function (value, element) {
          if (!called) {
            called = true;

            if (this.settings.debug && window.console) {
              console.warn("The `date` method is deprecated and will be removed in version '2.0.0'.\n" + "Please don't use it, since it relies on the Date constructor, which\n" + "behaves very differently across browsers and locales. Use `dateISO`\n" + "instead or one of the locale specific methods in `localizations/`\n" + "and `additional-methods.js`.");
            }
          }

          return this.optional(element) || !/Invalid|NaN/.test(new Date(value).toString());
        };
      }(),
      // https://jqueryvalidation.org/dateISO-method/
      dateISO: function dateISO(value, element) {
        return this.optional(element) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(value);
      },
      // https://jqueryvalidation.org/number-method/
      number: function number(value, element) {
        return this.optional(element) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
      },
      // https://jqueryvalidation.org/digits-method/
      digits: function digits(value, element) {
        return this.optional(element) || /^\d+$/.test(value);
      },
      // https://jqueryvalidation.org/minlength-method/
      minlength: function minlength(value, element, param) {
        var length = $.isArray(value) ? value.length : this.getLength(value, element);
        return this.optional(element) || length >= param;
      },
      // https://jqueryvalidation.org/maxlength-method/
      maxlength: function maxlength(value, element, param) {
        var length = $.isArray(value) ? value.length : this.getLength(value, element);
        return this.optional(element) || length <= param;
      },
      // https://jqueryvalidation.org/rangelength-method/
      rangelength: function rangelength(value, element, param) {
        var length = $.isArray(value) ? value.length : this.getLength(value, element);
        return this.optional(element) || length >= param[0] && length <= param[1];
      },
      // https://jqueryvalidation.org/min-method/
      min: function min(value, element, param) {
        return this.optional(element) || value >= param;
      },
      // https://jqueryvalidation.org/max-method/
      max: function max(value, element, param) {
        return this.optional(element) || value <= param;
      },
      // https://jqueryvalidation.org/range-method/
      range: function range(value, element, param) {
        return this.optional(element) || value >= param[0] && value <= param[1];
      },
      // https://jqueryvalidation.org/step-method/
      step: function step(value, element, param) {
        var type = $(element).attr("type"),
            errorMessage = "Step attribute on input type " + type + " is not supported.",
            supportedTypes = ["text", "number", "range"],
            re = new RegExp("\\b" + type + "\\b"),
            notSupported = type && !re.test(supportedTypes.join()),
            decimalPlaces = function decimalPlaces(num) {
          var match = ("" + num).match(/(?:\.(\d+))?$/);

          if (!match) {
            return 0;
          } // Number of digits right of decimal point.


          return match[1] ? match[1].length : 0;
        },
            toInt = function toInt(num) {
          return Math.round(num * Math.pow(10, decimals));
        },
            valid = true,
            decimals; // Works only for text, number and range input types
        // TODO find a way to support input types date, datetime, datetime-local, month, time and week


        if (notSupported) {
          throw new Error(errorMessage);
        }

        decimals = decimalPlaces(param); // Value can't have too many decimals

        if (decimalPlaces(value) > decimals || toInt(value) % toInt(param) !== 0) {
          valid = false;
        }

        return this.optional(element) || valid;
      },
      // https://jqueryvalidation.org/equalTo-method/
      equalTo: function equalTo(value, element, param) {
        // Bind to the blur event of the target in order to revalidate whenever the target field is updated
        var target = $(param);

        if (this.settings.onfocusout && target.not(".validate-equalTo-blur").length) {
          target.addClass("validate-equalTo-blur").on("blur.validate-equalTo", function () {
            $(element).valid();
          });
        }

        return value === target.val();
      },
      // https://jqueryvalidation.org/remote-method/
      remote: function remote(value, element, param, method) {
        if (this.optional(element)) {
          return "dependency-mismatch";
        }

        method = typeof method === "string" && method || "remote";
        var previous = this.previousValue(element, method),
            validator,
            data,
            optionDataString;

        if (!this.settings.messages[element.name]) {
          this.settings.messages[element.name] = {};
        }

        previous.originalMessage = previous.originalMessage || this.settings.messages[element.name][method];
        this.settings.messages[element.name][method] = previous.message;
        param = typeof param === "string" && {
          url: param
        } || param;
        optionDataString = $.param($.extend({
          data: value
        }, param.data));

        if (previous.old === optionDataString) {
          return previous.valid;
        }

        previous.old = optionDataString;
        validator = this;
        this.startRequest(element);
        data = {};
        data[element.name] = value;
        $.ajax($.extend(true, {
          mode: "abort",
          port: "validate" + element.name,
          dataType: "json",
          data: data,
          context: validator.currentForm,
          success: function success(response) {
            var valid = response === true || response === "true",
                errors,
                message,
                submitted;
            validator.settings.messages[element.name][method] = previous.originalMessage;

            if (valid) {
              submitted = validator.formSubmitted;
              validator.resetInternals();
              validator.toHide = validator.errorsFor(element);
              validator.formSubmitted = submitted;
              validator.successList.push(element);
              validator.invalid[element.name] = false;
              validator.showErrors();
            } else {
              errors = {};
              message = response || validator.defaultMessage(element, {
                method: method,
                parameters: value
              });
              errors[element.name] = previous.message = message;
              validator.invalid[element.name] = true;
              validator.showErrors(errors);
            }

            previous.valid = valid;
            validator.stopRequest(element, valid);
          }
        }, param));
        return "pending";
      }
    }
  }); // Ajax mode: abort
  // usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
  // if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()

  var pendingRequests = {},
      ajax; // Use a prefilter if available (1.5+)

  if ($.ajaxPrefilter) {
    $.ajaxPrefilter(function (settings, _, xhr) {
      var port = settings.port;

      if (settings.mode === "abort") {
        if (pendingRequests[port]) {
          pendingRequests[port].abort();
        }

        pendingRequests[port] = xhr;
      }
    });
  } else {
    // Proxy ajax
    ajax = $.ajax;

    $.ajax = function (settings) {
      var mode = ("mode" in settings ? settings : $.ajaxSettings).mode,
          port = ("port" in settings ? settings : $.ajaxSettings).port;

      if (mode === "abort") {
        if (pendingRequests[port]) {
          pendingRequests[port].abort();
        }

        pendingRequests[port] = ajax.apply(this, arguments);
        return pendingRequests[port];
      }

      return ajax.apply(this, arguments);
    };
  }

  return $;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "../../node_modules/webpack/buildin/harmony-module.js")(module), __webpack_require__(/*! jquery */ "../../node_modules/jquery/dist/jquery.min.js")))

/***/ }),

/***/ "./utils/throttle.js":
/*!***************************!*\
  !*** ./utils/throttle.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_date_now__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.date.now */ "../../node_modules/core-js/modules/es.date.now.js");
/* harmony import */ var core_js_modules_es_date_now__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_now__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.date.to-string */ "../../node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/web.timers */ "../../node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_2__);




/* eslint-disable no-undefined,no-param-reassign,no-shadow */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 *
 * @param  {Number}    delay
 * A zero-or-greater delay in milliseconds.
 * For event callbacks, values around 100 or 250 (or even higher) are most useful.
 * @param  {Boolean}   [noTrailing]   Optional, defaults to false.
 * If noTrailing is true, callback will only execute every `delay` milliseconds while the
 * throttled-function is being called. If noTrailing is false or unspecified, callback will be executed one final time
 * after the last throttled-function call. (After the throttled-function has not been called for `delay` milliseconds,
 * the internal counter is reset)
 * @param  {Function}  callback
 * A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
 * to `callback` when the throttled-function is executed.
 * @param  {Boolean}   [debounceMode]
 * If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms.
 * If `debounceMode` is false (at end), schedule `callback` to execute after `delay` ms.
 *
 * @return {Function}  A new, throttled, function.
 */
/* harmony default export */ __webpack_exports__["default"] = (function (delay, noTrailing, callback, debounceMode) {
  /*
   * After wrapper has stopped being called, this timeout ensures that
   * `callback` is executed at the proper times in `throttle` and `end`
   * debounce modes.
   */
  var timeoutID;
  var cancelled = false; // Keep track of the last time `callback` was executed.

  var lastExec = 0; // Function to clear existing timeout

  function clearExistingTimeout() {
    if (timeoutID) {
      clearTimeout(timeoutID);
    }
  } // Function to cancel next exec


  function cancel() {
    clearExistingTimeout();
    cancelled = true;
  } // `noTrailing` defaults to falsy.


  if (typeof noTrailing !== 'boolean') {
    debounceMode = callback;
    callback = noTrailing;
    noTrailing = undefined;
  }
  /*
   * The `wrapper` function encapsulates all of the throttling / debouncing
   * functionality and when executed will limit the rate at which `callback`
   * is executed.
   */


  function wrapper() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var self = this;
    var elapsed = Date.now() - lastExec;

    if (cancelled) {
      return;
    } // Execute `callback` and update the `lastExec` timestamp.


    function exec() {
      lastExec = Date.now();
      callback.apply(self, args);
    }
    /*
     * If `debounceMode` is true (at begin) this is used to clear the flag
     * to allow future `callback` executions.
     */


    function clear() {
      timeoutID = undefined;
    }

    if (debounceMode && !timeoutID) {
      /*
       * Since `wrapper` is being called for the first time and
       * `debounceMode` is true (at begin), execute `callback`.
       */
      exec();
    }

    clearExistingTimeout();

    if (debounceMode === undefined && elapsed > delay) {
      /*
       * In throttle mode, if `delay` time has been exceeded, execute
       * `callback`.
       */
      exec();
    } else if (noTrailing !== true) {
      /*
       * In trailing throttle mode, since `delay` time has not been
       * exceeded, schedule `callback` to execute `delay` ms after most
       * recent execution.
       *
       * If `debounceMode` is true (at begin), schedule `clear` to execute
       * after `delay` ms.
       *
       * If `debounceMode` is false (at end), schedule `callback` to
       * execute after `delay` ms.
       */
      timeoutID = setTimeout(debounceMode ? clear : exec, debounceMode === undefined ? delay - elapsed : delay);
    }
  }

  wrapper.cancel = cancel; // Return the wrapper function.

  return wrapper;
});

/***/ }),

/***/ 0:
/*!*******************************!*\
  !*** multi ./polyfills ./app ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./polyfills */"./polyfills.js");
module.exports = __webpack_require__(/*! ./app */"./app.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmJ1bmRsZS5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9ibG9nU2VlTW9yZS5qcyIsIndlYnBhY2s6Ly8vLi9tb2R1bGVzL2Nzc1ZhcnMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9jc3NWYXJzL3Njcm9sbFdpZHRoLmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvY3NzVmFycy92aC5qcyIsIndlYnBhY2s6Ly8vLi9tb2R1bGVzL2Rpc3BhdGNoZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9sYXp5bG9hZC5qcyIsIndlYnBhY2s6Ly8vLi9tb2R1bGVzL21hcC5qcyIsIndlYnBhY2s6Ly8vLi9tb2R1bGVzL21lZGlhLmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvbWVudS5qcyIsIndlYnBhY2s6Ly8vLi9tb2R1bGVzL292ZXJsYXkuanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9yZXNpemUuanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9yZXZpZXdzU2xpZGVyLmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvc2Nyb2xsVG8uanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9zZXJ2aWNlc0ZhbmN5Ym94LmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvc2VydmljZXNTbGlkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9zaXRlUHJlbG9hZGVyLmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvc3Vic2NyaWJlLmpzIiwid2VicGFjazovLy8uL3BvbHlmaWxscy5qcyIsIndlYnBhY2s6Ly8vLi91dGlscy9FdmVudEVtaXR0ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vdXRpbHMvZG9jdW1lbnRMb2FkZWQuanMiLCJ3ZWJwYWNrOi8vLy4vdXRpbHMvZG9jdW1lbnRSZWFkeS5qcyIsIndlYnBhY2s6Ly8vLi91dGlscy9nZXRTY3JvbGxXaWR0aC5qcyIsIndlYnBhY2s6Ly8vLi91dGlscy9pc1RvdWNoRGV2aWNlLmpzIiwid2VicGFjazovLy8uL3V0aWxzL2pxdWVyeVZhbGlkYXRlLmpzIiwid2VicGFjazovLy8uL3V0aWxzL3Rocm90dGxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIGluc3RhbGwgYSBKU09OUCBjYWxsYmFjayBmb3IgY2h1bmsgbG9hZGluZ1xuIFx0ZnVuY3Rpb24gd2VicGFja0pzb25wQ2FsbGJhY2soZGF0YSkge1xuIFx0XHR2YXIgY2h1bmtJZHMgPSBkYXRhWzBdO1xuIFx0XHR2YXIgbW9yZU1vZHVsZXMgPSBkYXRhWzFdO1xuIFx0XHR2YXIgZXhlY3V0ZU1vZHVsZXMgPSBkYXRhWzJdO1xuXG4gXHRcdC8vIGFkZCBcIm1vcmVNb2R1bGVzXCIgdG8gdGhlIG1vZHVsZXMgb2JqZWN0LFxuIFx0XHQvLyB0aGVuIGZsYWcgYWxsIFwiY2h1bmtJZHNcIiBhcyBsb2FkZWQgYW5kIGZpcmUgY2FsbGJhY2tcbiBcdFx0dmFyIG1vZHVsZUlkLCBjaHVua0lkLCBpID0gMCwgcmVzb2x2ZXMgPSBbXTtcbiBcdFx0Zm9yKDtpIDwgY2h1bmtJZHMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHRjaHVua0lkID0gY2h1bmtJZHNbaV07XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGluc3RhbGxlZENodW5rcywgY2h1bmtJZCkgJiYgaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdKSB7XG4gXHRcdFx0XHRyZXNvbHZlcy5wdXNoKGluc3RhbGxlZENodW5rc1tjaHVua0lkXVswXSk7XG4gXHRcdFx0fVxuIFx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXSA9IDA7XG4gXHRcdH1cbiBcdFx0Zm9yKG1vZHVsZUlkIGluIG1vcmVNb2R1bGVzKSB7XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vcmVNb2R1bGVzLCBtb2R1bGVJZCkpIHtcbiBcdFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdID0gbW9yZU1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHRcdH1cbiBcdFx0fVxuIFx0XHRpZihwYXJlbnRKc29ucEZ1bmN0aW9uKSBwYXJlbnRKc29ucEZ1bmN0aW9uKGRhdGEpO1xuXG4gXHRcdHdoaWxlKHJlc29sdmVzLmxlbmd0aCkge1xuIFx0XHRcdHJlc29sdmVzLnNoaWZ0KCkoKTtcbiBcdFx0fVxuXG4gXHRcdC8vIGFkZCBlbnRyeSBtb2R1bGVzIGZyb20gbG9hZGVkIGNodW5rIHRvIGRlZmVycmVkIGxpc3RcbiBcdFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2guYXBwbHkoZGVmZXJyZWRNb2R1bGVzLCBleGVjdXRlTW9kdWxlcyB8fCBbXSk7XG5cbiBcdFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiBhbGwgY2h1bmtzIHJlYWR5XG4gXHRcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIFx0fTtcbiBcdGZ1bmN0aW9uIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCkge1xuIFx0XHR2YXIgcmVzdWx0O1xuIFx0XHRmb3IodmFyIGkgPSAwOyBpIDwgZGVmZXJyZWRNb2R1bGVzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0dmFyIGRlZmVycmVkTW9kdWxlID0gZGVmZXJyZWRNb2R1bGVzW2ldO1xuIFx0XHRcdHZhciBmdWxmaWxsZWQgPSB0cnVlO1xuIFx0XHRcdGZvcih2YXIgaiA9IDE7IGogPCBkZWZlcnJlZE1vZHVsZS5sZW5ndGg7IGorKykge1xuIFx0XHRcdFx0dmFyIGRlcElkID0gZGVmZXJyZWRNb2R1bGVbal07XG4gXHRcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbZGVwSWRdICE9PSAwKSBmdWxmaWxsZWQgPSBmYWxzZTtcbiBcdFx0XHR9XG4gXHRcdFx0aWYoZnVsZmlsbGVkKSB7XG4gXHRcdFx0XHRkZWZlcnJlZE1vZHVsZXMuc3BsaWNlKGktLSwgMSk7XG4gXHRcdFx0XHRyZXN1bHQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IGRlZmVycmVkTW9kdWxlWzBdKTtcbiBcdFx0XHR9XG4gXHRcdH1cblxuIFx0XHRyZXR1cm4gcmVzdWx0O1xuIFx0fVxuXG4gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBvYmplY3QgdG8gc3RvcmUgbG9hZGVkIGFuZCBsb2FkaW5nIGNodW5rc1xuIFx0Ly8gdW5kZWZpbmVkID0gY2h1bmsgbm90IGxvYWRlZCwgbnVsbCA9IGNodW5rIHByZWxvYWRlZC9wcmVmZXRjaGVkXG4gXHQvLyBQcm9taXNlID0gY2h1bmsgbG9hZGluZywgMCA9IGNodW5rIGxvYWRlZFxuIFx0dmFyIGluc3RhbGxlZENodW5rcyA9IHtcbiBcdFx0XCJhcHBcIjogMFxuIFx0fTtcblxuIFx0dmFyIGRlZmVycmVkTW9kdWxlcyA9IFtdO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcImpzL1wiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbMCxcInZlbmRvcnNcIl0pO1xuIFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiByZWFkeVxuIFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4iLCJpbXBvcnQgJy4vbW9kdWxlcy9zaXRlUHJlbG9hZGVyJztcbmltcG9ydCBkb2N1bWVudFJlYWR5IGZyb20gJy4vdXRpbHMvZG9jdW1lbnRSZWFkeSc7XG5pbXBvcnQgZG9jdW1lbnRMb2FkZWQgZnJvbSAnLi91dGlscy9kb2N1bWVudExvYWRlZCc7XG5pbXBvcnQganF1ZXJ5VmFsaWRhdGUgZnJvbSAnLi91dGlscy9qcXVlcnlWYWxpZGF0ZSc7XG5cbmltcG9ydCBjc3NWYXJzIGZyb20gJy4vbW9kdWxlcy9jc3NWYXJzJztcbmltcG9ydCByZXNpemUgZnJvbSAnLi9tb2R1bGVzL3Jlc2l6ZSc7XG5pbXBvcnQgbGF6eWxvYWQgZnJvbSAnLi9tb2R1bGVzL2xhenlsb2FkJztcbmltcG9ydCBtZW51IGZyb20gJy4vbW9kdWxlcy9tZW51JztcbmltcG9ydCBzY3JvbGxUbyBmcm9tICcuL21vZHVsZXMvc2Nyb2xsVG8nO1xuaW1wb3J0IHNlcnZpY2VzU2xpZGVyIGZyb20gJy4vbW9kdWxlcy9zZXJ2aWNlc1NsaWRlcic7XG5pbXBvcnQgc2VydmljZXNGYW5jeWJveCBmcm9tICcuL21vZHVsZXMvc2VydmljZXNGYW5jeWJveCc7XG5pbXBvcnQgbWFwIGZyb20gJy4vbW9kdWxlcy9tYXAnO1xuaW1wb3J0IHJldmlld3NTbGlkZXIgZnJvbSAnLi9tb2R1bGVzL3Jldmlld3NTbGlkZXInO1xuaW1wb3J0IG92ZXJsYXkgZnJvbSAnLi9tb2R1bGVzL292ZXJsYXknO1xuaW1wb3J0IHN1YnNjcmliZSBmcm9tICcuL21vZHVsZXMvc3Vic2NyaWJlJztcbmltcG9ydCBibG9nU2VlTW9yZSBmcm9tICcuL21vZHVsZXMvYmxvZ1NlZU1vcmUnO1xuXG5cblxuXG5cbmRvY3VtZW50UmVhZHkoKCkgPT4ge1xuICBjc3NWYXJzLmluaXQoKTtcbiAgcmVzaXplLmluaXQoKTtcbiAgbGF6eWxvYWQuaW5pdCgpO1xuICBtZW51LmluaXQoKTtcbiAgc2Nyb2xsVG8uaW5pdCgpO1xuICBqcXVlcnlWYWxpZGF0ZS5pbml0KCk7XG5cblxuXG5cblxufSk7XG5cbmRvY3VtZW50TG9hZGVkKCgpID0+IHtcblxufSk7XG4iLCIkKGZ1bmN0aW9uICgpIHtcbiAgJCgnLnMtYmxvZ19fYnV0dG9uJykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICQoJy5zLWJsb2dfX2l0ZW1zIC5zLWJsb2dfX2l0ZW06aGlkZGVuJykuc2xpY2UoMCwgMykuc2hvdygpLmNzcygnZGlzcGxheScsICdmbGV4Jyk7XG4gICAgaWYgKCQoJy5zLWJsb2dfX2l0ZW1zIC5zLWJsb2dfX2l0ZW0nKS5sZW5ndGggPT0gJCgnLnMtYmxvZ19faXRlbXMgLnMtYmxvZ19faXRlbTp2aXNpYmxlJykubGVuZ3RoKSB7XG4gICAgICAkKCcucy1ibG9nX19idXR0b24nKS5oaWRlKCk7XG4gICAgfVxuICB9KTtcbn0pO1xuXG4iLCJpbXBvcnQgdmggZnJvbSAnLi92aCc7XG5pbXBvcnQgc2Nyb2xsV2lkdGggZnJvbSAnLi9zY3JvbGxXaWR0aCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgaW5pdCgpIHtcbiAgICB2aC5pbml0KCk7XG4gICAgc2Nyb2xsV2lkdGguaW5pdCgpO1xuICB9LFxufTtcbiIsImltcG9ydCBnZXRTY3JvbGxXaWR0aCBmcm9tICcuLi8uLi91dGlscy9nZXRTY3JvbGxXaWR0aCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgaW5pdCgpIHtcbiAgICB0aGlzLmNhbGN1bGF0ZSgpO1xuICB9LFxuICBjYWxjdWxhdGUoKSB7XG4gICAgY29uc3Qgc2Nyb2xsV2lkdGggPSBnZXRTY3JvbGxXaWR0aCgpO1xuICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zdHlsZS5zZXRQcm9wZXJ0eSgnLS1zY3JvbGwtd2lkdGgnLCBgJHsgc2Nyb2xsV2lkdGggfXB4YCk7XG4gIH0sXG59O1xuIiwiaW1wb3J0IGRpc3BhdGNoZXIgZnJvbSAnLi4vZGlzcGF0Y2hlcic7XG5pbXBvcnQgaXNUb3VjaERldmljZSBmcm9tICcuLi8uLi91dGlscy9pc1RvdWNoRGV2aWNlJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBpbml0KCkge1xuICAgIHRoaXMuY2FsY3VsYXRlKCk7XG4gICAgdGhpcy5oYW5kbGVSZXNpemUoKTtcbiAgfSxcbiAgY2FsY3VsYXRlKCkge1xuICAgIGNvbnN0IHZoID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCAqIDAuMDE7XG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlLnNldFByb3BlcnR5KCctLXZoJywgYCR7IHZoIH1weGApO1xuICB9LFxuICBoYW5kbGVSZXNpemUoKSB7XG4gICAgZGlzcGF0Y2hlci5zdWJzY3JpYmUoKHsgdHlwZSB9KSA9PiB7XG4gICAgICBjb25zdCB0YXJnZXRUeXBlID0gaXNUb3VjaERldmljZSgpID8gJ3Jlc2l6ZTpib3RoJyA6ICdyZXNpemU6aGVpZ2h0JztcblxuICAgICAgaWYgKHR5cGUgPT09IHRhcmdldFR5cGUpIHtcbiAgICAgICAgdGhpcy5jYWxjdWxhdGUoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSxcbn07XG4iLCJpbXBvcnQgRXZlbnRFbWl0dGVyIGZyb20gJy4uL3V0aWxzL0V2ZW50RW1pdHRlcic7XG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiIsImltcG9ydCAnY29yZS1qcy9mZWF0dXJlcy9vYmplY3QvYXNzaWduJztcbmltcG9ydCAnaW50ZXJzZWN0aW9uLW9ic2VydmVyJzsgLy8gcG9seWZpbGxcbmltcG9ydCBsb3phZCBmcm9tICdsb3phZCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgaW5pdChyb290ID0gZG9jdW1lbnQpIHtcbiAgICBjb25zdCBvcHRpb25zID0ge1xuICAgICAgcm9vdE1hcmdpbjogYCR7ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodH1weCAwcHhgLFxuICAgIH07XG5cbiAgICBjb25zdCBwaWN0dXJlcyA9IHJvb3QucXVlcnlTZWxlY3RvckFsbCgnLmpzLWxhenktaW1nOm5vdChbZGF0YS1sb2FkZWRdKScpO1xuICAgIGNvbnN0IGJhY2tncm91bmRzID0gcm9vdC5xdWVyeVNlbGVjdG9yQWxsKCcuanMtbGF6eS1iZzpub3QoW2RhdGEtbG9hZGVkXSknKTtcblxuICAgIGlmIChwaWN0dXJlcy5sZW5ndGgpIHtcbiAgICAgIGNvbnN0IHBpY3R1cmVPYnNlcnZlciA9IGxvemFkKHBpY3R1cmVzLCBvcHRpb25zKTtcbiAgICAgIHBpY3R1cmVPYnNlcnZlci5vYnNlcnZlKCk7XG4gICAgfVxuXG4gICAgaWYgKGJhY2tncm91bmRzLmxlbmd0aCkge1xuICAgICAgY29uc3QgYmFja2dyb3VuZE9ic2VydmVyID0gbG96YWQoYmFja2dyb3VuZHMsIG9wdGlvbnMpO1xuICAgICAgYmFja2dyb3VuZE9ic2VydmVyLm9ic2VydmUoKTtcbiAgICB9XG5cbiAgICB0aGlzLmxhenlWaWRlbyhyb290KTtcbiAgfSxcblxuICBsYXp5VmlkZW8ocm9vdCA9IGRvY3VtZW50KSB7XG4gICAgY29uc3QgbGF6eVZpZGVvcyA9IFsuLi5yb290LnF1ZXJ5U2VsZWN0b3JBbGwoJ3ZpZGVvLmpzLWxhenktdmlkZW86bm90KFtkYXRhLWxvYWRlZF0pJyldO1xuICAgIGNvbnN0IG9ic2VydmVyT3B0aW9ucyA9IHtcbiAgICAgIHJvb3RNYXJnaW46IGAke2RvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHR9cHggMHB4YCxcbiAgICB9O1xuXG4gICAgaWYgKCdJbnRlcnNlY3Rpb25PYnNlcnZlcicgaW4gd2luZG93KSB7XG4gICAgICBjb25zdCBsYXp5VmlkZW9PYnNlcnZlciA9IG5ldyBJbnRlcnNlY3Rpb25PYnNlcnZlcigoZW50cmllcykgPT4ge1xuICAgICAgICBlbnRyaWVzLmZvckVhY2goKHZpZGVvKSA9PiB7XG4gICAgICAgICAgaWYgKHZpZGVvLmlzSW50ZXJzZWN0aW5nKSB7XG4gICAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcmVzdHJpY3RlZC1zeW50YXgsZ3VhcmQtZm9yLWluXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHNvdXJjZSBpbiB2aWRlby50YXJnZXQuY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHByZWZlci1kZXN0cnVjdHVyaW5nXG4gICAgICAgICAgICAgIGNvbnN0IHZpZGVvU291cmNlID0gdmlkZW8udGFyZ2V0LmNoaWxkcmVuW3NvdXJjZV07XG4gICAgICAgICAgICAgIGlmICh0eXBlb2YgdmlkZW9Tb3VyY2UudGFnTmFtZSA9PT0gJ3N0cmluZycgJiYgdmlkZW9Tb3VyY2UudGFnTmFtZSA9PT0gJ1NPVVJDRScpIHtcbiAgICAgICAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgcHJlZmVyLWRlc3RydWN0dXJpbmdcbiAgICAgICAgICAgICAgICB2aWRlb1NvdXJjZS5zcmMgPSB2aWRlb1NvdXJjZS5kYXRhc2V0LnNyYztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2aWRlby50YXJnZXQubG9hZCgpO1xuICAgICAgICAgICAgdmlkZW8udGFyZ2V0LnNldEF0dHJpYnV0ZSgnZGF0YS1sb2FkZWQnLCAndHJ1ZScpO1xuICAgICAgICAgICAgbGF6eVZpZGVvT2JzZXJ2ZXIudW5vYnNlcnZlKHZpZGVvLnRhcmdldCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0sIG9ic2VydmVyT3B0aW9ucyk7XG5cbiAgICAgIGxhenlWaWRlb3MuZm9yRWFjaCgobGF6eVZpZGVvKSA9PiB7XG4gICAgICAgIGxhenlWaWRlb09ic2VydmVyLm9ic2VydmUobGF6eVZpZGVvKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSxcbn07XG4iLCJkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcbiAgdmFyIG1hcCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzLW1hcF93cmFwIGlmcmFtZScpXG4gIGlmKGUudGFyZ2V0LmlkID09PSAncy1tYXBfd3JhcCcpIHtcbiAgICBtYXAuc3R5bGUucG9pbnRlckV2ZW50cyA9ICdhbGwnO1xuICB9IGVsc2Uge1xuICAgIG1hcC5zdHlsZS5wb2ludGVyRXZlbnRzID0gJ25vbmUnO1xuICB9XG59KTtcbiIsImNvbnN0IHdpZHRoID0ge1xuICB3aWRlRGVza3RvcDogMTkyMCxcbiAgZGVza3RvcDogMTI4MCxcbiAgbG93RGVza3RvcDogMTAyNCxcbiAgdGFibGV0OiA3NjgsXG59O1xuXG5mdW5jdGlvbiB3aWRlRGVza3RvcE1pbigpIHtcbiAgcmV0dXJuIHdpbmRvdy5tYXRjaE1lZGlhKGAobWluLXdpZHRoOiAkeyB3aWR0aC53aWRlRGVza3RvcCB9cHgpYCkubWF0Y2hlcztcbn1cblxuZnVuY3Rpb24gbG93RGVza3RvcE1pbigpIHtcbiAgcmV0dXJuIHdpbmRvdy5tYXRjaE1lZGlhKGAobWluLXdpZHRoOiAkeyB3aWR0aC5sb3dEZXNrdG9wIH1weClgKS5tYXRjaGVzO1xufVxuXG5mdW5jdGlvbiB0YWJsZXQoKSB7XG4gIHJldHVybiB3aW5kb3cubWF0Y2hNZWRpYShgKG1heC13aWR0aDogJHsgd2lkdGgubG93RGVza3RvcCAtIDEgfXB4KWApLm1hdGNoZXM7XG59XG5cbmZ1bmN0aW9uIG1vYmlsZSgpIHtcbiAgcmV0dXJuIHdpbmRvdy5tYXRjaE1lZGlhKGAobWF4LXdpZHRoOiAkeyB3aWR0aC50YWJsZXQgLSAxIH1weClgKS5tYXRjaGVzO1xufVxuXG5leHBvcnQge1xuICB3aWR0aCBhcyBzY3JlZW5XaWR0aCxcbiAgd2lkZURlc2t0b3BNaW4sXG4gIGxvd0Rlc2t0b3BNaW4sXG4gIHRhYmxldCxcbiAgbW9iaWxlLFxufTtcbiIsImltcG9ydCB0aHJvdHRsZSBmcm9tICcuLi91dGlscy90aHJvdHRsZSc7XG5pbXBvcnQgeyBkaXNhYmxlQm9keVNjcm9sbCwgZW5hYmxlQm9keVNjcm9sbCB9IGZyb20gJ2JvZHktc2Nyb2xsLWxvY2snO1xuaW1wb3J0IHsgbG93RGVza3RvcE1pbiB9IGZyb20gJy4vbWVkaWEnO1xuaW1wb3J0IGRpc3BhdGNoZXIgZnJvbSAnLi9kaXNwYXRjaGVyJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBjbGFzc2VzOiB7XG4gICAgbWVudU9wZW46ICdfc2l0ZS1tZW51LW9wZW4nLFxuICB9LFxuICBlbDoge1xuICAgIGJvZHk6ICQoJ2JvZHknKSxcbiAgICBzaXRlTWVudUNvbnRhaW5lcjogJCgnLnNpdGUtbWVudV9fY29udGFpbmVyJyksXG4gIH0sXG4gIGluaXQoKSB7XG4gICAgdGhpcy5oYW5kbGVycygpO1xuICAgIHRoaXMuaGFuZGxlRGlzcGF0Y2hlcigpO1xuICB9LFxuICBoYW5kbGVycygpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICBjb25zdCB7IGNsYXNzZXMsIGVsIH0gPSBzZWxmO1xuXG4gICAgJChkb2N1bWVudClcbiAgICAgIC5vbignY2xpY2snLCAnLmJ1cmdlcicsIHRocm90dGxlKDUwMCwgZmFsc2UsICgpID0+IHtcbiAgICAgICAgZWwuYm9keS5oYXNDbGFzcyhjbGFzc2VzLm1lbnVPcGVuKSA/IHNlbGYuY2xvc2UoKSA6IHNlbGYub3BlbigpO1xuICAgICAgfSkpXG4gICAgICAub24oJ2NsaWNrJywgJy5zaXRlLW1lbnVfX2l0ZW0nLCB0aHJvdHRsZSg1MDAsIGZhbHNlLCAoKSA9PiB7XG4gICAgICAgIGVsLmJvZHkuaGFzQ2xhc3MoY2xhc3Nlcy5tZW51T3BlbikgPyBzZWxmLmNsb3NlKCkgOiBzZWxmLm9wZW4oKTtcbiAgICAgIH0pKVxuICAgICAgLm9uKCdjbGljaycsICcuc2l0ZS1tZW51X19iZycsIHRocm90dGxlKDUwMCwgZmFsc2UsICgpID0+IHtcbiAgICAgICAgc2VsZi5jbG9zZSgpO1xuICAgICAgfSkpXG4gICAgICAub24oJ2NsaWNrJywgJy5zaXRlLW1lbnVfX21vYmlsZS1idXR0b24nLCB0aHJvdHRsZSg1MDAsIGZhbHNlLCAoKSA9PiB7XG4gICAgICAgIHNlbGYuY2xvc2UoKTtcbiAgICAgIH0pKTtcbiAgfSxcbiAgb3BlbigpIHtcbiAgICB0aGlzLmVsLmJvZHkuYWRkQ2xhc3ModGhpcy5jbGFzc2VzLm1lbnVPcGVuKTtcbiAgfSxcbiAgY2xvc2UoKSB7XG4gICAgdGhpcy5lbC5ib2R5LnJlbW92ZUNsYXNzKHRoaXMuY2xhc3Nlcy5tZW51T3Blbik7XG4gIH0sXG4gIGhhbmRsZURpc3BhdGNoZXIoKSB7XG4gICAgZGlzcGF0Y2hlci5zdWJzY3JpYmUoKHsgdHlwZSB9KSA9PiB7XG4gICAgICBpZiAodHlwZSA9PT0gJ3NpdGUtbWVudTpvcGVuJykge1xuICAgICAgICB0aGlzLm9wZW4oKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHR5cGUgPT09ICdzaXRlLW1lbnU6Y2xvc2UnKSB7XG4gICAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICAgIH1cblxuICAgICAgaWYgKCh0eXBlID09PSAncmVzaXplOndpZHRoJykgJiYgKGxvd0Rlc2t0b3BNaW4oKSkpIHtcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9LFxufTtcbiIsInZhciBwb3B1cCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNvdmVybGF5Jyk7XG52YXIgbWVudUJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zaXRlLWhlYWRlcl9faWNvbnMtYnV0dG9uJyk7XG52YXIgbWVudU1vYmlsZUJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zaXRlLW1lbnVfX21vYmlsZS1idXR0b24nKTtcbm1lbnVCdXR0b24uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uIChlKSB7XG4gIHBvcHVwLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xufSk7XG5tZW51TW9iaWxlQnV0dG9uLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoZSkge1xuICBwb3B1cC5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcbn0pO1xuXG4iLCJpbXBvcnQgZGlzcGF0Y2hlciBmcm9tICcuL2Rpc3BhdGNoZXInO1xuaW1wb3J0IHRocm90dGxlIGZyb20gJy4uL3V0aWxzL3Rocm90dGxlJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBzaXplOiB7XG4gICAgd2lkdGg6IHdpbmRvdy5pbm5lcldpZHRoLFxuICAgIGhlaWdodDogd2luZG93LmlubmVySGVpZ2h0LFxuICB9LFxuICBpbml0KCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRocm90dGxlKDMwMCwgZmFsc2UsICgpID0+IHtcbiAgICAgIHNlbGYuaGFuZGxlUmVzaXplKCk7XG4gICAgfSksIGZhbHNlKTtcblxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdvcmllbnRhdGlvbmNoYW5nZScsIHRocm90dGxlKDMwMCwgZmFsc2UsICgpID0+IHtcbiAgICAgIHNlbGYuaGFuZGxlUmVzaXplKCk7XG4gICAgfSksIGZhbHNlKTtcbiAgfSxcbiAgaGFuZGxlUmVzaXplKCkge1xuICAgIC8qIGVzbGludC1kaXNhYmxlIHByZWZlci1kZXN0cnVjdHVyaW5nICovXG4gICAgY29uc3Qgd2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aDtcbiAgICBjb25zdCBoZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XG4gICAgLyogZXNsaW50LWVuYWJsZSBwcmVmZXItZGVzdHJ1Y3R1cmluZyAqL1xuXG4gICAgY29uc3Qgd2lkdGhDaGFuZ2VkID0gd2lkdGggIT09IHRoaXMuc2l6ZS53aWR0aDtcbiAgICBjb25zdCBoZWlnaHRDaGFuZ2VkID0gaGVpZ2h0ICE9PSB0aGlzLnNpemUuaGVpZ2h0O1xuXG4gICAgaWYgKHdpZHRoQ2hhbmdlZCkge1xuICAgICAgZGlzcGF0Y2hlci5kaXNwYXRjaCh7XG4gICAgICAgIHR5cGU6ICdyZXNpemU6d2lkdGgnLFxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKGhlaWdodENoYW5nZWQpIHtcbiAgICAgIGRpc3BhdGNoZXIuZGlzcGF0Y2goe1xuICAgICAgICB0eXBlOiAncmVzaXplOmhlaWdodCcsXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZiAod2lkdGhDaGFuZ2VkICYmIGhlaWdodENoYW5nZWQpIHtcbiAgICAgIGRpc3BhdGNoZXIuZGlzcGF0Y2goe1xuICAgICAgICB0eXBlOiAncmVzaXplOmJvdGgnLFxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZGlzcGF0Y2hlci5kaXNwYXRjaCh7XG4gICAgICB0eXBlOiAncmVzaXplOmRlZmF1bHQnLFxuICAgIH0pO1xuXG4gICAgdGhpcy5zaXplID0ge1xuICAgICAgd2lkdGgsXG4gICAgICBoZWlnaHQsXG4gICAgfTtcbiAgfSxcbn07XG4iLCIvLyBpbXBvcnQgU3dpcGVyIGJ1bmRsZSB3aXRoIGFsbCBtb2R1bGVzIGluc3RhbGxlZFxuaW1wb3J0IFN3aXBlciBmcm9tICdzd2lwZXIvYnVuZGxlJztcblxuXG52YXIgc3dpcGVyID0gbmV3IFN3aXBlcihcIi5zLXJldmlld3NfX3N3aXBlclwiLCB7XG4gIHNsaWRlc1BlclZpZXc6IDMsXG4gIHNwYWNlQmV0d2VlbjogMzAsXG4gIHBhZ2luYXRpb246IHtcbiAgICBlbDogXCIuc3dpcGVyLXBhZ2luYXRpb25cIixcbiAgICBjbGlja2FibGU6IHRydWUsXG4gIH0sXG4gIGJyZWFrcG9pbnRzOiB7XG4gICAgMzIwOiB7XG4gICAgICBzbGlkZXNQZXJWaWV3OiAxLFxuICAgIH0sXG4gICAgNTkwOiB7XG4gICAgICBzbGlkZXNQZXJWaWV3OiAyLFxuICAgIH0sXG4gICAgNzY4OiB7XG4gICAgICBzbGlkZXNQZXJWaWV3OiAzLFxuICAgIH1cbiAgfSxcbn0pO1xuIiwiaW1wb3J0IGRpc3BhdGNoZXIgZnJvbSAnLi9kaXNwYXRjaGVyJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBpbml0KCkge1xuICAgIHRoaXMuc2Nyb2xsQnlIYXNoKCk7XG4gICAgdGhpcy5oYW5kbGVFdmVudHMoKTtcbiAgICB0aGlzLmhhbmRsZURpc3BhdGNoZXIoKTtcbiAgfSxcblxuICBoYW5kbGVEaXNwYXRjaGVyKCkge1xuICAgIGRpc3BhdGNoZXIuc3Vic2NyaWJlKChlKSA9PiB7XG4gICAgICBpZiAoZS50eXBlID09PSAnc2Nyb2xsVG86c2Nyb2xsJykge1xuICAgICAgICB0aGlzLnNjcm9sbFRvKGUuc2VsZWN0b3IsIHtcbiAgICAgICAgICBvZmZzZXQ6IGUub2Zmc2V0LFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSxcblxuICBoYW5kbGVFdmVudHMoKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICAkKCcuanMtc2Nyb2xsLXRvJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGNvbnN0IHNlbGVjdG9yID0gJCh0aGlzKS5hdHRyKCdkYXRhLXRhcmdldCcpO1xuICAgICAgY29uc3Qgc3BlZWQgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtc3BlZWQnKTtcbiAgICAgIGNvbnN0IG9mZnNldCA9ICQodGhpcykuYXR0cignZGF0YS1vZmZzZXQnKTtcblxuICAgICAgc2VsZi5zY3JvbGxUbyhzZWxlY3Rvciwge1xuICAgICAgICBzcGVlZCxcbiAgICAgICAgb2Zmc2V0LFxuICAgICAgfSk7XG4gICAgfSk7XG4gIH0sXG5cbiAgc2Nyb2xsVG8oc2VsZWN0b3IsIHsgc3BlZWQgPSA4MDAsIG9mZnNldCA9IDAgfSA9IHt9KSB7XG4gICAgY29uc3QgJHRhcmdldCA9ICQoc2VsZWN0b3IpO1xuICAgIGlmICghJHRhcmdldC5sZW5ndGgpIHJldHVybjtcblxuICAgIGRpc3BhdGNoZXIuZGlzcGF0Y2goe1xuICAgICAgdHlwZTogJ3NpZGViYXI6Y2xvc2UnLFxuICAgIH0pO1xuXG4gICAgJChbZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCBkb2N1bWVudC5ib2R5XSkuYW5pbWF0ZSh7XG4gICAgICBzY3JvbGxUb3A6ICR0YXJnZXQub2Zmc2V0KCkudG9wIC0gb2Zmc2V0LFxuICAgIH0sIHNwZWVkKTtcbiAgfSxcblxuICBzY3JvbGxCeUhhc2goKSB7XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHByZWZlci1kZXN0cnVjdHVyaW5nXG4gICAgY29uc3QgeyBoYXNoIH0gPSB3aW5kb3cubG9jYXRpb247XG5cbiAgICBpZiAoaGFzaC5sZW5ndGgpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHRoaXMuc2Nyb2xsVG8oaGFzaCk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIGNvbnNvbGUud2FybihlLm1lc3NhZ2UpO1xuICAgICAgfVxuICAgIH1cbiAgfSxcbn07XG4iLCIvLyBSZXF1aXJlIGpRdWVyeSAoRmFuY3lib3ggZGVwZW5kZW5jeSlcbndpbmRvdy4kID0gd2luZG93LmpRdWVyeSA9IHJlcXVpcmUoJ2pxdWVyeScpO1xuXG4vLyBGYW5jeWJveFxuY29uc3QgZmFuY3lib3ggPSByZXF1aXJlKCdAZmFuY3lhcHBzL2ZhbmN5Ym94Jyk7XG5cbiIsIi8vIGltcG9ydCBTd2lwZXIgYnVuZGxlIHdpdGggYWxsIG1vZHVsZXMgaW5zdGFsbGVkXG5pbXBvcnQgU3dpcGVyIGZyb20gJ3N3aXBlci9idW5kbGUnO1xuXG5sZXQgc3dpcGVyID0gbmV3IFN3aXBlcignLm15U3dpcGVyJywge1xuICBzcGFjZUJldHdlZW46IDAsXG4gIHNsaWRlc1BlclZpZXc6IDIsXG4gIGZyZWVNb2RlOiB0cnVlLFxuICB3YXRjaFNsaWRlc1Zpc2liaWxpdHk6IHRydWUsXG4gIHdhdGNoU2xpZGVzUHJvZ3Jlc3M6IHRydWUsXG4gIHNsaWRlc1BlckNvbHVtbjogMixcbiAgb2JzZXJ2ZXI6IHRydWUsXG4gIG9ic2VydmVQYXJlbnRzOiB0cnVlLFxuICBvYnNlcnZlU2xpZGVDaGlsZHJlbjogdHJ1ZSxcblxuICBicmVha3BvaW50czoge1xuICAgIDMyMDoge1xuICAgICAgc2xpZGVzUGVyVmlldzogMixcbiAgICAgIHNsaWRlc1BlckNvbHVtbjogMSxcbiAgICB9LFxuICAgIDM2MDoge1xuICAgICAgc2xpZGVzUGVyVmlldzogMyxcbiAgICAgIHNsaWRlc1BlckNvbHVtbjogMSxcbiAgICB9LFxuICAgIDQ5MDoge1xuICAgICAgc2xpZGVzUGVyVmlldzogNCxcbiAgICAgIHNsaWRlc1BlckNvbHVtbjogMSxcbiAgICB9LFxuICAgIDc2ODoge1xuICAgICAgc2xpZGVzUGVyQ29sdW1uOiAyLFxuICAgIH1cbiAgfSxcblxuICBuYXZpZ2F0aW9uOiB7XG4gICAgbmV4dEVsOiAnLnMtc2VydmljZXNfX25hdi1yaWdodCcsXG4gICAgcHJldkVsOiAnLnMtc2VydmljZXNfX25hdi1sZWZ0JyxcbiAgfSxcbiAgdGh1bWJzOiB7XG4gICAgc3dpcGVyOiBzd2lwZXIsXG4gIH0sXG5cbn0pO1xubGV0IHN3aXBlcjIgPSBuZXcgU3dpcGVyKCcubXlTd2lwZXIyJywge1xuICBzcGFjZUJldHdlZW46IDEwLFxuICBvYnNlcnZlcjp0cnVlLFxuICBvYnNlcnZlUGFyZW50czogdHJ1ZSxcbiAgb2JzZXJ2ZVNsaWRlQ2hpbGRyZW46IHRydWUsXG5cbiAgbmF2aWdhdGlvbjoge1xuICAgIG5leHRFbDogJy5zLXNlcnZpY2VzX19tb2JpbGUtYnV0dG9uLW5leHQnLFxuICAgIHByZXZFbDogJy5zLXNlcnZpY2VzX19tb2JpbGUtYnV0dG9uLXByZXYnLFxuICB9LFxuICB0aHVtYnM6IHtcbiAgICBzd2lwZXI6IHN3aXBlcixcbiAgfSxcbn0pO1xuXG4kKCcucy1zZXJ2aWNlc19fZGlyZWN0aW9uIHNwYW4nKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gIGxldCBmaWx0ZXIgPSAkKHRoaXMpLmh0bWwoKS50b0xvd2VyQ2FzZSgpO1xuICBsZXQgc2xpZGVzeGNvbDtcbiAgJCgnLnMtc2VydmljZXNfX2RpcmVjdGlvbiBzcGFuJykucmVtb3ZlQ2xhc3MoJ3Mtc2VydmljZXNfZGlyZWN0aW9uLWFjdGl2ZScpO1xuICAkKHRoaXMpLmFkZENsYXNzKCdzLXNlcnZpY2VzX2RpcmVjdGlvbi1hY3RpdmUnKTtcbiAgaWYgKGZpbHRlciA9PSAnYWxsJykge1xuXG4gICAgJCgnW2RhdGEtZmlsdGVyXScpXG4gICAgICAucmVtb3ZlQ2xhc3MoJ25vbi1zd2lwZXItc2xpZGUnKVxuICAgICAgLmFkZENsYXNzKCdzd2lwZXItc2xpZGUnKVxuICAgICAgLnNob3coKTtcbiAgICBpZigkKCcuc3dpcGVyLXNsaWRlJykubGVuZ3RoID4gMilcbiAgICAgIHNsaWRlc3hjb2wgPSAyO1xuICAgIGVsc2Ugc2xpZGVzeGNvbCA9IDE7XG4gICAgc3dpcGVyLmRlc3Ryb3koKTtcbiAgICBzd2lwZXIgPSBuZXcgU3dpcGVyKCcubXlTd2lwZXInLCB7XG4gICAgICBzbGlkZXNQZXJWaWV3OiAyLFxuICAgICAgc2xpZGVzUGVyQ29sdW1uOjIsXG4gICAgICBzbGlkZXNQZXJDb2x1bW46IHNsaWRlc3hjb2wsXG4gICAgICBwYWdpbmF0aW9uQ2xpY2thYmxlOiB0cnVlLFxuICAgICAgc3BhY2VCZXR3ZWVuOiAwLFxuICAgICAgYnJlYWtwb2ludHM6IHtcbiAgICAgICAgMzIwOiB7XG4gICAgICAgICAgc2xpZGVzUGVyVmlldzogMixcbiAgICAgICAgICBzbGlkZXNQZXJDb2x1bW46MSxcbiAgICAgICAgfSxcbiAgICAgICAgMzYwOiB7XG4gICAgICAgICAgc2xpZGVzUGVyVmlldzogMyxcbiAgICAgICAgICBzbGlkZXNQZXJDb2x1bW46MSxcbiAgICAgICAgfSxcbiAgICAgICAgNDkwOiB7XG4gICAgICAgICAgc2xpZGVzUGVyVmlldzogNCxcbiAgICAgICAgICBzbGlkZXNQZXJDb2x1bW46MSxcbiAgICAgICAgfSxcbiAgICAgICAgNzY4OiB7XG4gICAgICAgICAgc2xpZGVzUGVyQ29sdW1uOnNsaWRlc3hjb2wsXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBuYXZpZ2F0aW9uOiB7XG4gICAgICAgIG5leHRFbDogJy5zLXNlcnZpY2VzX19uYXYtcmlnaHQnLFxuICAgICAgICBwcmV2RWw6ICcucy1zZXJ2aWNlc19fbmF2LWxlZnQnLFxuICAgICAgfVxuICAgIH0pO1xuICAgIHN3aXBlcjIuZGVzdHJveSgpO1xuICAgIHN3aXBlcjIgPSBuZXcgU3dpcGVyKCcubXlTd2lwZXIyJywge1xuICAgICAgc3BhY2VCZXR3ZWVuOiAxMCxcbiAgICAgIHRodW1iczoge1xuICAgICAgICBzd2lwZXI6IHN3aXBlcixcbiAgICAgIH0sXG4gICAgICBuYXZpZ2F0aW9uOiB7XG4gICAgICAgIG5leHRFbDogJy5zLXNlcnZpY2VzX19tb2JpbGUtYnV0dG9uLW5leHQnLFxuICAgICAgICBwcmV2RWw6ICcucy1zZXJ2aWNlc19fbW9iaWxlLWJ1dHRvbi1wcmV2JyxcbiAgICAgIH0sXG4gICAgfSk7XG4gICAgLypuYXYtYm94LWhpZGUqL1xuICAgIGlmICgkKCcuc3dpcGVyLXdyYXBwZXItdGh1bWJzIC5zd2lwZXItc2xpZGUnKS5sZW5ndGggPT09IDApIHtcbiAgICAgICQoJy5zLXNlcnZpY2VzX19uYXYtYm94JykuYWRkQ2xhc3MoJ3Mtc2VydmljZXNfbmF2LWJveC1oaWRlJyk7XG4gICAgfSBlbHNlIGlmICgkKCcuc3dpcGVyLXdyYXBwZXItdGh1bWJzIC5zd2lwZXItc2xpZGUnKS5sZW5ndGggPCAyKSB7XG4gICAgICAkKCcucy1zZXJ2aWNlc19fbmF2LWJveCcpXG4gICAgICAgIC5hZGRDbGFzcygncy1zZXJ2aWNlc19uYXYtYm94LW10JylcbiAgICAgICAgLnJlbW92ZUNsYXNzKCdzLXNlcnZpY2VzX25hdi1ib3gtaGlkZScpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkKCcucy1zZXJ2aWNlc19fbmF2LWJveCcpXG4gICAgICAgIC5yZW1vdmVDbGFzcygncy1zZXJ2aWNlc19uYXYtYm94LW10JylcbiAgICAgICAgLnJlbW92ZUNsYXNzKCdzLXNlcnZpY2VzX25hdi1ib3gtaGlkZScpO1xuICAgIH1cbiAgICAvKm5hdi1ib3gtaGlkZSovXG4gIH1cbiAgZWxzZSB7XG5cbiAgICAkKCcucy1zZXJ2aWNlcy1zd2lwZXItc2xpZGUnKVxuICAgICAgLm5vdChcIltkYXRhLWZpbHRlcj0nXCIrZmlsdGVyK1wiJ11cIilcbiAgICAgIC5hZGRDbGFzcygnbm9uLXN3aXBlci1zbGlkZScpXG4gICAgICAucmVtb3ZlQ2xhc3MoJ3N3aXBlci1zbGlkZScpXG4gICAgICAuaGlkZSgpO1xuICAgICQoXCJbZGF0YS1maWx0ZXI9J1wiK2ZpbHRlcitcIiddXCIpXG4gICAgICAucmVtb3ZlQ2xhc3MoJ25vbi1zd2lwZXItc2xpZGUnKVxuICAgICAgLmFkZENsYXNzKCdzd2lwZXItc2xpZGUnKVxuICAgICAgLmF0dHIoJ3N0eWxlJywgbnVsbClcbiAgICAgIC5zaG93KCk7XG4gICAgaWYoJCgnLnN3aXBlci1zbGlkZScpLmxlbmd0aCA+IDIpXG4gICAgICBzbGlkZXN4Y29sID0gMjtcbiAgICBlbHNlIHNsaWRlc3hjb2wgPSAxO1xuICAgIHN3aXBlci5kZXN0cm95KCk7XG4gICAgc3dpcGVyID0gbmV3IFN3aXBlcignLm15U3dpcGVyJywge1xuICAgICAgc2xpZGVzUGVyVmlldzogMixcbiAgICAgIHNsaWRlc1BlckNvbHVtbjogc2xpZGVzeGNvbCxcbiAgICAgIHBhZ2luYXRpb25DbGlja2FibGU6IHRydWUsXG4gICAgICBzcGFjZUJldHdlZW46IDAsXG4gICAgICBicmVha3BvaW50czoge1xuICAgICAgICAzMjA6IHtcbiAgICAgICAgICBzbGlkZXNQZXJWaWV3OiAyLFxuICAgICAgICAgIHNsaWRlc1BlckNvbHVtbjogMSxcbiAgICAgICAgfSxcbiAgICAgICAgMzYwOiB7XG4gICAgICAgICAgc2xpZGVzUGVyVmlldzogMyxcbiAgICAgICAgICBzbGlkZXNQZXJDb2x1bW46IDEsXG4gICAgICAgIH0sXG4gICAgICAgIDQ5MDoge1xuICAgICAgICAgIHNsaWRlc1BlclZpZXc6IDQsXG4gICAgICAgICAgc2xpZGVzUGVyQ29sdW1uOiAxLFxuICAgICAgICB9LFxuICAgICAgICA3Njg6IHtcbiAgICAgICAgICBzbGlkZXNQZXJDb2x1bW46IHNsaWRlc3hjb2wsXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBuYXZpZ2F0aW9uOiB7XG4gICAgICAgIG5leHRFbDogJy5zLXNlcnZpY2VzX19uYXYtcmlnaHQnLFxuICAgICAgICBwcmV2RWw6ICcucy1zZXJ2aWNlc19fbmF2LWxlZnQnLFxuICAgICAgfVxuICAgIH0pO1xuICAgIHN3aXBlcjIuZGVzdHJveSgpO1xuICAgIHN3aXBlcjIgPSBuZXcgU3dpcGVyKCcubXlTd2lwZXIyJywge1xuICAgICAgc3BhY2VCZXR3ZWVuOiAxMCxcbiAgICAgIHRodW1iczoge1xuICAgICAgICBzd2lwZXI6IHN3aXBlcixcbiAgICAgIH0sXG4gICAgICBuYXZpZ2F0aW9uOiB7XG4gICAgICAgIG5leHRFbDogXCIucy1zZXJ2aWNlc19fbW9iaWxlLWJ1dHRvbi1uZXh0XCIsXG4gICAgICAgIHByZXZFbDogXCIucy1zZXJ2aWNlc19fbW9iaWxlLWJ1dHRvbi1wcmV2XCIsXG4gICAgICB9LFxuICAgIH0pO1xuICAgIC8qbmF2LWJveC1oaWRlKi9cbiAgICBpZiAoJCgnLnN3aXBlci13cmFwcGVyLXRodW1icyAuc3dpcGVyLXNsaWRlJykubGVuZ3RoID09PSAwKSB7XG4gICAgICAkKCcucy1zZXJ2aWNlc19fbmF2LWJveCcpLmFkZENsYXNzKCdzLXNlcnZpY2VzX25hdi1ib3gtaGlkZScpO1xuICAgIH1cbiAgICBlbHNlIGlmICgkKCcuc3dpcGVyLXdyYXBwZXItdGh1bWJzIC5zd2lwZXItc2xpZGUnKS5sZW5ndGggPCAyKSB7XG4gICAgICAkKCcucy1zZXJ2aWNlc19fbmF2LWJveCcpXG4gICAgICAgIC5hZGRDbGFzcygncy1zZXJ2aWNlc19uYXYtYm94LW10JylcbiAgICAgICAgLnJlbW92ZUNsYXNzKCdzLXNlcnZpY2VzX25hdi1ib3gtaGlkZScpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkKCcucy1zZXJ2aWNlc19fbmF2LWJveCcpXG4gICAgICAgIC5yZW1vdmVDbGFzcygncy1zZXJ2aWNlc19uYXYtYm94LW10JylcbiAgICAgICAgLnJlbW92ZUNsYXNzKCdzLXNlcnZpY2VzX25hdi1ib3gtaGlkZScpO1xuICAgIH1cbiAgICAvKm5hdi1ib3gtaGlkZSovXG4gIH1cbn0pXG4kKCcucy1zZXJ2aWNlc19fZmFuY3lib3gtbGluaycpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgbGV0IGl0ZW1zID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgncy1zZXJ2aWNlc19fZmFuY3lib3gtbGluaycpO1xuICBsZXQgYWN0aXZlQWxsID0gJCgnI3Mtc2VydmljZXNfZGlyZWN0aW9uLWFjdGl2ZScpLmhhc0NsYXNzKCdzLXNlcnZpY2VzX2RpcmVjdGlvbi1hY3RpdmUnKTtcbiAgaWYgKGFjdGl2ZUFsbCkge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgaXRlbXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGl0ZW1zW2ldLmRhdGFzZXQuZmFuY3lib3ggPSAnYWxsJztcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpdGVtcy5sZW5ndGg7IGkrKykge1xuICAgICAgaXRlbXNbaV0uZGF0YXNldC5mYW5jeWJveCA9IGl0ZW1zW2ldLnJlbDtcbiAgICB9XG4gIH1cbn0pXG5cblxuXG5cbiIsImltcG9ydCBkaXNwYXRjaGVyIGZyb20gJy4vZGlzcGF0Y2hlcic7XG5pbXBvcnQgZG9jdW1lbnRSZWFkeSBmcm9tICcuLi91dGlscy9kb2N1bWVudFJlYWR5JztcblxuY29uc3QgcHJlbG9hZGVyID0ge1xuICBlbDogbnVsbCxcbiAgaW1hZ2VzOiBbXSxcbiAgYmFja2dyb3VuZEVsczogW10sXG4gIGltYWdlc051bWJlcjogMCxcbiAgaW1hZ2VzTG9hZGVkOiAwLFxuICB0cmFuc2l0aW9uOiAxMDAwLFxuXG4gIGluaXQoKSB7XG4gICAgdGhpcy5lbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzaXRlLXByZWxvYWRlcicpO1xuICAgIHRoaXMuaW1hZ2VzID0gWy4uLmRvY3VtZW50LmltYWdlc107XG4gICAgdGhpcy5iYWNrZ3JvdW5kRWxzID0gWy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qcy1wcmVsb2FkZXItYmcnKV07XG5cbiAgICBjb25zdCBpbWFnZXNQYXRocyA9IHRoaXMuaW1hZ2VzLm1hcCgoaW1hZ2UpID0+IGltYWdlLnNyYyk7XG4gICAgY29uc3QgYmFja2dyb3VuZFBhdGhzID0gdGhpcy5iYWNrZ3JvdW5kRWxzLm1hcCgoZWxlbSkgPT4ge1xuICAgICAgY29uc3QgeyBiYWNrZ3JvdW5kSW1hZ2UgfSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW0sIGZhbHNlKTtcbiAgICAgIHJldHVybiBiYWNrZ3JvdW5kSW1hZ2Uuc2xpY2UoNCwgLTEpLnJlcGxhY2UoL1wiL2csICcnKTtcbiAgICB9KTtcbiAgICBjb25zdCBhbGxQYXRocyA9IFsuLi5pbWFnZXNQYXRocywgLi4uYmFja2dyb3VuZFBhdGhzXTtcblxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBwcmVmZXItZGVzdHJ1Y3R1cmluZ1xuICAgIHRoaXMuaW1hZ2VzTnVtYmVyID0gYWxsUGF0aHMubGVuZ3RoO1xuXG4gICAgaWYgKHRoaXMuaW1hZ2VzTnVtYmVyKSB7XG4gICAgICBhbGxQYXRocy5mb3JFYWNoKChpbWFnZXNQYXRoKSA9PiB7XG4gICAgICAgIGNvbnN0IGNsb25lID0gbmV3IEltYWdlKCk7XG4gICAgICAgIGNsb25lLmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCB0aGlzLmltYWdlTG9hZGVkLmJpbmQodGhpcykpO1xuICAgICAgICBjbG9uZS5hZGRFdmVudExpc3RlbmVyKCdlcnJvcicsIHRoaXMuaW1hZ2VMb2FkZWQuYmluZCh0aGlzKSk7XG4gICAgICAgIGNsb25lLnNyYyA9IGltYWdlc1BhdGg7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5wcmVsb2FkZXJIaWRlKCk7XG4gICAgfVxuICB9LFxuXG4gIHByZWxvYWRlckhpZGUodHJhbnNpdGlvbiA9IHRoaXMudHJhbnNpdGlvbikge1xuICAgIGNvbnN0IHsgZWw6IHByZWxvYWRlciB9ID0gdGhpcztcbiAgICBpZiAoIXByZWxvYWRlcikgcmV0dXJuO1xuXG4gICAgZGlzcGF0Y2hlci5kaXNwYXRjaCh7XG4gICAgICB0eXBlOiAnc2l0ZS1wcmVsb2FkZXI6aGlkaW5nJyxcbiAgICB9KTtcblxuICAgIHByZWxvYWRlci5zdHlsZS50cmFuc2l0aW9uID0gYG9wYWNpdHkgJHsgdHJhbnNpdGlvbiB9bXMgZWFzZSwgdmlzaWJpbGl0eSAkeyB0cmFuc2l0aW9uIH1tcyBlYXNlYDtcbiAgICBwcmVsb2FkZXIuY2xhc3NMaXN0LmFkZCgnX2xvYWRlZCcpO1xuICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmFkZCgnX3NpdGUtbG9hZGVkJyk7XG5cbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGRpc3BhdGNoZXIuZGlzcGF0Y2goe1xuICAgICAgICB0eXBlOiAnc2l0ZS1wcmVsb2FkZXI6cmVtb3ZlZCcsXG4gICAgICB9KTtcblxuICAgICAgcHJlbG9hZGVyLnJlbW92ZSgpO1xuICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuYWRkKCdfc2l0ZS1wcmVsb2FkZXItaGlkZGVuJyk7XG4gICAgfSwgdHJhbnNpdGlvbik7XG4gIH0sXG5cbiAgaW1hZ2VMb2FkZWQoKSB7XG4gICAgdGhpcy5pbWFnZXNMb2FkZWQgKz0gMTtcblxuICAgIGlmICh0aGlzLmltYWdlc0xvYWRlZCA+PSB0aGlzLmltYWdlc051bWJlcikge1xuICAgICAgdGhpcy5wcmVsb2FkZXJIaWRlKCk7XG4gICAgfVxuICB9LFxufTtcblxuZG9jdW1lbnRSZWFkeSgoKSA9PiB7XG4gIHByZWxvYWRlci5pbml0KCk7XG59KTtcblxuZXhwb3J0IGRlZmF1bHQgcHJlbG9hZGVyO1xuIiwiXG5mdW5jdGlvbiBpbml0U3Vic2NyaWJlVmFsaWRhdGUoKXtcblxuICAkKCcjc3Vic2NyaWJlX19mb3JtJykudmFsaWRhdGUoe1xuICAgIHN1Ym1pdEhhbmRsZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgbGV0IHN1YnNjcmliZU5hbWUgPSAkKCcjc3Vic2NyaWJlX19uYW1lJykudmFsKCk7XG4gICAgICBsZXQgc3Vic2NyaWJlRW1haWwgPSAkKCcjc3Vic2NyaWJlX19lbWFpbCcpLnZhbCgpO1xuICAgICAgbGV0IHN1YnNjcmliZU51bWJlciA9ICQoJyNzdWJzY3JpYmVfX251bWJlcicpLnZhbCgpO1xuICAgICAgbGV0IHN1YnNjcmliZVRleHRhcmVhID0gJCgnI3N1YnNjcmliZV9fdGV4dGFyZWEnKS52YWwoKTtcbiAgICAgIGxldCBvdmVybGF5Q2xvc2UgPSAkKCcjb3ZlcmxheScpO1xuICAgICAgY29uc29sZS5sb2coc3Vic2NyaWJlTmFtZSk7XG4gICAgICBjb25zb2xlLmxvZyhzdWJzY3JpYmVFbWFpbCk7XG4gICAgICBjb25zb2xlLmxvZyhzdWJzY3JpYmVOdW1iZXIpO1xuICAgICAgY29uc29sZS5sb2coc3Vic2NyaWJlVGV4dGFyZWEpO1xuICAgICAgYWxlcnQoXCJUaGFua3MuIFRoZSBhcHBsaWNhdGlvbiBpcyBhY2NlcHRlZC4gT3VyIG1hbmFnZXIgd2lsbCBjb250YWN0IHlvdSBzaG9ydGx5LlwiKTtcbiAgICAgIG92ZXJsYXlDbG9zZS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICB9LFxuICAgIHJ1bGVzOiB7XG4gICAgICBlbWFpbDoge1xuICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgZW1haWw6IHRydWVcbiAgICAgIH1cbiAgICB9LFxuICAgIG1lc3NhZ2VzOiB7XG4gICAgICBlbWFpbDogXCJQbGVhc2UgZW50ZXIgYSB2YWxpZCBlbWFpbCBhZGRyZXNzXCJcbiAgICB9XG4gIH0pO1xuXG4gIGxldCBpbnAgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjc3Vic2NyaWJlX19udW1iZXInKTtcbiAgaW5wLmFkZEV2ZW50TGlzdGVuZXIoJ2tleXByZXNzJywgZSA9PiB7XG4gICAgLy8g0J7RgtC80LXQvdGP0LXQvCDQstCy0L7QtCDQvdC1INGG0LjRhNGAXG4gICAgaWYoIS9cXGQvLnRlc3QoZS5rZXkpKVxuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICB9KTtcbn1cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcbiAgaW5pdFN1YnNjcmliZVZhbGlkYXRlKCk7XG59KTtcbiIsIi8vIEpTXG5pbXBvcnQgJ2NvcmUtanMvZmVhdHVyZXMvcHJvbWlzZSc7XG5pbXBvcnQgJ2NvcmUtanMvZmVhdHVyZXMvbnVtYmVyL2lzLW5hbic7XG5pbXBvcnQgJ2NvcmUtanMvZmVhdHVyZXMvYXJyYXkvZmluZCc7XG5pbXBvcnQgJ2NvcmUtanMvZmVhdHVyZXMvc3RyaW5nL3N0YXJ0cy13aXRoJztcbmltcG9ydCAndmVuZG9yL19wb2x5ZmlsbHMvanMvcmVxdWVzdEFuaW1hdGlvbkZyYW1lJztcblxuLy8gRE9NXG5pbXBvcnQgJ3ZlbmRvci9fcG9seWZpbGxzL2pzL0RPTS9FbGVtZW50Lm1hdGNoZXMnO1xuaW1wb3J0ICd2ZW5kb3IvX3BvbHlmaWxscy9qcy9ET00vRWxlbWVudC5jbG9zZXN0JzsgLy8g0LfQsNCy0LjRgdC40LzQvtGB0YLRjCAtIEVsZW1lbnQubWF0Y2hlc1xuaW1wb3J0ICd2ZW5kb3IvX3BvbHlmaWxscy9qcy9ET00vTm9kZS5yZW1vdmUnO1xuaW1wb3J0ICd2ZW5kb3IvX3BvbHlmaWxscy9qcy9ET00vTm9kZS5hZnRlcic7XG5pbXBvcnQgJ3ZlbmRvci9fcG9seWZpbGxzL2pzL0RPTS9Ob2RlLmJlZm9yZSc7XG5pbXBvcnQgJ3ZlbmRvci9fcG9seWZpbGxzL2pzL0RPTS9Ob2RlLnJlcGxhY2VXaXRoJztcbmltcG9ydCAndmVuZG9yL19wb2x5ZmlsbHMvanMvRE9NL1BhcmVudE5vZGUuYXBwZW5kJztcbmltcG9ydCAndmVuZG9yL19wb2x5ZmlsbHMvanMvRE9NL1BhcmVudE5vZGUucHJlcGVuZCc7XG5cbmlmICghU1ZHRWxlbWVudC5wcm90b3R5cGUuY29udGFpbnMpIHtcbiAgU1ZHRWxlbWVudC5wcm90b3R5cGUuY29udGFpbnMgPSBIVE1MRGl2RWxlbWVudC5wcm90b3R5cGUuY29udGFpbnM7XG59XG5cbi8vIGN1c3RvbUVsZW1lbnRzXG4vLyBpbXBvcnQgJ3ZlbmRvci9fcG9seWZpbGxzL2N1c3RvbUVsZW1lbnRzL2RvY3VtZW50LXJlZ2lzdGVyLWVsZW1lbnQubWF4JztcbiIsIi8qIGVzbGludC1kaXNhYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBFdmVudEVtaXR0ZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLl9oYW5kbGVycyA9IHtcbiAgICAgIGFsbDogW10sXG4gICAgfTtcbiAgICB0aGlzLl9mcm96ZW4gPSBmYWxzZTtcbiAgfVxuXG4gIGRpc3BhdGNoKGNoYW5uZWwsIGV2ZW50KSB7XG4gICAgaWYgKCFldmVudCkge1xuICAgICAgZXZlbnQgPSBjaGFubmVsO1xuICAgICAgY2hhbm5lbCA9ICdhbGwnO1xuICAgIH1cblxuICAgIGlmIChldmVudCAmJiBldmVudC50eXBlLmluZGV4T2YoJzonKSkge1xuICAgICAgY2hhbm5lbCA9IGV2ZW50LnR5cGUuc3BsaXQoJzonKVswXTtcbiAgICB9XG5cbiAgICBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbCh0aGlzLl9oYW5kbGVycywgY2hhbm5lbCkpIHtcbiAgICAgIHRoaXMuX2hhbmRsZXJzW2NoYW5uZWxdID0gW107XG4gICAgfVxuXG4gICAgdGhpcy5fZnJvemVuID0gdHJ1ZTtcblxuICAgIHRoaXMuX2hhbmRsZXJzW2NoYW5uZWxdLmZvckVhY2goKGhhbmRsZXIpID0+IGhhbmRsZXIoZXZlbnQpKTtcblxuICAgIGlmIChjaGFubmVsICE9PSAnYWxsJykge1xuICAgICAgdGhpcy5faGFuZGxlcnMuYWxsLmZvckVhY2goKGhhbmRsZXIpID0+IGhhbmRsZXIoZXZlbnQpKTtcbiAgICB9XG5cbiAgICB0aGlzLl9mcm96ZW4gPSBmYWxzZTtcbiAgfVxuXG4gIHN1YnNjcmliZShjaGFubmVsLCBoYW5kbGVyKSB7XG4gICAgaWYgKCFoYW5kbGVyKSB7XG4gICAgICBoYW5kbGVyID0gY2hhbm5lbDtcbiAgICAgIGNoYW5uZWwgPSAnYWxsJztcbiAgICB9XG5cbiAgICBpZiAodGhpcy5fZnJvemVuKSB7XG4gICAgICBjb25zb2xlLmVycm9yKCd0cnlpbmcgdG8gc3Vic2NyaWJlIHRvIEV2ZW50RW1pdHRlciB3aGlsZSBkaXNwYXRjaCBpcyB3b3JraW5nJyk7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBoYW5kbGVyICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICBjb25zb2xlLmVycm9yKCdoYW5kbGVyIGhhcyB0byBiZSBhIGZ1bmN0aW9uJyk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwodGhpcy5faGFuZGxlcnMsIGNoYW5uZWwpKSB7XG4gICAgICB0aGlzLl9oYW5kbGVyc1tjaGFubmVsXSA9IFtdO1xuICAgIH1cblxuICAgIGlmICh0aGlzLl9oYW5kbGVyc1tjaGFubmVsXS5pbmRleE9mKGhhbmRsZXIpID09PSAtMSkge1xuICAgICAgdGhpcy5faGFuZGxlcnNbY2hhbm5lbF0ucHVzaChoYW5kbGVyKTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc29sZS5lcnJvcignaGFuZGxlciBhbHJlYWR5IHNldCcpO1xuICAgIH1cbiAgfVxuXG4gIHVuc3Vic2NyaWJlKGNoYW5uZWwsIGhhbmRsZXIpIHtcbiAgICBpZiAoIWhhbmRsZXIpIHtcbiAgICAgIGhhbmRsZXIgPSBjaGFubmVsO1xuICAgICAgY2hhbm5lbCA9ICdhbGwnO1xuICAgIH1cblxuICAgIGlmICh0aGlzLl9mcm96ZW4pIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ3RyeWluZyB0byB1bnN1YnNjcmliZSBmcm9tIEV2ZW50RW1pdHRlciB3aGlsZSBkaXNwYXRjaCBpcyB3b3JraW5nJyk7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBoYW5kbGVyICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICBjb25zb2xlLmVycm9yKCdoYW5kbGVyIGhhcyB0byBiZSBhIGZ1bmN0aW9uJyk7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLl9oYW5kbGVyc1tjaGFubmVsXSkge1xuICAgICAgY29uc29sZS5lcnJvcihgY2hhbm5lbCAkeyBjaGFubmVsIH0gZG9lcyBub3QgZXhpc3RgKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5faGFuZGxlcnNbY2hhbm5lbF0uaW5kZXhPZihoYW5kbGVyKSA9PT0gLTEpIHtcbiAgICAgIGNvbnNvbGUubG9nKGhhbmRsZXIpO1xuICAgICAgY29uc29sZS5lcnJvcigndHJ5aW5nIHRvIHVuc3Vic2NyaWJlIHVuZXhpc3RpbmcgaGFuZGxlcicpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuX2hhbmRsZXJzW2NoYW5uZWxdID0gdGhpcy5faGFuZGxlcnNbY2hhbm5lbF0uZmlsdGVyKChoKSA9PiBoICE9PSBoYW5kbGVyKTtcbiAgfVxufVxuIiwiLyoqXG4gKiBXYWl0IHVudGlsIGRvY3VtZW50IGlzIGxvYWRlZCB0byBydW4gbWV0aG9kXG4gKiBAcGFyYW0gIHtGdW5jdGlvbn0gZm4gTWV0aG9kIHRvIHJ1blxuICovXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBkb2N1bWVudExvYWRlZChmbikge1xuICAvLyBTYW5pdHkgY2hlY2tcbiAgaWYgKHR5cGVvZiBmbiAhPT0gJ2Z1bmN0aW9uJykgcmV0dXJuO1xuXG4gIC8vIElmIGRvY3VtZW50IGlzIGFscmVhZHkgbG9hZGVkLCBydW4gbWV0aG9kXG4gIGlmIChkb2N1bWVudC5yZWFkeVN0YXRlID09PSAnY29tcGxldGUnKSB7XG4gICAgcmV0dXJuIGZuKCk7XG4gIH1cblxuICAvLyBPdGhlcndpc2UsIHdhaXQgdW50aWwgZG9jdW1lbnQgaXMgbG9hZGVkXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZm4sIGZhbHNlKTtcbn1cbiIsIi8qKlxuICogV2FpdCB1bnRpbCBkb2N1bWVudCBpcyByZWFkeSB0byBydW4gbWV0aG9kXG4gKiBAcGFyYW0gIHtGdW5jdGlvbn0gZm4gTWV0aG9kIHRvIHJ1blxuICovXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBkb2N1bWVudFJlYWR5KGZuKSB7XG4gIC8vIFNhbml0eSBjaGVja1xuICBpZiAodHlwZW9mIGZuICE9PSAnZnVuY3Rpb24nKSByZXR1cm47XG5cbiAgLy8gSWYgZG9jdW1lbnQgaXMgYWxyZWFkeSBsb2FkZWQsIHJ1biBtZXRob2RcbiAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgPT09ICdpbnRlcmFjdGl2ZScgfHwgZG9jdW1lbnQucmVhZHlTdGF0ZSA9PT0gJ2NvbXBsZXRlJykge1xuICAgIHJldHVybiBmbigpO1xuICB9XG5cbiAgLy8gT3RoZXJ3aXNlLCB3YWl0IHVudGlsIGRvY3VtZW50IGlzIGxvYWRlZFxuICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZm4sIGZhbHNlKTtcbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGdldFNjcm9sbFdpZHRoKCkge1xuICBjb25zdCBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gIE9iamVjdC5hc3NpZ24oZWxlbWVudC5zdHlsZSwge1xuICAgIG92ZXJmbG93WTogJ3Njcm9sbCcsXG4gICAgaGVpZ2h0OiAnNTBweCcsXG4gICAgd2lkdGg6ICc1MHB4JyxcbiAgICB2aXNpYmlsaXR5OiAnaGlkZGVuJyxcbiAgfSk7XG4gIGRvY3VtZW50LmJvZHkuYXBwZW5kKGVsZW1lbnQpO1xuICBjb25zdCBzY3JvbGxXaWR0aCA9IGVsZW1lbnQub2Zmc2V0V2lkdGggLSBlbGVtZW50LmNsaWVudFdpZHRoO1xuICBlbGVtZW50LnJlbW92ZSgpO1xuXG4gIHJldHVybiBzY3JvbGxXaWR0aDtcbn1cbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGlzVG91Y2hEZXZpY2UoKSB7XG4gIHJldHVybiAnb250b3VjaHN0YXJ0JyBpbiB3aW5kb3cgfHwgbmF2aWdhdG9yLm1heFRvdWNoUG9pbnRzO1xufVxuIiwiLyohXG4gKiBqUXVlcnkgVmFsaWRhdGlvbiBQbHVnaW4gdjEuMTkuMlxuICpcbiAqIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvXG4gKlxuICogQ29weXJpZ2h0IChjKSAyMDIwIErDtnJuIFphZWZmZXJlclxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlXG4gKi9cbihmdW5jdGlvbiggZmFjdG9yeSApIHtcbiAgaWYgKCB0eXBlb2YgZGVmaW5lID09PSBcImZ1bmN0aW9uXCIgJiYgZGVmaW5lLmFtZCApIHtcbiAgICBkZWZpbmUoIFtcImpxdWVyeVwiXSwgZmFjdG9yeSApO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBtb2R1bGUgPT09IFwib2JqZWN0XCIgJiYgbW9kdWxlLmV4cG9ydHMpIHtcbiAgICBtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoIHJlcXVpcmUoIFwianF1ZXJ5XCIgKSApO1xuICB9IGVsc2Uge1xuICAgIGZhY3RvcnkoIGpRdWVyeSApO1xuICB9XG59KGZ1bmN0aW9uKCAkICkge1xuXG4gICQuZXh0ZW5kKCAkLmZuLCB7XG5cbiAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL3ZhbGlkYXRlL1xuICAgIHZhbGlkYXRlOiBmdW5jdGlvbiggb3B0aW9ucyApIHtcblxuICAgICAgLy8gSWYgbm90aGluZyBpcyBzZWxlY3RlZCwgcmV0dXJuIG5vdGhpbmc7IGNhbid0IGNoYWluIGFueXdheVxuICAgICAgaWYgKCAhdGhpcy5sZW5ndGggKSB7XG4gICAgICAgIGlmICggb3B0aW9ucyAmJiBvcHRpb25zLmRlYnVnICYmIHdpbmRvdy5jb25zb2xlICkge1xuICAgICAgICAgIGNvbnNvbGUud2FybiggXCJOb3RoaW5nIHNlbGVjdGVkLCBjYW4ndCB2YWxpZGF0ZSwgcmV0dXJuaW5nIG5vdGhpbmcuXCIgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8vIENoZWNrIGlmIGEgdmFsaWRhdG9yIGZvciB0aGlzIGZvcm0gd2FzIGFscmVhZHkgY3JlYXRlZFxuICAgICAgdmFyIHZhbGlkYXRvciA9ICQuZGF0YSggdGhpc1sgMCBdLCBcInZhbGlkYXRvclwiICk7XG4gICAgICBpZiAoIHZhbGlkYXRvciApIHtcbiAgICAgICAgcmV0dXJuIHZhbGlkYXRvcjtcbiAgICAgIH1cblxuICAgICAgLy8gQWRkIG5vdmFsaWRhdGUgdGFnIGlmIEhUTUw1LlxuICAgICAgdGhpcy5hdHRyKCBcIm5vdmFsaWRhdGVcIiwgXCJub3ZhbGlkYXRlXCIgKTtcblxuICAgICAgdmFsaWRhdG9yID0gbmV3ICQudmFsaWRhdG9yKCBvcHRpb25zLCB0aGlzWyAwIF0gKTtcbiAgICAgICQuZGF0YSggdGhpc1sgMCBdLCBcInZhbGlkYXRvclwiLCB2YWxpZGF0b3IgKTtcblxuICAgICAgaWYgKCB2YWxpZGF0b3Iuc2V0dGluZ3Mub25zdWJtaXQgKSB7XG5cbiAgICAgICAgdGhpcy5vbiggXCJjbGljay52YWxpZGF0ZVwiLCBcIjpzdWJtaXRcIiwgZnVuY3Rpb24oIGV2ZW50ICkge1xuXG4gICAgICAgICAgLy8gVHJhY2sgdGhlIHVzZWQgc3VibWl0IGJ1dHRvbiB0byBwcm9wZXJseSBoYW5kbGUgc2NyaXB0ZWRcbiAgICAgICAgICAvLyBzdWJtaXRzIGxhdGVyLlxuICAgICAgICAgIHZhbGlkYXRvci5zdWJtaXRCdXR0b24gPSBldmVudC5jdXJyZW50VGFyZ2V0O1xuXG4gICAgICAgICAgLy8gQWxsb3cgc3VwcHJlc3NpbmcgdmFsaWRhdGlvbiBieSBhZGRpbmcgYSBjYW5jZWwgY2xhc3MgdG8gdGhlIHN1Ym1pdCBidXR0b25cbiAgICAgICAgICBpZiAoICQoIHRoaXMgKS5oYXNDbGFzcyggXCJjYW5jZWxcIiApICkge1xuICAgICAgICAgICAgdmFsaWRhdG9yLmNhbmNlbFN1Ym1pdCA9IHRydWU7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gQWxsb3cgc3VwcHJlc3NpbmcgdmFsaWRhdGlvbiBieSBhZGRpbmcgdGhlIGh0bWw1IGZvcm1ub3ZhbGlkYXRlIGF0dHJpYnV0ZSB0byB0aGUgc3VibWl0IGJ1dHRvblxuICAgICAgICAgIGlmICggJCggdGhpcyApLmF0dHIoIFwiZm9ybW5vdmFsaWRhdGVcIiApICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICB2YWxpZGF0b3IuY2FuY2VsU3VibWl0ID0gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gKTtcblxuICAgICAgICAvLyBWYWxpZGF0ZSB0aGUgZm9ybSBvbiBzdWJtaXRcbiAgICAgICAgdGhpcy5vbiggXCJzdWJtaXQudmFsaWRhdGVcIiwgZnVuY3Rpb24oIGV2ZW50ICkge1xuICAgICAgICAgIGlmICggdmFsaWRhdG9yLnNldHRpbmdzLmRlYnVnICkge1xuXG4gICAgICAgICAgICAvLyBQcmV2ZW50IGZvcm0gc3VibWl0IHRvIGJlIGFibGUgdG8gc2VlIGNvbnNvbGUgb3V0cHV0XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGZ1bmN0aW9uIGhhbmRsZSgpIHtcbiAgICAgICAgICAgIHZhciBoaWRkZW4sIHJlc3VsdDtcblxuICAgICAgICAgICAgLy8gSW5zZXJ0IGEgaGlkZGVuIGlucHV0IGFzIGEgcmVwbGFjZW1lbnQgZm9yIHRoZSBtaXNzaW5nIHN1Ym1pdCBidXR0b25cbiAgICAgICAgICAgIC8vIFRoZSBoaWRkZW4gaW5wdXQgaXMgaW5zZXJ0ZWQgaW4gdHdvIGNhc2VzOlxuICAgICAgICAgICAgLy8gICAtIEEgdXNlciBkZWZpbmVkIGEgYHN1Ym1pdEhhbmRsZXJgXG4gICAgICAgICAgICAvLyAgIC0gVGhlcmUgd2FzIGEgcGVuZGluZyByZXF1ZXN0IGR1ZSB0byBgcmVtb3RlYCBtZXRob2QgYW5kIGBzdG9wUmVxdWVzdCgpYFxuICAgICAgICAgICAgLy8gICAgIHdhcyBjYWxsZWQgdG8gc3VibWl0IHRoZSBmb3JtIGluIGNhc2UgaXQncyB2YWxpZFxuICAgICAgICAgICAgaWYgKCB2YWxpZGF0b3Iuc3VibWl0QnV0dG9uICYmICggdmFsaWRhdG9yLnNldHRpbmdzLnN1Ym1pdEhhbmRsZXIgfHwgdmFsaWRhdG9yLmZvcm1TdWJtaXR0ZWQgKSApIHtcbiAgICAgICAgICAgICAgaGlkZGVuID0gJCggXCI8aW5wdXQgdHlwZT0naGlkZGVuJy8+XCIgKVxuICAgICAgICAgICAgICAgIC5hdHRyKCBcIm5hbWVcIiwgdmFsaWRhdG9yLnN1Ym1pdEJ1dHRvbi5uYW1lIClcbiAgICAgICAgICAgICAgICAudmFsKCAkKCB2YWxpZGF0b3Iuc3VibWl0QnV0dG9uICkudmFsKCkgKVxuICAgICAgICAgICAgICAgIC5hcHBlbmRUbyggdmFsaWRhdG9yLmN1cnJlbnRGb3JtICk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICggdmFsaWRhdG9yLnNldHRpbmdzLnN1Ym1pdEhhbmRsZXIgJiYgIXZhbGlkYXRvci5zZXR0aW5ncy5kZWJ1ZyApIHtcbiAgICAgICAgICAgICAgcmVzdWx0ID0gdmFsaWRhdG9yLnNldHRpbmdzLnN1Ym1pdEhhbmRsZXIuY2FsbCggdmFsaWRhdG9yLCB2YWxpZGF0b3IuY3VycmVudEZvcm0sIGV2ZW50ICk7XG4gICAgICAgICAgICAgIGlmICggaGlkZGVuICkge1xuXG4gICAgICAgICAgICAgICAgLy8gQW5kIGNsZWFuIHVwIGFmdGVyd2FyZHM7IHRoYW5rcyB0byBuby1ibG9jay1zY29wZSwgaGlkZGVuIGNhbiBiZSByZWZlcmVuY2VkXG4gICAgICAgICAgICAgICAgaGlkZGVuLnJlbW92ZSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGlmICggcmVzdWx0ICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBQcmV2ZW50IHN1Ym1pdCBmb3IgaW52YWxpZCBmb3JtcyBvciBjdXN0b20gc3VibWl0IGhhbmRsZXJzXG4gICAgICAgICAgaWYgKCB2YWxpZGF0b3IuY2FuY2VsU3VibWl0ICkge1xuICAgICAgICAgICAgdmFsaWRhdG9yLmNhbmNlbFN1Ym1pdCA9IGZhbHNlO1xuICAgICAgICAgICAgcmV0dXJuIGhhbmRsZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoIHZhbGlkYXRvci5mb3JtKCkgKSB7XG4gICAgICAgICAgICBpZiAoIHZhbGlkYXRvci5wZW5kaW5nUmVxdWVzdCApIHtcbiAgICAgICAgICAgICAgdmFsaWRhdG9yLmZvcm1TdWJtaXR0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gaGFuZGxlKCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHZhbGlkYXRvci5mb2N1c0ludmFsaWQoKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHZhbGlkYXRvcjtcbiAgICB9LFxuXG4gICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy92YWxpZC9cbiAgICB2YWxpZDogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgdmFsaWQsIHZhbGlkYXRvciwgZXJyb3JMaXN0O1xuXG4gICAgICBpZiAoICQoIHRoaXNbIDAgXSApLmlzKCBcImZvcm1cIiApICkge1xuICAgICAgICB2YWxpZCA9IHRoaXMudmFsaWRhdGUoKS5mb3JtKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBlcnJvckxpc3QgPSBbXTtcbiAgICAgICAgdmFsaWQgPSB0cnVlO1xuICAgICAgICB2YWxpZGF0b3IgPSAkKCB0aGlzWyAwIF0uZm9ybSApLnZhbGlkYXRlKCk7XG4gICAgICAgIHRoaXMuZWFjaCggZnVuY3Rpb24oKSB7XG4gICAgICAgICAgdmFsaWQgPSB2YWxpZGF0b3IuZWxlbWVudCggdGhpcyApICYmIHZhbGlkO1xuICAgICAgICAgIGlmICggIXZhbGlkICkge1xuICAgICAgICAgICAgZXJyb3JMaXN0ID0gZXJyb3JMaXN0LmNvbmNhdCggdmFsaWRhdG9yLmVycm9yTGlzdCApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSApO1xuICAgICAgICB2YWxpZGF0b3IuZXJyb3JMaXN0ID0gZXJyb3JMaXN0O1xuICAgICAgfVxuICAgICAgcmV0dXJuIHZhbGlkO1xuICAgIH0sXG5cbiAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL3J1bGVzL1xuICAgIHJ1bGVzOiBmdW5jdGlvbiggY29tbWFuZCwgYXJndW1lbnQgKSB7XG4gICAgICB2YXIgZWxlbWVudCA9IHRoaXNbIDAgXSxcbiAgICAgICAgaXNDb250ZW50RWRpdGFibGUgPSB0eXBlb2YgdGhpcy5hdHRyKCBcImNvbnRlbnRlZGl0YWJsZVwiICkgIT09IFwidW5kZWZpbmVkXCIgJiYgdGhpcy5hdHRyKCBcImNvbnRlbnRlZGl0YWJsZVwiICkgIT09IFwiZmFsc2VcIixcbiAgICAgICAgc2V0dGluZ3MsIHN0YXRpY1J1bGVzLCBleGlzdGluZ1J1bGVzLCBkYXRhLCBwYXJhbSwgZmlsdGVyZWQ7XG5cbiAgICAgIC8vIElmIG5vdGhpbmcgaXMgc2VsZWN0ZWQsIHJldHVybiBlbXB0eSBvYmplY3Q7IGNhbid0IGNoYWluIGFueXdheVxuICAgICAgaWYgKCBlbGVtZW50ID09IG51bGwgKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKCAhZWxlbWVudC5mb3JtICYmIGlzQ29udGVudEVkaXRhYmxlICkge1xuICAgICAgICBlbGVtZW50LmZvcm0gPSB0aGlzLmNsb3Nlc3QoIFwiZm9ybVwiIClbIDAgXTtcbiAgICAgICAgZWxlbWVudC5uYW1lID0gdGhpcy5hdHRyKCBcIm5hbWVcIiApO1xuICAgICAgfVxuXG4gICAgICBpZiAoIGVsZW1lbnQuZm9ybSA9PSBudWxsICkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmICggY29tbWFuZCApIHtcbiAgICAgICAgc2V0dGluZ3MgPSAkLmRhdGEoIGVsZW1lbnQuZm9ybSwgXCJ2YWxpZGF0b3JcIiApLnNldHRpbmdzO1xuICAgICAgICBzdGF0aWNSdWxlcyA9IHNldHRpbmdzLnJ1bGVzO1xuICAgICAgICBleGlzdGluZ1J1bGVzID0gJC52YWxpZGF0b3Iuc3RhdGljUnVsZXMoIGVsZW1lbnQgKTtcbiAgICAgICAgc3dpdGNoICggY29tbWFuZCApIHtcbiAgICAgICAgICBjYXNlIFwiYWRkXCI6XG4gICAgICAgICAgICAkLmV4dGVuZCggZXhpc3RpbmdSdWxlcywgJC52YWxpZGF0b3Iubm9ybWFsaXplUnVsZSggYXJndW1lbnQgKSApO1xuXG4gICAgICAgICAgICAvLyBSZW1vdmUgbWVzc2FnZXMgZnJvbSBydWxlcywgYnV0IGFsbG93IHRoZW0gdG8gYmUgc2V0IHNlcGFyYXRlbHlcbiAgICAgICAgICAgIGRlbGV0ZSBleGlzdGluZ1J1bGVzLm1lc3NhZ2VzO1xuICAgICAgICAgICAgc3RhdGljUnVsZXNbIGVsZW1lbnQubmFtZSBdID0gZXhpc3RpbmdSdWxlcztcbiAgICAgICAgICAgIGlmICggYXJndW1lbnQubWVzc2FnZXMgKSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzLm1lc3NhZ2VzWyBlbGVtZW50Lm5hbWUgXSA9ICQuZXh0ZW5kKCBzZXR0aW5ncy5tZXNzYWdlc1sgZWxlbWVudC5uYW1lIF0sIGFyZ3VtZW50Lm1lc3NhZ2VzICk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwicmVtb3ZlXCI6XG4gICAgICAgICAgICBpZiAoICFhcmd1bWVudCApIHtcbiAgICAgICAgICAgICAgZGVsZXRlIHN0YXRpY1J1bGVzWyBlbGVtZW50Lm5hbWUgXTtcbiAgICAgICAgICAgICAgcmV0dXJuIGV4aXN0aW5nUnVsZXM7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBmaWx0ZXJlZCA9IHt9O1xuICAgICAgICAgICAgJC5lYWNoKCBhcmd1bWVudC5zcGxpdCggL1xccy8gKSwgZnVuY3Rpb24oIGluZGV4LCBtZXRob2QgKSB7XG4gICAgICAgICAgICAgIGZpbHRlcmVkWyBtZXRob2QgXSA9IGV4aXN0aW5nUnVsZXNbIG1ldGhvZCBdO1xuICAgICAgICAgICAgICBkZWxldGUgZXhpc3RpbmdSdWxlc1sgbWV0aG9kIF07XG4gICAgICAgICAgICB9ICk7XG4gICAgICAgICAgICByZXR1cm4gZmlsdGVyZWQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgZGF0YSA9ICQudmFsaWRhdG9yLm5vcm1hbGl6ZVJ1bGVzKFxuICAgICAgICAkLmV4dGVuZChcbiAgICAgICAgICB7fSxcbiAgICAgICAgICAkLnZhbGlkYXRvci5jbGFzc1J1bGVzKCBlbGVtZW50ICksXG4gICAgICAgICAgJC52YWxpZGF0b3IuYXR0cmlidXRlUnVsZXMoIGVsZW1lbnQgKSxcbiAgICAgICAgICAkLnZhbGlkYXRvci5kYXRhUnVsZXMoIGVsZW1lbnQgKSxcbiAgICAgICAgICAkLnZhbGlkYXRvci5zdGF0aWNSdWxlcyggZWxlbWVudCApXG4gICAgICAgICksIGVsZW1lbnQgKTtcblxuICAgICAgLy8gTWFrZSBzdXJlIHJlcXVpcmVkIGlzIGF0IGZyb250XG4gICAgICBpZiAoIGRhdGEucmVxdWlyZWQgKSB7XG4gICAgICAgIHBhcmFtID0gZGF0YS5yZXF1aXJlZDtcbiAgICAgICAgZGVsZXRlIGRhdGEucmVxdWlyZWQ7XG4gICAgICAgIGRhdGEgPSAkLmV4dGVuZCggeyByZXF1aXJlZDogcGFyYW0gfSwgZGF0YSApO1xuICAgICAgfVxuXG4gICAgICAvLyBNYWtlIHN1cmUgcmVtb3RlIGlzIGF0IGJhY2tcbiAgICAgIGlmICggZGF0YS5yZW1vdGUgKSB7XG4gICAgICAgIHBhcmFtID0gZGF0YS5yZW1vdGU7XG4gICAgICAgIGRlbGV0ZSBkYXRhLnJlbW90ZTtcbiAgICAgICAgZGF0YSA9ICQuZXh0ZW5kKCBkYXRhLCB7IHJlbW90ZTogcGFyYW0gfSApO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG4gIH0gKTtcblxuLy8gSlF1ZXJ5IHRyaW0gaXMgZGVwcmVjYXRlZCwgcHJvdmlkZSBhIHRyaW0gbWV0aG9kIGJhc2VkIG9uIFN0cmluZy5wcm90b3R5cGUudHJpbVxuICB2YXIgdHJpbSA9IGZ1bmN0aW9uKCBzdHIgKSB7XG5cbiAgICAvLyBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9TdHJpbmcvdHJpbSNQb2x5ZmlsbFxuICAgIHJldHVybiBzdHIucmVwbGFjZSggL15bXFxzXFx1RkVGRlxceEEwXSt8W1xcc1xcdUZFRkZcXHhBMF0rJC9nLCBcIlwiICk7XG4gIH07XG5cbi8vIEN1c3RvbSBzZWxlY3RvcnNcbiAgJC5leHRlbmQoICQuZXhwci5wc2V1ZG9zIHx8ICQuZXhwclsgXCI6XCIgXSwge1x0XHQvLyAnfHwgJC5leHByWyBcIjpcIiBdJyBoZXJlIGVuYWJsZXMgYmFja3dhcmRzIGNvbXBhdGliaWxpdHkgdG8galF1ZXJ5IDEuNy4gQ2FuIGJlIHJlbW92ZWQgd2hlbiBkcm9wcGluZyBqUSAxLjcueCBzdXBwb3J0XG5cbiAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL2JsYW5rLXNlbGVjdG9yL1xuICAgIGJsYW5rOiBmdW5jdGlvbiggYSApIHtcbiAgICAgIHJldHVybiAhdHJpbSggXCJcIiArICQoIGEgKS52YWwoKSApO1xuICAgIH0sXG5cbiAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL2ZpbGxlZC1zZWxlY3Rvci9cbiAgICBmaWxsZWQ6IGZ1bmN0aW9uKCBhICkge1xuICAgICAgdmFyIHZhbCA9ICQoIGEgKS52YWwoKTtcbiAgICAgIHJldHVybiB2YWwgIT09IG51bGwgJiYgISF0cmltKCBcIlwiICsgdmFsICk7XG4gICAgfSxcblxuICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvdW5jaGVja2VkLXNlbGVjdG9yL1xuICAgIHVuY2hlY2tlZDogZnVuY3Rpb24oIGEgKSB7XG4gICAgICByZXR1cm4gISQoIGEgKS5wcm9wKCBcImNoZWNrZWRcIiApO1xuICAgIH1cbiAgfSApO1xuXG4vLyBDb25zdHJ1Y3RvciBmb3IgdmFsaWRhdG9yXG4gICQudmFsaWRhdG9yID0gZnVuY3Rpb24oIG9wdGlvbnMsIGZvcm0gKSB7XG4gICAgdGhpcy5zZXR0aW5ncyA9ICQuZXh0ZW5kKCB0cnVlLCB7fSwgJC52YWxpZGF0b3IuZGVmYXVsdHMsIG9wdGlvbnMgKTtcbiAgICB0aGlzLmN1cnJlbnRGb3JtID0gZm9ybTtcbiAgICB0aGlzLmluaXQoKTtcbiAgfTtcblxuLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9qUXVlcnkudmFsaWRhdG9yLmZvcm1hdC9cbiAgJC52YWxpZGF0b3IuZm9ybWF0ID0gZnVuY3Rpb24oIHNvdXJjZSwgcGFyYW1zICkge1xuICAgIGlmICggYXJndW1lbnRzLmxlbmd0aCA9PT0gMSApIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIGFyZ3MgPSAkLm1ha2VBcnJheSggYXJndW1lbnRzICk7XG4gICAgICAgIGFyZ3MudW5zaGlmdCggc291cmNlICk7XG4gICAgICAgIHJldHVybiAkLnZhbGlkYXRvci5mb3JtYXQuYXBwbHkoIHRoaXMsIGFyZ3MgKTtcbiAgICAgIH07XG4gICAgfVxuICAgIGlmICggcGFyYW1zID09PSB1bmRlZmluZWQgKSB7XG4gICAgICByZXR1cm4gc291cmNlO1xuICAgIH1cbiAgICBpZiAoIGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIHBhcmFtcy5jb25zdHJ1Y3RvciAhPT0gQXJyYXkgICkge1xuICAgICAgcGFyYW1zID0gJC5tYWtlQXJyYXkoIGFyZ3VtZW50cyApLnNsaWNlKCAxICk7XG4gICAgfVxuICAgIGlmICggcGFyYW1zLmNvbnN0cnVjdG9yICE9PSBBcnJheSApIHtcbiAgICAgIHBhcmFtcyA9IFsgcGFyYW1zIF07XG4gICAgfVxuICAgICQuZWFjaCggcGFyYW1zLCBmdW5jdGlvbiggaSwgbiApIHtcbiAgICAgIHNvdXJjZSA9IHNvdXJjZS5yZXBsYWNlKCBuZXcgUmVnRXhwKCBcIlxcXFx7XCIgKyBpICsgXCJcXFxcfVwiLCBcImdcIiApLCBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIG47XG4gICAgICB9ICk7XG4gICAgfSApO1xuICAgIHJldHVybiBzb3VyY2U7XG4gIH07XG5cbiAgJC5leHRlbmQoICQudmFsaWRhdG9yLCB7XG5cbiAgICBkZWZhdWx0czoge1xuICAgICAgbWVzc2FnZXM6IHt9LFxuICAgICAgZ3JvdXBzOiB7fSxcbiAgICAgIHJ1bGVzOiB7fSxcbiAgICAgIGVycm9yQ2xhc3M6IFwiZXJyb3JcIixcbiAgICAgIHBlbmRpbmdDbGFzczogXCJwZW5kaW5nXCIsXG4gICAgICB2YWxpZENsYXNzOiBcInZhbGlkXCIsXG4gICAgICBlcnJvckVsZW1lbnQ6IFwibGFiZWxcIixcbiAgICAgIGZvY3VzQ2xlYW51cDogZmFsc2UsXG4gICAgICBmb2N1c0ludmFsaWQ6IHRydWUsXG4gICAgICBlcnJvckNvbnRhaW5lcjogJCggW10gKSxcbiAgICAgIGVycm9yTGFiZWxDb250YWluZXI6ICQoIFtdICksXG4gICAgICBvbnN1Ym1pdDogdHJ1ZSxcbiAgICAgIGlnbm9yZTogXCI6aGlkZGVuXCIsXG4gICAgICBpZ25vcmVUaXRsZTogZmFsc2UsXG4gICAgICBvbmZvY3VzaW46IGZ1bmN0aW9uKCBlbGVtZW50ICkge1xuICAgICAgICB0aGlzLmxhc3RBY3RpdmUgPSBlbGVtZW50O1xuXG4gICAgICAgIC8vIEhpZGUgZXJyb3IgbGFiZWwgYW5kIHJlbW92ZSBlcnJvciBjbGFzcyBvbiBmb2N1cyBpZiBlbmFibGVkXG4gICAgICAgIGlmICggdGhpcy5zZXR0aW5ncy5mb2N1c0NsZWFudXAgKSB7XG4gICAgICAgICAgaWYgKCB0aGlzLnNldHRpbmdzLnVuaGlnaGxpZ2h0ICkge1xuICAgICAgICAgICAgdGhpcy5zZXR0aW5ncy51bmhpZ2hsaWdodC5jYWxsKCB0aGlzLCBlbGVtZW50LCB0aGlzLnNldHRpbmdzLmVycm9yQ2xhc3MsIHRoaXMuc2V0dGluZ3MudmFsaWRDbGFzcyApO1xuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLmhpZGVUaGVzZSggdGhpcy5lcnJvcnNGb3IoIGVsZW1lbnQgKSApO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgb25mb2N1c291dDogZnVuY3Rpb24oIGVsZW1lbnQgKSB7XG4gICAgICAgIGlmICggIXRoaXMuY2hlY2thYmxlKCBlbGVtZW50ICkgJiYgKCBlbGVtZW50Lm5hbWUgaW4gdGhpcy5zdWJtaXR0ZWQgfHwgIXRoaXMub3B0aW9uYWwoIGVsZW1lbnQgKSApICkge1xuICAgICAgICAgIHRoaXMuZWxlbWVudCggZWxlbWVudCApO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgb25rZXl1cDogZnVuY3Rpb24oIGVsZW1lbnQsIGV2ZW50ICkge1xuXG4gICAgICAgIC8vIEF2b2lkIHJldmFsaWRhdGUgdGhlIGZpZWxkIHdoZW4gcHJlc3Npbmcgb25lIG9mIHRoZSBmb2xsb3dpbmcga2V5c1xuICAgICAgICAvLyBTaGlmdCAgICAgICA9PiAxNlxuICAgICAgICAvLyBDdHJsICAgICAgICA9PiAxN1xuICAgICAgICAvLyBBbHQgICAgICAgICA9PiAxOFxuICAgICAgICAvLyBDYXBzIGxvY2sgICA9PiAyMFxuICAgICAgICAvLyBFbmQgICAgICAgICA9PiAzNVxuICAgICAgICAvLyBIb21lICAgICAgICA9PiAzNlxuICAgICAgICAvLyBMZWZ0IGFycm93ICA9PiAzN1xuICAgICAgICAvLyBVcCBhcnJvdyAgICA9PiAzOFxuICAgICAgICAvLyBSaWdodCBhcnJvdyA9PiAzOVxuICAgICAgICAvLyBEb3duIGFycm93ICA9PiA0MFxuICAgICAgICAvLyBJbnNlcnQgICAgICA9PiA0NVxuICAgICAgICAvLyBOdW0gbG9jayAgICA9PiAxNDRcbiAgICAgICAgLy8gQWx0R3Iga2V5ICAgPT4gMjI1XG4gICAgICAgIHZhciBleGNsdWRlZEtleXMgPSBbXG4gICAgICAgICAgMTYsIDE3LCAxOCwgMjAsIDM1LCAzNiwgMzcsXG4gICAgICAgICAgMzgsIDM5LCA0MCwgNDUsIDE0NCwgMjI1XG4gICAgICAgIF07XG5cbiAgICAgICAgaWYgKCBldmVudC53aGljaCA9PT0gOSAmJiB0aGlzLmVsZW1lbnRWYWx1ZSggZWxlbWVudCApID09PSBcIlwiIHx8ICQuaW5BcnJheSggZXZlbnQua2V5Q29kZSwgZXhjbHVkZWRLZXlzICkgIT09IC0xICkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSBlbHNlIGlmICggZWxlbWVudC5uYW1lIGluIHRoaXMuc3VibWl0dGVkIHx8IGVsZW1lbnQubmFtZSBpbiB0aGlzLmludmFsaWQgKSB7XG4gICAgICAgICAgdGhpcy5lbGVtZW50KCBlbGVtZW50ICk7XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBvbmNsaWNrOiBmdW5jdGlvbiggZWxlbWVudCApIHtcblxuICAgICAgICAvLyBDbGljayBvbiBzZWxlY3RzLCByYWRpb2J1dHRvbnMgYW5kIGNoZWNrYm94ZXNcbiAgICAgICAgaWYgKCBlbGVtZW50Lm5hbWUgaW4gdGhpcy5zdWJtaXR0ZWQgKSB7XG4gICAgICAgICAgdGhpcy5lbGVtZW50KCBlbGVtZW50ICk7XG5cbiAgICAgICAgICAvLyBPciBvcHRpb24gZWxlbWVudHMsIGNoZWNrIHBhcmVudCBzZWxlY3QgaW4gdGhhdCBjYXNlXG4gICAgICAgIH0gZWxzZSBpZiAoIGVsZW1lbnQucGFyZW50Tm9kZS5uYW1lIGluIHRoaXMuc3VibWl0dGVkICkge1xuICAgICAgICAgIHRoaXMuZWxlbWVudCggZWxlbWVudC5wYXJlbnROb2RlICk7XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBoaWdobGlnaHQ6IGZ1bmN0aW9uKCBlbGVtZW50LCBlcnJvckNsYXNzLCB2YWxpZENsYXNzICkge1xuICAgICAgICBpZiAoIGVsZW1lbnQudHlwZSA9PT0gXCJyYWRpb1wiICkge1xuICAgICAgICAgIHRoaXMuZmluZEJ5TmFtZSggZWxlbWVudC5uYW1lICkuYWRkQ2xhc3MoIGVycm9yQ2xhc3MgKS5yZW1vdmVDbGFzcyggdmFsaWRDbGFzcyApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICQoIGVsZW1lbnQgKS5hZGRDbGFzcyggZXJyb3JDbGFzcyApLnJlbW92ZUNsYXNzKCB2YWxpZENsYXNzICk7XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB1bmhpZ2hsaWdodDogZnVuY3Rpb24oIGVsZW1lbnQsIGVycm9yQ2xhc3MsIHZhbGlkQ2xhc3MgKSB7XG4gICAgICAgIGlmICggZWxlbWVudC50eXBlID09PSBcInJhZGlvXCIgKSB7XG4gICAgICAgICAgdGhpcy5maW5kQnlOYW1lKCBlbGVtZW50Lm5hbWUgKS5yZW1vdmVDbGFzcyggZXJyb3JDbGFzcyApLmFkZENsYXNzKCB2YWxpZENsYXNzICk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgJCggZWxlbWVudCApLnJlbW92ZUNsYXNzKCBlcnJvckNsYXNzICkuYWRkQ2xhc3MoIHZhbGlkQ2xhc3MgKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL2pRdWVyeS52YWxpZGF0b3Iuc2V0RGVmYXVsdHMvXG4gICAgc2V0RGVmYXVsdHM6IGZ1bmN0aW9uKCBzZXR0aW5ncyApIHtcbiAgICAgICQuZXh0ZW5kKCAkLnZhbGlkYXRvci5kZWZhdWx0cywgc2V0dGluZ3MgKTtcbiAgICB9LFxuXG4gICAgbWVzc2FnZXM6IHtcbiAgICAgIHJlcXVpcmVkOiBcIlRoaXMgZmllbGQgaXMgcmVxdWlyZWQuXCIsXG4gICAgICByZW1vdGU6IFwiUGxlYXNlIGZpeCB0aGlzIGZpZWxkLlwiLFxuICAgICAgZW1haWw6IFwiUGxlYXNlIGVudGVyIGEgdmFsaWQgZW1haWwgYWRkcmVzcy5cIixcbiAgICAgIHVybDogXCJQbGVhc2UgZW50ZXIgYSB2YWxpZCBVUkwuXCIsXG4gICAgICBkYXRlOiBcIlBsZWFzZSBlbnRlciBhIHZhbGlkIGRhdGUuXCIsXG4gICAgICBkYXRlSVNPOiBcIlBsZWFzZSBlbnRlciBhIHZhbGlkIGRhdGUgKElTTykuXCIsXG4gICAgICBudW1iZXI6IFwiUGxlYXNlIGVudGVyIGEgdmFsaWQgbnVtYmVyLlwiLFxuICAgICAgZGlnaXRzOiBcIlBsZWFzZSBlbnRlciBvbmx5IGRpZ2l0cy5cIixcbiAgICAgIGVxdWFsVG86IFwiUGxlYXNlIGVudGVyIHRoZSBzYW1lIHZhbHVlIGFnYWluLlwiLFxuICAgICAgbWF4bGVuZ3RoOiAkLnZhbGlkYXRvci5mb3JtYXQoIFwiUGxlYXNlIGVudGVyIG5vIG1vcmUgdGhhbiB7MH0gY2hhcmFjdGVycy5cIiApLFxuICAgICAgbWlubGVuZ3RoOiAkLnZhbGlkYXRvci5mb3JtYXQoIFwiUGxlYXNlIGVudGVyIGF0IGxlYXN0IHswfSBjaGFyYWN0ZXJzLlwiICksXG4gICAgICByYW5nZWxlbmd0aDogJC52YWxpZGF0b3IuZm9ybWF0KCBcIlBsZWFzZSBlbnRlciBhIHZhbHVlIGJldHdlZW4gezB9IGFuZCB7MX0gY2hhcmFjdGVycyBsb25nLlwiICksXG4gICAgICByYW5nZTogJC52YWxpZGF0b3IuZm9ybWF0KCBcIlBsZWFzZSBlbnRlciBhIHZhbHVlIGJldHdlZW4gezB9IGFuZCB7MX0uXCIgKSxcbiAgICAgIG1heDogJC52YWxpZGF0b3IuZm9ybWF0KCBcIlBsZWFzZSBlbnRlciBhIHZhbHVlIGxlc3MgdGhhbiBvciBlcXVhbCB0byB7MH0uXCIgKSxcbiAgICAgIG1pbjogJC52YWxpZGF0b3IuZm9ybWF0KCBcIlBsZWFzZSBlbnRlciBhIHZhbHVlIGdyZWF0ZXIgdGhhbiBvciBlcXVhbCB0byB7MH0uXCIgKSxcbiAgICAgIHN0ZXA6ICQudmFsaWRhdG9yLmZvcm1hdCggXCJQbGVhc2UgZW50ZXIgYSBtdWx0aXBsZSBvZiB7MH0uXCIgKVxuICAgIH0sXG5cbiAgICBhdXRvQ3JlYXRlUmFuZ2VzOiBmYWxzZSxcblxuICAgIHByb3RvdHlwZToge1xuXG4gICAgICBpbml0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgdGhpcy5sYWJlbENvbnRhaW5lciA9ICQoIHRoaXMuc2V0dGluZ3MuZXJyb3JMYWJlbENvbnRhaW5lciApO1xuICAgICAgICB0aGlzLmVycm9yQ29udGV4dCA9IHRoaXMubGFiZWxDb250YWluZXIubGVuZ3RoICYmIHRoaXMubGFiZWxDb250YWluZXIgfHwgJCggdGhpcy5jdXJyZW50Rm9ybSApO1xuICAgICAgICB0aGlzLmNvbnRhaW5lcnMgPSAkKCB0aGlzLnNldHRpbmdzLmVycm9yQ29udGFpbmVyICkuYWRkKCB0aGlzLnNldHRpbmdzLmVycm9yTGFiZWxDb250YWluZXIgKTtcbiAgICAgICAgdGhpcy5zdWJtaXR0ZWQgPSB7fTtcbiAgICAgICAgdGhpcy52YWx1ZUNhY2hlID0ge307XG4gICAgICAgIHRoaXMucGVuZGluZ1JlcXVlc3QgPSAwO1xuICAgICAgICB0aGlzLnBlbmRpbmcgPSB7fTtcbiAgICAgICAgdGhpcy5pbnZhbGlkID0ge307XG4gICAgICAgIHRoaXMucmVzZXQoKTtcblxuICAgICAgICB2YXIgY3VycmVudEZvcm0gPSB0aGlzLmN1cnJlbnRGb3JtLFxuICAgICAgICAgIGdyb3VwcyA9ICggdGhpcy5ncm91cHMgPSB7fSApLFxuICAgICAgICAgIHJ1bGVzO1xuICAgICAgICAkLmVhY2goIHRoaXMuc2V0dGluZ3MuZ3JvdXBzLCBmdW5jdGlvbigga2V5LCB2YWx1ZSApIHtcbiAgICAgICAgICBpZiAoIHR5cGVvZiB2YWx1ZSA9PT0gXCJzdHJpbmdcIiApIHtcbiAgICAgICAgICAgIHZhbHVlID0gdmFsdWUuc3BsaXQoIC9cXHMvICk7XG4gICAgICAgICAgfVxuICAgICAgICAgICQuZWFjaCggdmFsdWUsIGZ1bmN0aW9uKCBpbmRleCwgbmFtZSApIHtcbiAgICAgICAgICAgIGdyb3Vwc1sgbmFtZSBdID0ga2V5O1xuICAgICAgICAgIH0gKTtcbiAgICAgICAgfSApO1xuICAgICAgICBydWxlcyA9IHRoaXMuc2V0dGluZ3MucnVsZXM7XG4gICAgICAgICQuZWFjaCggcnVsZXMsIGZ1bmN0aW9uKCBrZXksIHZhbHVlICkge1xuICAgICAgICAgIHJ1bGVzWyBrZXkgXSA9ICQudmFsaWRhdG9yLm5vcm1hbGl6ZVJ1bGUoIHZhbHVlICk7XG4gICAgICAgIH0gKTtcblxuICAgICAgICBmdW5jdGlvbiBkZWxlZ2F0ZSggZXZlbnQgKSB7XG4gICAgICAgICAgdmFyIGlzQ29udGVudEVkaXRhYmxlID0gdHlwZW9mICQoIHRoaXMgKS5hdHRyKCBcImNvbnRlbnRlZGl0YWJsZVwiICkgIT09IFwidW5kZWZpbmVkXCIgJiYgJCggdGhpcyApLmF0dHIoIFwiY29udGVudGVkaXRhYmxlXCIgKSAhPT0gXCJmYWxzZVwiO1xuXG4gICAgICAgICAgLy8gU2V0IGZvcm0gZXhwYW5kbyBvbiBjb250ZW50ZWRpdGFibGVcbiAgICAgICAgICBpZiAoICF0aGlzLmZvcm0gJiYgaXNDb250ZW50RWRpdGFibGUgKSB7XG4gICAgICAgICAgICB0aGlzLmZvcm0gPSAkKCB0aGlzICkuY2xvc2VzdCggXCJmb3JtXCIgKVsgMCBdO1xuICAgICAgICAgICAgdGhpcy5uYW1lID0gJCggdGhpcyApLmF0dHIoIFwibmFtZVwiICk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gSWdub3JlIHRoZSBlbGVtZW50IGlmIGl0IGJlbG9uZ3MgdG8gYW5vdGhlciBmb3JtLiBUaGlzIHdpbGwgaGFwcGVuIG1haW5seVxuICAgICAgICAgIC8vIHdoZW4gc2V0dGluZyB0aGUgYGZvcm1gIGF0dHJpYnV0ZSBvZiBhbiBpbnB1dCB0byB0aGUgaWQgb2YgYW5vdGhlciBmb3JtLlxuICAgICAgICAgIGlmICggY3VycmVudEZvcm0gIT09IHRoaXMuZm9ybSApIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB2YXIgdmFsaWRhdG9yID0gJC5kYXRhKCB0aGlzLmZvcm0sIFwidmFsaWRhdG9yXCIgKSxcbiAgICAgICAgICAgIGV2ZW50VHlwZSA9IFwib25cIiArIGV2ZW50LnR5cGUucmVwbGFjZSggL152YWxpZGF0ZS8sIFwiXCIgKSxcbiAgICAgICAgICAgIHNldHRpbmdzID0gdmFsaWRhdG9yLnNldHRpbmdzO1xuICAgICAgICAgIGlmICggc2V0dGluZ3NbIGV2ZW50VHlwZSBdICYmICEkKCB0aGlzICkuaXMoIHNldHRpbmdzLmlnbm9yZSApICkge1xuICAgICAgICAgICAgc2V0dGluZ3NbIGV2ZW50VHlwZSBdLmNhbGwoIHZhbGlkYXRvciwgdGhpcywgZXZlbnQgKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAkKCB0aGlzLmN1cnJlbnRGb3JtIClcbiAgICAgICAgICAub24oIFwiZm9jdXNpbi52YWxpZGF0ZSBmb2N1c291dC52YWxpZGF0ZSBrZXl1cC52YWxpZGF0ZVwiLFxuICAgICAgICAgICAgXCI6dGV4dCwgW3R5cGU9J3Bhc3N3b3JkJ10sIFt0eXBlPSdmaWxlJ10sIHNlbGVjdCwgdGV4dGFyZWEsIFt0eXBlPSdudW1iZXInXSwgW3R5cGU9J3NlYXJjaCddLCBcIiArXG4gICAgICAgICAgICBcIlt0eXBlPSd0ZWwnXSwgW3R5cGU9J3VybCddLCBbdHlwZT0nZW1haWwnXSwgW3R5cGU9J2RhdGV0aW1lJ10sIFt0eXBlPSdkYXRlJ10sIFt0eXBlPSdtb250aCddLCBcIiArXG4gICAgICAgICAgICBcIlt0eXBlPSd3ZWVrJ10sIFt0eXBlPSd0aW1lJ10sIFt0eXBlPSdkYXRldGltZS1sb2NhbCddLCBbdHlwZT0ncmFuZ2UnXSwgW3R5cGU9J2NvbG9yJ10sIFwiICtcbiAgICAgICAgICAgIFwiW3R5cGU9J3JhZGlvJ10sIFt0eXBlPSdjaGVja2JveCddLCBbY29udGVudGVkaXRhYmxlXSwgW3R5cGU9J2J1dHRvbiddXCIsIGRlbGVnYXRlIClcblxuICAgICAgICAgIC8vIFN1cHBvcnQ6IENocm9tZSwgb2xkSUVcbiAgICAgICAgICAvLyBcInNlbGVjdFwiIGlzIHByb3ZpZGVkIGFzIGV2ZW50LnRhcmdldCB3aGVuIGNsaWNraW5nIGEgb3B0aW9uXG4gICAgICAgICAgLm9uKCBcImNsaWNrLnZhbGlkYXRlXCIsIFwic2VsZWN0LCBvcHRpb24sIFt0eXBlPSdyYWRpbyddLCBbdHlwZT0nY2hlY2tib3gnXVwiLCBkZWxlZ2F0ZSApO1xuXG4gICAgICAgIGlmICggdGhpcy5zZXR0aW5ncy5pbnZhbGlkSGFuZGxlciApIHtcbiAgICAgICAgICAkKCB0aGlzLmN1cnJlbnRGb3JtICkub24oIFwiaW52YWxpZC1mb3JtLnZhbGlkYXRlXCIsIHRoaXMuc2V0dGluZ3MuaW52YWxpZEhhbmRsZXIgKTtcbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9WYWxpZGF0b3IuZm9ybS9cbiAgICAgIGZvcm06IGZ1bmN0aW9uKCkge1xuICAgICAgICB0aGlzLmNoZWNrRm9ybSgpO1xuICAgICAgICAkLmV4dGVuZCggdGhpcy5zdWJtaXR0ZWQsIHRoaXMuZXJyb3JNYXAgKTtcbiAgICAgICAgdGhpcy5pbnZhbGlkID0gJC5leHRlbmQoIHt9LCB0aGlzLmVycm9yTWFwICk7XG4gICAgICAgIGlmICggIXRoaXMudmFsaWQoKSApIHtcbiAgICAgICAgICAkKCB0aGlzLmN1cnJlbnRGb3JtICkudHJpZ2dlckhhbmRsZXIoIFwiaW52YWxpZC1mb3JtXCIsIFsgdGhpcyBdICk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5zaG93RXJyb3JzKCk7XG4gICAgICAgIHJldHVybiB0aGlzLnZhbGlkKCk7XG4gICAgICB9LFxuXG4gICAgICBjaGVja0Zvcm06IGZ1bmN0aW9uKCkge1xuICAgICAgICB0aGlzLnByZXBhcmVGb3JtKCk7XG4gICAgICAgIGZvciAoIHZhciBpID0gMCwgZWxlbWVudHMgPSAoIHRoaXMuY3VycmVudEVsZW1lbnRzID0gdGhpcy5lbGVtZW50cygpICk7IGVsZW1lbnRzWyBpIF07IGkrKyApIHtcbiAgICAgICAgICB0aGlzLmNoZWNrKCBlbGVtZW50c1sgaSBdICk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMudmFsaWQoKTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvVmFsaWRhdG9yLmVsZW1lbnQvXG4gICAgICBlbGVtZW50OiBmdW5jdGlvbiggZWxlbWVudCApIHtcbiAgICAgICAgdmFyIGNsZWFuRWxlbWVudCA9IHRoaXMuY2xlYW4oIGVsZW1lbnQgKSxcbiAgICAgICAgICBjaGVja0VsZW1lbnQgPSB0aGlzLnZhbGlkYXRpb25UYXJnZXRGb3IoIGNsZWFuRWxlbWVudCApLFxuICAgICAgICAgIHYgPSB0aGlzLFxuICAgICAgICAgIHJlc3VsdCA9IHRydWUsXG4gICAgICAgICAgcnMsIGdyb3VwO1xuXG4gICAgICAgIGlmICggY2hlY2tFbGVtZW50ID09PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgZGVsZXRlIHRoaXMuaW52YWxpZFsgY2xlYW5FbGVtZW50Lm5hbWUgXTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnByZXBhcmVFbGVtZW50KCBjaGVja0VsZW1lbnQgKTtcbiAgICAgICAgICB0aGlzLmN1cnJlbnRFbGVtZW50cyA9ICQoIGNoZWNrRWxlbWVudCApO1xuXG4gICAgICAgICAgLy8gSWYgdGhpcyBlbGVtZW50IGlzIGdyb3VwZWQsIHRoZW4gdmFsaWRhdGUgYWxsIGdyb3VwIGVsZW1lbnRzIGFscmVhZHlcbiAgICAgICAgICAvLyBjb250YWluaW5nIGEgdmFsdWVcbiAgICAgICAgICBncm91cCA9IHRoaXMuZ3JvdXBzWyBjaGVja0VsZW1lbnQubmFtZSBdO1xuICAgICAgICAgIGlmICggZ3JvdXAgKSB7XG4gICAgICAgICAgICAkLmVhY2goIHRoaXMuZ3JvdXBzLCBmdW5jdGlvbiggbmFtZSwgdGVzdGdyb3VwICkge1xuICAgICAgICAgICAgICBpZiAoIHRlc3Rncm91cCA9PT0gZ3JvdXAgJiYgbmFtZSAhPT0gY2hlY2tFbGVtZW50Lm5hbWUgKSB7XG4gICAgICAgICAgICAgICAgY2xlYW5FbGVtZW50ID0gdi52YWxpZGF0aW9uVGFyZ2V0Rm9yKCB2LmNsZWFuKCB2LmZpbmRCeU5hbWUoIG5hbWUgKSApICk7XG4gICAgICAgICAgICAgICAgaWYgKCBjbGVhbkVsZW1lbnQgJiYgY2xlYW5FbGVtZW50Lm5hbWUgaW4gdi5pbnZhbGlkICkge1xuICAgICAgICAgICAgICAgICAgdi5jdXJyZW50RWxlbWVudHMucHVzaCggY2xlYW5FbGVtZW50ICk7XG4gICAgICAgICAgICAgICAgICByZXN1bHQgPSB2LmNoZWNrKCBjbGVhbkVsZW1lbnQgKSAmJiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9ICk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcnMgPSB0aGlzLmNoZWNrKCBjaGVja0VsZW1lbnQgKSAhPT0gZmFsc2U7XG4gICAgICAgICAgcmVzdWx0ID0gcmVzdWx0ICYmIHJzO1xuICAgICAgICAgIGlmICggcnMgKSB7XG4gICAgICAgICAgICB0aGlzLmludmFsaWRbIGNoZWNrRWxlbWVudC5uYW1lIF0gPSBmYWxzZTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5pbnZhbGlkWyBjaGVja0VsZW1lbnQubmFtZSBdID0gdHJ1ZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoICF0aGlzLm51bWJlck9mSW52YWxpZHMoKSApIHtcblxuICAgICAgICAgICAgLy8gSGlkZSBlcnJvciBjb250YWluZXJzIG9uIGxhc3QgZXJyb3JcbiAgICAgICAgICAgIHRoaXMudG9IaWRlID0gdGhpcy50b0hpZGUuYWRkKCB0aGlzLmNvbnRhaW5lcnMgKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5zaG93RXJyb3JzKCk7XG5cbiAgICAgICAgICAvLyBBZGQgYXJpYS1pbnZhbGlkIHN0YXR1cyBmb3Igc2NyZWVuIHJlYWRlcnNcbiAgICAgICAgICAkKCBlbGVtZW50ICkuYXR0ciggXCJhcmlhLWludmFsaWRcIiwgIXJzICk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9WYWxpZGF0b3Iuc2hvd0Vycm9ycy9cbiAgICAgIHNob3dFcnJvcnM6IGZ1bmN0aW9uKCBlcnJvcnMgKSB7XG4gICAgICAgIGlmICggZXJyb3JzICkge1xuICAgICAgICAgIHZhciB2YWxpZGF0b3IgPSB0aGlzO1xuXG4gICAgICAgICAgLy8gQWRkIGl0ZW1zIHRvIGVycm9yIGxpc3QgYW5kIG1hcFxuICAgICAgICAgICQuZXh0ZW5kKCB0aGlzLmVycm9yTWFwLCBlcnJvcnMgKTtcbiAgICAgICAgICB0aGlzLmVycm9yTGlzdCA9ICQubWFwKCB0aGlzLmVycm9yTWFwLCBmdW5jdGlvbiggbWVzc2FnZSwgbmFtZSApIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIG1lc3NhZ2U6IG1lc3NhZ2UsXG4gICAgICAgICAgICAgIGVsZW1lbnQ6IHZhbGlkYXRvci5maW5kQnlOYW1lKCBuYW1lIClbIDAgXVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9ICk7XG5cbiAgICAgICAgICAvLyBSZW1vdmUgaXRlbXMgZnJvbSBzdWNjZXNzIGxpc3RcbiAgICAgICAgICB0aGlzLnN1Y2Nlc3NMaXN0ID0gJC5ncmVwKCB0aGlzLnN1Y2Nlc3NMaXN0LCBmdW5jdGlvbiggZWxlbWVudCApIHtcbiAgICAgICAgICAgIHJldHVybiAhKCBlbGVtZW50Lm5hbWUgaW4gZXJyb3JzICk7XG4gICAgICAgICAgfSApO1xuICAgICAgICB9XG4gICAgICAgIGlmICggdGhpcy5zZXR0aW5ncy5zaG93RXJyb3JzICkge1xuICAgICAgICAgIHRoaXMuc2V0dGluZ3Muc2hvd0Vycm9ycy5jYWxsKCB0aGlzLCB0aGlzLmVycm9yTWFwLCB0aGlzLmVycm9yTGlzdCApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuZGVmYXVsdFNob3dFcnJvcnMoKTtcbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9WYWxpZGF0b3IucmVzZXRGb3JtL1xuICAgICAgcmVzZXRGb3JtOiBmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKCAkLmZuLnJlc2V0Rm9ybSApIHtcbiAgICAgICAgICAkKCB0aGlzLmN1cnJlbnRGb3JtICkucmVzZXRGb3JtKCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5pbnZhbGlkID0ge307XG4gICAgICAgIHRoaXMuc3VibWl0dGVkID0ge307XG4gICAgICAgIHRoaXMucHJlcGFyZUZvcm0oKTtcbiAgICAgICAgdGhpcy5oaWRlRXJyb3JzKCk7XG4gICAgICAgIHZhciBlbGVtZW50cyA9IHRoaXMuZWxlbWVudHMoKVxuICAgICAgICAgIC5yZW1vdmVEYXRhKCBcInByZXZpb3VzVmFsdWVcIiApXG4gICAgICAgICAgLnJlbW92ZUF0dHIoIFwiYXJpYS1pbnZhbGlkXCIgKTtcblxuICAgICAgICB0aGlzLnJlc2V0RWxlbWVudHMoIGVsZW1lbnRzICk7XG4gICAgICB9LFxuXG4gICAgICByZXNldEVsZW1lbnRzOiBmdW5jdGlvbiggZWxlbWVudHMgKSB7XG4gICAgICAgIHZhciBpO1xuXG4gICAgICAgIGlmICggdGhpcy5zZXR0aW5ncy51bmhpZ2hsaWdodCApIHtcbiAgICAgICAgICBmb3IgKCBpID0gMDsgZWxlbWVudHNbIGkgXTsgaSsrICkge1xuICAgICAgICAgICAgdGhpcy5zZXR0aW5ncy51bmhpZ2hsaWdodC5jYWxsKCB0aGlzLCBlbGVtZW50c1sgaSBdLFxuICAgICAgICAgICAgICB0aGlzLnNldHRpbmdzLmVycm9yQ2xhc3MsIFwiXCIgKTtcbiAgICAgICAgICAgIHRoaXMuZmluZEJ5TmFtZSggZWxlbWVudHNbIGkgXS5uYW1lICkucmVtb3ZlQ2xhc3MoIHRoaXMuc2V0dGluZ3MudmFsaWRDbGFzcyApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBlbGVtZW50c1xuICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCB0aGlzLnNldHRpbmdzLmVycm9yQ2xhc3MgKVxuICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCB0aGlzLnNldHRpbmdzLnZhbGlkQ2xhc3MgKTtcbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgbnVtYmVyT2ZJbnZhbGlkczogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm9iamVjdExlbmd0aCggdGhpcy5pbnZhbGlkICk7XG4gICAgICB9LFxuXG4gICAgICBvYmplY3RMZW5ndGg6IGZ1bmN0aW9uKCBvYmogKSB7XG4gICAgICAgIC8qIGpzaGludCB1bnVzZWQ6IGZhbHNlICovXG4gICAgICAgIHZhciBjb3VudCA9IDAsXG4gICAgICAgICAgaTtcbiAgICAgICAgZm9yICggaSBpbiBvYmogKSB7XG5cbiAgICAgICAgICAvLyBUaGlzIGNoZWNrIGFsbG93cyBjb3VudGluZyBlbGVtZW50cyB3aXRoIGVtcHR5IGVycm9yXG4gICAgICAgICAgLy8gbWVzc2FnZSBhcyBpbnZhbGlkIGVsZW1lbnRzXG4gICAgICAgICAgaWYgKCBvYmpbIGkgXSAhPT0gdW5kZWZpbmVkICYmIG9ialsgaSBdICE9PSBudWxsICYmIG9ialsgaSBdICE9PSBmYWxzZSApIHtcbiAgICAgICAgICAgIGNvdW50Kys7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjb3VudDtcbiAgICAgIH0sXG5cbiAgICAgIGhpZGVFcnJvcnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICB0aGlzLmhpZGVUaGVzZSggdGhpcy50b0hpZGUgKTtcbiAgICAgIH0sXG5cbiAgICAgIGhpZGVUaGVzZTogZnVuY3Rpb24oIGVycm9ycyApIHtcbiAgICAgICAgZXJyb3JzLm5vdCggdGhpcy5jb250YWluZXJzICkudGV4dCggXCJcIiApO1xuICAgICAgICB0aGlzLmFkZFdyYXBwZXIoIGVycm9ycyApLmhpZGUoKTtcbiAgICAgIH0sXG5cbiAgICAgIHZhbGlkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2l6ZSgpID09PSAwO1xuICAgICAgfSxcblxuICAgICAgc2l6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmVycm9yTGlzdC5sZW5ndGg7XG4gICAgICB9LFxuXG4gICAgICBmb2N1c0ludmFsaWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoIHRoaXMuc2V0dGluZ3MuZm9jdXNJbnZhbGlkICkge1xuICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAkKCB0aGlzLmZpbmRMYXN0QWN0aXZlKCkgfHwgdGhpcy5lcnJvckxpc3QubGVuZ3RoICYmIHRoaXMuZXJyb3JMaXN0WyAwIF0uZWxlbWVudCB8fCBbXSApXG4gICAgICAgICAgICAgIC5maWx0ZXIoIFwiOnZpc2libGVcIiApXG4gICAgICAgICAgICAgIC50cmlnZ2VyKCBcImZvY3VzXCIgKVxuXG4gICAgICAgICAgICAgIC8vIE1hbnVhbGx5IHRyaWdnZXIgZm9jdXNpbiBldmVudDsgd2l0aG91dCBpdCwgZm9jdXNpbiBoYW5kbGVyIGlzbid0IGNhbGxlZCwgZmluZExhc3RBY3RpdmUgd29uJ3QgaGF2ZSBhbnl0aGluZyB0byBmaW5kXG4gICAgICAgICAgICAgIC50cmlnZ2VyKCBcImZvY3VzaW5cIiApO1xuICAgICAgICAgIH0gY2F0Y2ggKCBlICkge1xuXG4gICAgICAgICAgICAvLyBJZ25vcmUgSUUgdGhyb3dpbmcgZXJyb3JzIHdoZW4gZm9jdXNpbmcgaGlkZGVuIGVsZW1lbnRzXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LFxuXG4gICAgICBmaW5kTGFzdEFjdGl2ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBsYXN0QWN0aXZlID0gdGhpcy5sYXN0QWN0aXZlO1xuICAgICAgICByZXR1cm4gbGFzdEFjdGl2ZSAmJiAkLmdyZXAoIHRoaXMuZXJyb3JMaXN0LCBmdW5jdGlvbiggbiApIHtcbiAgICAgICAgICByZXR1cm4gbi5lbGVtZW50Lm5hbWUgPT09IGxhc3RBY3RpdmUubmFtZTtcbiAgICAgICAgfSApLmxlbmd0aCA9PT0gMSAmJiBsYXN0QWN0aXZlO1xuICAgICAgfSxcblxuICAgICAgZWxlbWVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgdmFsaWRhdG9yID0gdGhpcyxcbiAgICAgICAgICBydWxlc0NhY2hlID0ge307XG5cbiAgICAgICAgLy8gU2VsZWN0IGFsbCB2YWxpZCBpbnB1dHMgaW5zaWRlIHRoZSBmb3JtIChubyBzdWJtaXQgb3IgcmVzZXQgYnV0dG9ucylcbiAgICAgICAgcmV0dXJuICQoIHRoaXMuY3VycmVudEZvcm0gKVxuICAgICAgICAgIC5maW5kKCBcImlucHV0LCBzZWxlY3QsIHRleHRhcmVhLCBbY29udGVudGVkaXRhYmxlXVwiIClcbiAgICAgICAgICAubm90KCBcIjpzdWJtaXQsIDpyZXNldCwgOmltYWdlLCA6ZGlzYWJsZWRcIiApXG4gICAgICAgICAgLm5vdCggdGhpcy5zZXR0aW5ncy5pZ25vcmUgKVxuICAgICAgICAgIC5maWx0ZXIoIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIG5hbWUgPSB0aGlzLm5hbWUgfHwgJCggdGhpcyApLmF0dHIoIFwibmFtZVwiICk7IC8vIEZvciBjb250ZW50ZWRpdGFibGVcbiAgICAgICAgICAgIHZhciBpc0NvbnRlbnRFZGl0YWJsZSA9IHR5cGVvZiAkKCB0aGlzICkuYXR0ciggXCJjb250ZW50ZWRpdGFibGVcIiApICE9PSBcInVuZGVmaW5lZFwiICYmICQoIHRoaXMgKS5hdHRyKCBcImNvbnRlbnRlZGl0YWJsZVwiICkgIT09IFwiZmFsc2VcIjtcblxuICAgICAgICAgICAgaWYgKCAhbmFtZSAmJiB2YWxpZGF0b3Iuc2V0dGluZ3MuZGVidWcgJiYgd2luZG93LmNvbnNvbGUgKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoIFwiJW8gaGFzIG5vIG5hbWUgYXNzaWduZWRcIiwgdGhpcyApO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBTZXQgZm9ybSBleHBhbmRvIG9uIGNvbnRlbnRlZGl0YWJsZVxuICAgICAgICAgICAgaWYgKCBpc0NvbnRlbnRFZGl0YWJsZSApIHtcbiAgICAgICAgICAgICAgdGhpcy5mb3JtID0gJCggdGhpcyApLmNsb3Nlc3QoIFwiZm9ybVwiIClbIDAgXTtcbiAgICAgICAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gSWdub3JlIGVsZW1lbnRzIHRoYXQgYmVsb25nIHRvIG90aGVyL25lc3RlZCBmb3Jtc1xuICAgICAgICAgICAgaWYgKCB0aGlzLmZvcm0gIT09IHZhbGlkYXRvci5jdXJyZW50Rm9ybSApIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBTZWxlY3Qgb25seSB0aGUgZmlyc3QgZWxlbWVudCBmb3IgZWFjaCBuYW1lLCBhbmQgb25seSB0aG9zZSB3aXRoIHJ1bGVzIHNwZWNpZmllZFxuICAgICAgICAgICAgaWYgKCBuYW1lIGluIHJ1bGVzQ2FjaGUgfHwgIXZhbGlkYXRvci5vYmplY3RMZW5ndGgoICQoIHRoaXMgKS5ydWxlcygpICkgKSB7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcnVsZXNDYWNoZVsgbmFtZSBdID0gdHJ1ZTtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH0gKTtcbiAgICAgIH0sXG5cbiAgICAgIGNsZWFuOiBmdW5jdGlvbiggc2VsZWN0b3IgKSB7XG4gICAgICAgIHJldHVybiAkKCBzZWxlY3RvciApWyAwIF07XG4gICAgICB9LFxuXG4gICAgICBlcnJvcnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgZXJyb3JDbGFzcyA9IHRoaXMuc2V0dGluZ3MuZXJyb3JDbGFzcy5zcGxpdCggXCIgXCIgKS5qb2luKCBcIi5cIiApO1xuICAgICAgICByZXR1cm4gJCggdGhpcy5zZXR0aW5ncy5lcnJvckVsZW1lbnQgKyBcIi5cIiArIGVycm9yQ2xhc3MsIHRoaXMuZXJyb3JDb250ZXh0ICk7XG4gICAgICB9LFxuXG4gICAgICByZXNldEludGVybmFsczogZnVuY3Rpb24oKSB7XG4gICAgICAgIHRoaXMuc3VjY2Vzc0xpc3QgPSBbXTtcbiAgICAgICAgdGhpcy5lcnJvckxpc3QgPSBbXTtcbiAgICAgICAgdGhpcy5lcnJvck1hcCA9IHt9O1xuICAgICAgICB0aGlzLnRvU2hvdyA9ICQoIFtdICk7XG4gICAgICAgIHRoaXMudG9IaWRlID0gJCggW10gKTtcbiAgICAgIH0sXG5cbiAgICAgIHJlc2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgdGhpcy5yZXNldEludGVybmFscygpO1xuICAgICAgICB0aGlzLmN1cnJlbnRFbGVtZW50cyA9ICQoIFtdICk7XG4gICAgICB9LFxuXG4gICAgICBwcmVwYXJlRm9ybTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICAgICAgdGhpcy50b0hpZGUgPSB0aGlzLmVycm9ycygpLmFkZCggdGhpcy5jb250YWluZXJzICk7XG4gICAgICB9LFxuXG4gICAgICBwcmVwYXJlRWxlbWVudDogZnVuY3Rpb24oIGVsZW1lbnQgKSB7XG4gICAgICAgIHRoaXMucmVzZXQoKTtcbiAgICAgICAgdGhpcy50b0hpZGUgPSB0aGlzLmVycm9yc0ZvciggZWxlbWVudCApO1xuICAgICAgfSxcblxuICAgICAgZWxlbWVudFZhbHVlOiBmdW5jdGlvbiggZWxlbWVudCApIHtcbiAgICAgICAgdmFyICRlbGVtZW50ID0gJCggZWxlbWVudCApLFxuICAgICAgICAgIHR5cGUgPSBlbGVtZW50LnR5cGUsXG4gICAgICAgICAgaXNDb250ZW50RWRpdGFibGUgPSB0eXBlb2YgJGVsZW1lbnQuYXR0ciggXCJjb250ZW50ZWRpdGFibGVcIiApICE9PSBcInVuZGVmaW5lZFwiICYmICRlbGVtZW50LmF0dHIoIFwiY29udGVudGVkaXRhYmxlXCIgKSAhPT0gXCJmYWxzZVwiLFxuICAgICAgICAgIHZhbCwgaWR4O1xuXG4gICAgICAgIGlmICggdHlwZSA9PT0gXCJyYWRpb1wiIHx8IHR5cGUgPT09IFwiY2hlY2tib3hcIiApIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5maW5kQnlOYW1lKCBlbGVtZW50Lm5hbWUgKS5maWx0ZXIoIFwiOmNoZWNrZWRcIiApLnZhbCgpO1xuICAgICAgICB9IGVsc2UgaWYgKCB0eXBlID09PSBcIm51bWJlclwiICYmIHR5cGVvZiBlbGVtZW50LnZhbGlkaXR5ICE9PSBcInVuZGVmaW5lZFwiICkge1xuICAgICAgICAgIHJldHVybiBlbGVtZW50LnZhbGlkaXR5LmJhZElucHV0ID8gXCJOYU5cIiA6ICRlbGVtZW50LnZhbCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCBpc0NvbnRlbnRFZGl0YWJsZSApIHtcbiAgICAgICAgICB2YWwgPSAkZWxlbWVudC50ZXh0KCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdmFsID0gJGVsZW1lbnQudmFsKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIHR5cGUgPT09IFwiZmlsZVwiICkge1xuXG4gICAgICAgICAgLy8gTW9kZXJuIGJyb3dzZXIgKGNocm9tZSAmIHNhZmFyaSlcbiAgICAgICAgICBpZiAoIHZhbC5zdWJzdHIoIDAsIDEyICkgPT09IFwiQzpcXFxcZmFrZXBhdGhcXFxcXCIgKSB7XG4gICAgICAgICAgICByZXR1cm4gdmFsLnN1YnN0ciggMTIgKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBMZWdhY3kgYnJvd3NlcnNcbiAgICAgICAgICAvLyBVbml4LWJhc2VkIHBhdGhcbiAgICAgICAgICBpZHggPSB2YWwubGFzdEluZGV4T2YoIFwiL1wiICk7XG4gICAgICAgICAgaWYgKCBpZHggPj0gMCApIHtcbiAgICAgICAgICAgIHJldHVybiB2YWwuc3Vic3RyKCBpZHggKyAxICk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gV2luZG93cy1iYXNlZCBwYXRoXG4gICAgICAgICAgaWR4ID0gdmFsLmxhc3RJbmRleE9mKCBcIlxcXFxcIiApO1xuICAgICAgICAgIGlmICggaWR4ID49IDAgKSB7XG4gICAgICAgICAgICByZXR1cm4gdmFsLnN1YnN0ciggaWR4ICsgMSApO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vIEp1c3QgdGhlIGZpbGUgbmFtZVxuICAgICAgICAgIHJldHVybiB2YWw7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIHR5cGVvZiB2YWwgPT09IFwic3RyaW5nXCIgKSB7XG4gICAgICAgICAgcmV0dXJuIHZhbC5yZXBsYWNlKCAvXFxyL2csIFwiXCIgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdmFsO1xuICAgICAgfSxcblxuICAgICAgY2hlY2s6IGZ1bmN0aW9uKCBlbGVtZW50ICkge1xuICAgICAgICBlbGVtZW50ID0gdGhpcy52YWxpZGF0aW9uVGFyZ2V0Rm9yKCB0aGlzLmNsZWFuKCBlbGVtZW50ICkgKTtcblxuICAgICAgICB2YXIgcnVsZXMgPSAkKCBlbGVtZW50ICkucnVsZXMoKSxcbiAgICAgICAgICBydWxlc0NvdW50ID0gJC5tYXAoIHJ1bGVzLCBmdW5jdGlvbiggbiwgaSApIHtcbiAgICAgICAgICAgIHJldHVybiBpO1xuICAgICAgICAgIH0gKS5sZW5ndGgsXG4gICAgICAgICAgZGVwZW5kZW5jeU1pc21hdGNoID0gZmFsc2UsXG4gICAgICAgICAgdmFsID0gdGhpcy5lbGVtZW50VmFsdWUoIGVsZW1lbnQgKSxcbiAgICAgICAgICByZXN1bHQsIG1ldGhvZCwgcnVsZSwgbm9ybWFsaXplcjtcblxuICAgICAgICAvLyBQcmlvcml0aXplIHRoZSBsb2NhbCBub3JtYWxpemVyIGRlZmluZWQgZm9yIHRoaXMgZWxlbWVudCBvdmVyIHRoZSBnbG9iYWwgb25lXG4gICAgICAgIC8vIGlmIHRoZSBmb3JtZXIgZXhpc3RzLCBvdGhlcndpc2UgdXNlciB0aGUgZ2xvYmFsIG9uZSBpbiBjYXNlIGl0IGV4aXN0cy5cbiAgICAgICAgaWYgKCB0eXBlb2YgcnVsZXMubm9ybWFsaXplciA9PT0gXCJmdW5jdGlvblwiICkge1xuICAgICAgICAgIG5vcm1hbGl6ZXIgPSBydWxlcy5ub3JtYWxpemVyO1xuICAgICAgICB9IGVsc2UgaWYgKFx0dHlwZW9mIHRoaXMuc2V0dGluZ3Mubm9ybWFsaXplciA9PT0gXCJmdW5jdGlvblwiICkge1xuICAgICAgICAgIG5vcm1hbGl6ZXIgPSB0aGlzLnNldHRpbmdzLm5vcm1hbGl6ZXI7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBJZiBub3JtYWxpemVyIGlzIGRlZmluZWQsIHRoZW4gY2FsbCBpdCB0byByZXRyZWl2ZSB0aGUgY2hhbmdlZCB2YWx1ZSBpbnN0ZWFkXG4gICAgICAgIC8vIG9mIHVzaW5nIHRoZSByZWFsIG9uZS5cbiAgICAgICAgLy8gTm90ZSB0aGF0IGB0aGlzYCBpbiB0aGUgbm9ybWFsaXplciBpcyBgZWxlbWVudGAuXG4gICAgICAgIGlmICggbm9ybWFsaXplciApIHtcbiAgICAgICAgICB2YWwgPSBub3JtYWxpemVyLmNhbGwoIGVsZW1lbnQsIHZhbCApO1xuXG4gICAgICAgICAgLy8gRGVsZXRlIHRoZSBub3JtYWxpemVyIGZyb20gcnVsZXMgdG8gYXZvaWQgdHJlYXRpbmcgaXQgYXMgYSBwcmUtZGVmaW5lZCBtZXRob2QuXG4gICAgICAgICAgZGVsZXRlIHJ1bGVzLm5vcm1hbGl6ZXI7XG4gICAgICAgIH1cblxuICAgICAgICBmb3IgKCBtZXRob2QgaW4gcnVsZXMgKSB7XG4gICAgICAgICAgcnVsZSA9IHsgbWV0aG9kOiBtZXRob2QsIHBhcmFtZXRlcnM6IHJ1bGVzWyBtZXRob2QgXSB9O1xuICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICByZXN1bHQgPSAkLnZhbGlkYXRvci5tZXRob2RzWyBtZXRob2QgXS5jYWxsKCB0aGlzLCB2YWwsIGVsZW1lbnQsIHJ1bGUucGFyYW1ldGVycyApO1xuXG4gICAgICAgICAgICAvLyBJZiBhIG1ldGhvZCBpbmRpY2F0ZXMgdGhhdCB0aGUgZmllbGQgaXMgb3B0aW9uYWwgYW5kIHRoZXJlZm9yZSB2YWxpZCxcbiAgICAgICAgICAgIC8vIGRvbid0IG1hcmsgaXQgYXMgdmFsaWQgd2hlbiB0aGVyZSBhcmUgbm8gb3RoZXIgcnVsZXNcbiAgICAgICAgICAgIGlmICggcmVzdWx0ID09PSBcImRlcGVuZGVuY3ktbWlzbWF0Y2hcIiAmJiBydWxlc0NvdW50ID09PSAxICkge1xuICAgICAgICAgICAgICBkZXBlbmRlbmN5TWlzbWF0Y2ggPSB0cnVlO1xuICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGRlcGVuZGVuY3lNaXNtYXRjaCA9IGZhbHNlO1xuXG4gICAgICAgICAgICBpZiAoIHJlc3VsdCA9PT0gXCJwZW5kaW5nXCIgKSB7XG4gICAgICAgICAgICAgIHRoaXMudG9IaWRlID0gdGhpcy50b0hpZGUubm90KCB0aGlzLmVycm9yc0ZvciggZWxlbWVudCApICk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCAhcmVzdWx0ICkge1xuICAgICAgICAgICAgICB0aGlzLmZvcm1hdEFuZEFkZCggZWxlbWVudCwgcnVsZSApO1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSBjYXRjaCAoIGUgKSB7XG4gICAgICAgICAgICBpZiAoIHRoaXMuc2V0dGluZ3MuZGVidWcgJiYgd2luZG93LmNvbnNvbGUgKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKCBcIkV4Y2VwdGlvbiBvY2N1cnJlZCB3aGVuIGNoZWNraW5nIGVsZW1lbnQgXCIgKyBlbGVtZW50LmlkICsgXCIsIGNoZWNrIHRoZSAnXCIgKyBydWxlLm1ldGhvZCArIFwiJyBtZXRob2QuXCIsIGUgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICggZSBpbnN0YW5jZW9mIFR5cGVFcnJvciApIHtcbiAgICAgICAgICAgICAgZS5tZXNzYWdlICs9IFwiLiAgRXhjZXB0aW9uIG9jY3VycmVkIHdoZW4gY2hlY2tpbmcgZWxlbWVudCBcIiArIGVsZW1lbnQuaWQgKyBcIiwgY2hlY2sgdGhlICdcIiArIHJ1bGUubWV0aG9kICsgXCInIG1ldGhvZC5cIjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhyb3cgZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCBkZXBlbmRlbmN5TWlzbWF0Y2ggKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmICggdGhpcy5vYmplY3RMZW5ndGgoIHJ1bGVzICkgKSB7XG4gICAgICAgICAgdGhpcy5zdWNjZXNzTGlzdC5wdXNoKCBlbGVtZW50ICk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9LFxuXG4gICAgICAvLyBSZXR1cm4gdGhlIGN1c3RvbSBtZXNzYWdlIGZvciB0aGUgZ2l2ZW4gZWxlbWVudCBhbmQgdmFsaWRhdGlvbiBtZXRob2RcbiAgICAgIC8vIHNwZWNpZmllZCBpbiB0aGUgZWxlbWVudCdzIEhUTUw1IGRhdGEgYXR0cmlidXRlXG4gICAgICAvLyByZXR1cm4gdGhlIGdlbmVyaWMgbWVzc2FnZSBpZiBwcmVzZW50IGFuZCBubyBtZXRob2Qgc3BlY2lmaWMgbWVzc2FnZSBpcyBwcmVzZW50XG4gICAgICBjdXN0b21EYXRhTWVzc2FnZTogZnVuY3Rpb24oIGVsZW1lbnQsIG1ldGhvZCApIHtcbiAgICAgICAgcmV0dXJuICQoIGVsZW1lbnQgKS5kYXRhKCBcIm1zZ1wiICsgbWV0aG9kLmNoYXJBdCggMCApLnRvVXBwZXJDYXNlKCkgK1xuICAgICAgICAgIG1ldGhvZC5zdWJzdHJpbmcoIDEgKS50b0xvd2VyQ2FzZSgpICkgfHwgJCggZWxlbWVudCApLmRhdGEoIFwibXNnXCIgKTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIFJldHVybiB0aGUgY3VzdG9tIG1lc3NhZ2UgZm9yIHRoZSBnaXZlbiBlbGVtZW50IG5hbWUgYW5kIHZhbGlkYXRpb24gbWV0aG9kXG4gICAgICBjdXN0b21NZXNzYWdlOiBmdW5jdGlvbiggbmFtZSwgbWV0aG9kICkge1xuICAgICAgICB2YXIgbSA9IHRoaXMuc2V0dGluZ3MubWVzc2FnZXNbIG5hbWUgXTtcbiAgICAgICAgcmV0dXJuIG0gJiYgKCBtLmNvbnN0cnVjdG9yID09PSBTdHJpbmcgPyBtIDogbVsgbWV0aG9kIF0gKTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIFJldHVybiB0aGUgZmlyc3QgZGVmaW5lZCBhcmd1bWVudCwgYWxsb3dpbmcgZW1wdHkgc3RyaW5nc1xuICAgICAgZmluZERlZmluZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICBmb3IgKCB2YXIgaSA9IDA7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKysgKSB7XG4gICAgICAgICAgaWYgKCBhcmd1bWVudHNbIGkgXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgcmV0dXJuIGFyZ3VtZW50c1sgaSBdO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xuICAgICAgfSxcblxuICAgICAgLy8gVGhlIHNlY29uZCBwYXJhbWV0ZXIgJ3J1bGUnIHVzZWQgdG8gYmUgYSBzdHJpbmcsIGFuZCBleHRlbmRlZCB0byBhbiBvYmplY3QgbGl0ZXJhbFxuICAgICAgLy8gb2YgdGhlIGZvbGxvd2luZyBmb3JtOlxuICAgICAgLy8gcnVsZSA9IHtcbiAgICAgIC8vICAgICBtZXRob2Q6IFwibWV0aG9kIG5hbWVcIixcbiAgICAgIC8vICAgICBwYXJhbWV0ZXJzOiBcInRoZSBnaXZlbiBtZXRob2QgcGFyYW1ldGVyc1wiXG4gICAgICAvLyB9XG4gICAgICAvL1xuICAgICAgLy8gVGhlIG9sZCBiZWhhdmlvciBzdGlsbCBzdXBwb3J0ZWQsIGtlcHQgdG8gbWFpbnRhaW4gYmFja3dhcmQgY29tcGF0aWJpbGl0eSB3aXRoXG4gICAgICAvLyBvbGQgY29kZSwgYW5kIHdpbGwgYmUgcmVtb3ZlZCBpbiB0aGUgbmV4dCBtYWpvciByZWxlYXNlLlxuICAgICAgZGVmYXVsdE1lc3NhZ2U6IGZ1bmN0aW9uKCBlbGVtZW50LCBydWxlICkge1xuICAgICAgICBpZiAoIHR5cGVvZiBydWxlID09PSBcInN0cmluZ1wiICkge1xuICAgICAgICAgIHJ1bGUgPSB7IG1ldGhvZDogcnVsZSB9O1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIG1lc3NhZ2UgPSB0aGlzLmZpbmREZWZpbmVkKFxuICAgICAgICAgIHRoaXMuY3VzdG9tTWVzc2FnZSggZWxlbWVudC5uYW1lLCBydWxlLm1ldGhvZCApLFxuICAgICAgICAgIHRoaXMuY3VzdG9tRGF0YU1lc3NhZ2UoIGVsZW1lbnQsIHJ1bGUubWV0aG9kICksXG5cbiAgICAgICAgICAvLyAndGl0bGUnIGlzIG5ldmVyIHVuZGVmaW5lZCwgc28gaGFuZGxlIGVtcHR5IHN0cmluZyBhcyB1bmRlZmluZWRcbiAgICAgICAgICAhdGhpcy5zZXR0aW5ncy5pZ25vcmVUaXRsZSAmJiBlbGVtZW50LnRpdGxlIHx8IHVuZGVmaW5lZCxcbiAgICAgICAgICAkLnZhbGlkYXRvci5tZXNzYWdlc1sgcnVsZS5tZXRob2QgXSxcbiAgICAgICAgICBcIjxzdHJvbmc+V2FybmluZzogTm8gbWVzc2FnZSBkZWZpbmVkIGZvciBcIiArIGVsZW1lbnQubmFtZSArIFwiPC9zdHJvbmc+XCJcbiAgICAgICAgICApLFxuICAgICAgICAgIHRoZXJlZ2V4ID0gL1xcJD9cXHsoXFxkKylcXH0vZztcbiAgICAgICAgaWYgKCB0eXBlb2YgbWVzc2FnZSA9PT0gXCJmdW5jdGlvblwiICkge1xuICAgICAgICAgIG1lc3NhZ2UgPSBtZXNzYWdlLmNhbGwoIHRoaXMsIHJ1bGUucGFyYW1ldGVycywgZWxlbWVudCApO1xuICAgICAgICB9IGVsc2UgaWYgKCB0aGVyZWdleC50ZXN0KCBtZXNzYWdlICkgKSB7XG4gICAgICAgICAgbWVzc2FnZSA9ICQudmFsaWRhdG9yLmZvcm1hdCggbWVzc2FnZS5yZXBsYWNlKCB0aGVyZWdleCwgXCJ7JDF9XCIgKSwgcnVsZS5wYXJhbWV0ZXJzICk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICAgIH0sXG5cbiAgICAgIGZvcm1hdEFuZEFkZDogZnVuY3Rpb24oIGVsZW1lbnQsIHJ1bGUgKSB7XG4gICAgICAgIHZhciBtZXNzYWdlID0gdGhpcy5kZWZhdWx0TWVzc2FnZSggZWxlbWVudCwgcnVsZSApO1xuXG4gICAgICAgIHRoaXMuZXJyb3JMaXN0LnB1c2goIHtcbiAgICAgICAgICBtZXNzYWdlOiBtZXNzYWdlLFxuICAgICAgICAgIGVsZW1lbnQ6IGVsZW1lbnQsXG4gICAgICAgICAgbWV0aG9kOiBydWxlLm1ldGhvZFxuICAgICAgICB9ICk7XG5cbiAgICAgICAgdGhpcy5lcnJvck1hcFsgZWxlbWVudC5uYW1lIF0gPSBtZXNzYWdlO1xuICAgICAgICB0aGlzLnN1Ym1pdHRlZFsgZWxlbWVudC5uYW1lIF0gPSBtZXNzYWdlO1xuICAgICAgfSxcblxuICAgICAgYWRkV3JhcHBlcjogZnVuY3Rpb24oIHRvVG9nZ2xlICkge1xuICAgICAgICBpZiAoIHRoaXMuc2V0dGluZ3Mud3JhcHBlciApIHtcbiAgICAgICAgICB0b1RvZ2dsZSA9IHRvVG9nZ2xlLmFkZCggdG9Ub2dnbGUucGFyZW50KCB0aGlzLnNldHRpbmdzLndyYXBwZXIgKSApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0b1RvZ2dsZTtcbiAgICAgIH0sXG5cbiAgICAgIGRlZmF1bHRTaG93RXJyb3JzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIGksIGVsZW1lbnRzLCBlcnJvcjtcbiAgICAgICAgZm9yICggaSA9IDA7IHRoaXMuZXJyb3JMaXN0WyBpIF07IGkrKyApIHtcbiAgICAgICAgICBlcnJvciA9IHRoaXMuZXJyb3JMaXN0WyBpIF07XG4gICAgICAgICAgaWYgKCB0aGlzLnNldHRpbmdzLmhpZ2hsaWdodCApIHtcbiAgICAgICAgICAgIHRoaXMuc2V0dGluZ3MuaGlnaGxpZ2h0LmNhbGwoIHRoaXMsIGVycm9yLmVsZW1lbnQsIHRoaXMuc2V0dGluZ3MuZXJyb3JDbGFzcywgdGhpcy5zZXR0aW5ncy52YWxpZENsYXNzICk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuc2hvd0xhYmVsKCBlcnJvci5lbGVtZW50LCBlcnJvci5tZXNzYWdlICk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCB0aGlzLmVycm9yTGlzdC5sZW5ndGggKSB7XG4gICAgICAgICAgdGhpcy50b1Nob3cgPSB0aGlzLnRvU2hvdy5hZGQoIHRoaXMuY29udGFpbmVycyApO1xuICAgICAgICB9XG4gICAgICAgIGlmICggdGhpcy5zZXR0aW5ncy5zdWNjZXNzICkge1xuICAgICAgICAgIGZvciAoIGkgPSAwOyB0aGlzLnN1Y2Nlc3NMaXN0WyBpIF07IGkrKyApIHtcbiAgICAgICAgICAgIHRoaXMuc2hvd0xhYmVsKCB0aGlzLnN1Y2Nlc3NMaXN0WyBpIF0gKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCB0aGlzLnNldHRpbmdzLnVuaGlnaGxpZ2h0ICkge1xuICAgICAgICAgIGZvciAoIGkgPSAwLCBlbGVtZW50cyA9IHRoaXMudmFsaWRFbGVtZW50cygpOyBlbGVtZW50c1sgaSBdOyBpKysgKSB7XG4gICAgICAgICAgICB0aGlzLnNldHRpbmdzLnVuaGlnaGxpZ2h0LmNhbGwoIHRoaXMsIGVsZW1lbnRzWyBpIF0sIHRoaXMuc2V0dGluZ3MuZXJyb3JDbGFzcywgdGhpcy5zZXR0aW5ncy52YWxpZENsYXNzICk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMudG9IaWRlID0gdGhpcy50b0hpZGUubm90KCB0aGlzLnRvU2hvdyApO1xuICAgICAgICB0aGlzLmhpZGVFcnJvcnMoKTtcbiAgICAgICAgdGhpcy5hZGRXcmFwcGVyKCB0aGlzLnRvU2hvdyApLnNob3coKTtcbiAgICAgIH0sXG5cbiAgICAgIHZhbGlkRWxlbWVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50RWxlbWVudHMubm90KCB0aGlzLmludmFsaWRFbGVtZW50cygpICk7XG4gICAgICB9LFxuXG4gICAgICBpbnZhbGlkRWxlbWVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gJCggdGhpcy5lcnJvckxpc3QgKS5tYXAoIGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQ7XG4gICAgICAgIH0gKTtcbiAgICAgIH0sXG5cbiAgICAgIHNob3dMYWJlbDogZnVuY3Rpb24oIGVsZW1lbnQsIG1lc3NhZ2UgKSB7XG4gICAgICAgIHZhciBwbGFjZSwgZ3JvdXAsIGVycm9ySUQsIHYsXG4gICAgICAgICAgZXJyb3IgPSB0aGlzLmVycm9yc0ZvciggZWxlbWVudCApLFxuICAgICAgICAgIGVsZW1lbnRJRCA9IHRoaXMuaWRPck5hbWUoIGVsZW1lbnQgKSxcbiAgICAgICAgICBkZXNjcmliZWRCeSA9ICQoIGVsZW1lbnQgKS5hdHRyKCBcImFyaWEtZGVzY3JpYmVkYnlcIiApO1xuXG4gICAgICAgIGlmICggZXJyb3IubGVuZ3RoICkge1xuXG4gICAgICAgICAgLy8gUmVmcmVzaCBlcnJvci9zdWNjZXNzIGNsYXNzXG4gICAgICAgICAgZXJyb3IucmVtb3ZlQ2xhc3MoIHRoaXMuc2V0dGluZ3MudmFsaWRDbGFzcyApLmFkZENsYXNzKCB0aGlzLnNldHRpbmdzLmVycm9yQ2xhc3MgKTtcblxuICAgICAgICAgIC8vIFJlcGxhY2UgbWVzc2FnZSBvbiBleGlzdGluZyBsYWJlbFxuICAgICAgICAgIGVycm9yLmh0bWwoIG1lc3NhZ2UgKTtcbiAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgIC8vIENyZWF0ZSBlcnJvciBlbGVtZW50XG4gICAgICAgICAgZXJyb3IgPSAkKCBcIjxcIiArIHRoaXMuc2V0dGluZ3MuZXJyb3JFbGVtZW50ICsgXCI+XCIgKVxuICAgICAgICAgICAgLmF0dHIoIFwiaWRcIiwgZWxlbWVudElEICsgXCItZXJyb3JcIiApXG4gICAgICAgICAgICAuYWRkQ2xhc3MoIHRoaXMuc2V0dGluZ3MuZXJyb3JDbGFzcyApXG4gICAgICAgICAgICAuaHRtbCggbWVzc2FnZSB8fCBcIlwiICk7XG5cbiAgICAgICAgICAvLyBNYWludGFpbiByZWZlcmVuY2UgdG8gdGhlIGVsZW1lbnQgdG8gYmUgcGxhY2VkIGludG8gdGhlIERPTVxuICAgICAgICAgIHBsYWNlID0gZXJyb3I7XG4gICAgICAgICAgaWYgKCB0aGlzLnNldHRpbmdzLndyYXBwZXIgKSB7XG5cbiAgICAgICAgICAgIC8vIE1ha2Ugc3VyZSB0aGUgZWxlbWVudCBpcyB2aXNpYmxlLCBldmVuIGluIElFXG4gICAgICAgICAgICAvLyBhY3R1YWxseSBzaG93aW5nIHRoZSB3cmFwcGVkIGVsZW1lbnQgaXMgaGFuZGxlZCBlbHNld2hlcmVcbiAgICAgICAgICAgIHBsYWNlID0gZXJyb3IuaGlkZSgpLnNob3coKS53cmFwKCBcIjxcIiArIHRoaXMuc2V0dGluZ3Mud3JhcHBlciArIFwiLz5cIiApLnBhcmVudCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoIHRoaXMubGFiZWxDb250YWluZXIubGVuZ3RoICkge1xuICAgICAgICAgICAgdGhpcy5sYWJlbENvbnRhaW5lci5hcHBlbmQoIHBsYWNlICk7XG4gICAgICAgICAgfSBlbHNlIGlmICggdGhpcy5zZXR0aW5ncy5lcnJvclBsYWNlbWVudCApIHtcbiAgICAgICAgICAgIHRoaXMuc2V0dGluZ3MuZXJyb3JQbGFjZW1lbnQuY2FsbCggdGhpcywgcGxhY2UsICQoIGVsZW1lbnQgKSApO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBwbGFjZS5pbnNlcnRBZnRlciggZWxlbWVudCApO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vIExpbmsgZXJyb3IgYmFjayB0byB0aGUgZWxlbWVudFxuICAgICAgICAgIGlmICggZXJyb3IuaXMoIFwibGFiZWxcIiApICkge1xuXG4gICAgICAgICAgICAvLyBJZiB0aGUgZXJyb3IgaXMgYSBsYWJlbCwgdGhlbiBhc3NvY2lhdGUgdXNpbmcgJ2ZvcidcbiAgICAgICAgICAgIGVycm9yLmF0dHIoIFwiZm9yXCIsIGVsZW1lbnRJRCApO1xuXG4gICAgICAgICAgICAvLyBJZiB0aGUgZWxlbWVudCBpcyBub3QgYSBjaGlsZCBvZiBhbiBhc3NvY2lhdGVkIGxhYmVsLCB0aGVuIGl0J3MgbmVjZXNzYXJ5XG4gICAgICAgICAgICAvLyB0byBleHBsaWNpdGx5IGFwcGx5IGFyaWEtZGVzY3JpYmVkYnlcbiAgICAgICAgICB9IGVsc2UgaWYgKCBlcnJvci5wYXJlbnRzKCBcImxhYmVsW2Zvcj0nXCIgKyB0aGlzLmVzY2FwZUNzc01ldGEoIGVsZW1lbnRJRCApICsgXCInXVwiICkubGVuZ3RoID09PSAwICkge1xuICAgICAgICAgICAgZXJyb3JJRCA9IGVycm9yLmF0dHIoIFwiaWRcIiApO1xuXG4gICAgICAgICAgICAvLyBSZXNwZWN0IGV4aXN0aW5nIG5vbi1lcnJvciBhcmlhLWRlc2NyaWJlZGJ5XG4gICAgICAgICAgICBpZiAoICFkZXNjcmliZWRCeSApIHtcbiAgICAgICAgICAgICAgZGVzY3JpYmVkQnkgPSBlcnJvcklEO1xuICAgICAgICAgICAgfSBlbHNlIGlmICggIWRlc2NyaWJlZEJ5Lm1hdGNoKCBuZXcgUmVnRXhwKCBcIlxcXFxiXCIgKyB0aGlzLmVzY2FwZUNzc01ldGEoIGVycm9ySUQgKSArIFwiXFxcXGJcIiApICkgKSB7XG5cbiAgICAgICAgICAgICAgLy8gQWRkIHRvIGVuZCBvZiBsaXN0IGlmIG5vdCBhbHJlYWR5IHByZXNlbnRcbiAgICAgICAgICAgICAgZGVzY3JpYmVkQnkgKz0gXCIgXCIgKyBlcnJvcklEO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJCggZWxlbWVudCApLmF0dHIoIFwiYXJpYS1kZXNjcmliZWRieVwiLCBkZXNjcmliZWRCeSApO1xuXG4gICAgICAgICAgICAvLyBJZiB0aGlzIGVsZW1lbnQgaXMgZ3JvdXBlZCwgdGhlbiBhc3NpZ24gdG8gYWxsIGVsZW1lbnRzIGluIHRoZSBzYW1lIGdyb3VwXG4gICAgICAgICAgICBncm91cCA9IHRoaXMuZ3JvdXBzWyBlbGVtZW50Lm5hbWUgXTtcbiAgICAgICAgICAgIGlmICggZ3JvdXAgKSB7XG4gICAgICAgICAgICAgIHYgPSB0aGlzO1xuICAgICAgICAgICAgICAkLmVhY2goIHYuZ3JvdXBzLCBmdW5jdGlvbiggbmFtZSwgdGVzdGdyb3VwICkge1xuICAgICAgICAgICAgICAgIGlmICggdGVzdGdyb3VwID09PSBncm91cCApIHtcbiAgICAgICAgICAgICAgICAgICQoIFwiW25hbWU9J1wiICsgdi5lc2NhcGVDc3NNZXRhKCBuYW1lICkgKyBcIiddXCIsIHYuY3VycmVudEZvcm0gKVxuICAgICAgICAgICAgICAgICAgICAuYXR0ciggXCJhcmlhLWRlc2NyaWJlZGJ5XCIsIGVycm9yLmF0dHIoIFwiaWRcIiApICk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9ICk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmICggIW1lc3NhZ2UgJiYgdGhpcy5zZXR0aW5ncy5zdWNjZXNzICkge1xuICAgICAgICAgIGVycm9yLnRleHQoIFwiXCIgKTtcbiAgICAgICAgICBpZiAoIHR5cGVvZiB0aGlzLnNldHRpbmdzLnN1Y2Nlc3MgPT09IFwic3RyaW5nXCIgKSB7XG4gICAgICAgICAgICBlcnJvci5hZGRDbGFzcyggdGhpcy5zZXR0aW5ncy5zdWNjZXNzICk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc2V0dGluZ3Muc3VjY2VzcyggZXJyb3IsIGVsZW1lbnQgKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy50b1Nob3cgPSB0aGlzLnRvU2hvdy5hZGQoIGVycm9yICk7XG4gICAgICB9LFxuXG4gICAgICBlcnJvcnNGb3I6IGZ1bmN0aW9uKCBlbGVtZW50ICkge1xuICAgICAgICB2YXIgbmFtZSA9IHRoaXMuZXNjYXBlQ3NzTWV0YSggdGhpcy5pZE9yTmFtZSggZWxlbWVudCApICksXG4gICAgICAgICAgZGVzY3JpYmVyID0gJCggZWxlbWVudCApLmF0dHIoIFwiYXJpYS1kZXNjcmliZWRieVwiICksXG4gICAgICAgICAgc2VsZWN0b3IgPSBcImxhYmVsW2Zvcj0nXCIgKyBuYW1lICsgXCInXSwgbGFiZWxbZm9yPSdcIiArIG5hbWUgKyBcIiddICpcIjtcblxuICAgICAgICAvLyAnYXJpYS1kZXNjcmliZWRieScgc2hvdWxkIGRpcmVjdGx5IHJlZmVyZW5jZSB0aGUgZXJyb3IgZWxlbWVudFxuICAgICAgICBpZiAoIGRlc2NyaWJlciApIHtcbiAgICAgICAgICBzZWxlY3RvciA9IHNlbGVjdG9yICsgXCIsICNcIiArIHRoaXMuZXNjYXBlQ3NzTWV0YSggZGVzY3JpYmVyIClcbiAgICAgICAgICAgIC5yZXBsYWNlKCAvXFxzKy9nLCBcIiwgI1wiICk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGhpc1xuICAgICAgICAgIC5lcnJvcnMoKVxuICAgICAgICAgIC5maWx0ZXIoIHNlbGVjdG9yICk7XG4gICAgICB9LFxuXG4gICAgICAvLyBTZWUgaHR0cHM6Ly9hcGkuanF1ZXJ5LmNvbS9jYXRlZ29yeS9zZWxlY3RvcnMvLCBmb3IgQ1NTXG4gICAgICAvLyBtZXRhLWNoYXJhY3RlcnMgdGhhdCBzaG91bGQgYmUgZXNjYXBlZCBpbiBvcmRlciB0byBiZSB1c2VkIHdpdGggSlF1ZXJ5XG4gICAgICAvLyBhcyBhIGxpdGVyYWwgcGFydCBvZiBhIG5hbWUvaWQgb3IgYW55IHNlbGVjdG9yLlxuICAgICAgZXNjYXBlQ3NzTWV0YTogZnVuY3Rpb24oIHN0cmluZyApIHtcbiAgICAgICAgcmV0dXJuIHN0cmluZy5yZXBsYWNlKCAvKFtcXFxcIVwiIyQlJicoKSorLC4vOjs8PT4/QFxcW1xcXV5ge3x9fl0pL2csIFwiXFxcXCQxXCIgKTtcbiAgICAgIH0sXG5cbiAgICAgIGlkT3JOYW1lOiBmdW5jdGlvbiggZWxlbWVudCApIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ3JvdXBzWyBlbGVtZW50Lm5hbWUgXSB8fCAoIHRoaXMuY2hlY2thYmxlKCBlbGVtZW50ICkgPyBlbGVtZW50Lm5hbWUgOiBlbGVtZW50LmlkIHx8IGVsZW1lbnQubmFtZSApO1xuICAgICAgfSxcblxuICAgICAgdmFsaWRhdGlvblRhcmdldEZvcjogZnVuY3Rpb24oIGVsZW1lbnQgKSB7XG5cbiAgICAgICAgLy8gSWYgcmFkaW8vY2hlY2tib3gsIHZhbGlkYXRlIGZpcnN0IGVsZW1lbnQgaW4gZ3JvdXAgaW5zdGVhZFxuICAgICAgICBpZiAoIHRoaXMuY2hlY2thYmxlKCBlbGVtZW50ICkgKSB7XG4gICAgICAgICAgZWxlbWVudCA9IHRoaXMuZmluZEJ5TmFtZSggZWxlbWVudC5uYW1lICk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBBbHdheXMgYXBwbHkgaWdub3JlIGZpbHRlclxuICAgICAgICByZXR1cm4gJCggZWxlbWVudCApLm5vdCggdGhpcy5zZXR0aW5ncy5pZ25vcmUgKVsgMCBdO1xuICAgICAgfSxcblxuICAgICAgY2hlY2thYmxlOiBmdW5jdGlvbiggZWxlbWVudCApIHtcbiAgICAgICAgcmV0dXJuICggL3JhZGlvfGNoZWNrYm94L2kgKS50ZXN0KCBlbGVtZW50LnR5cGUgKTtcbiAgICAgIH0sXG5cbiAgICAgIGZpbmRCeU5hbWU6IGZ1bmN0aW9uKCBuYW1lICkge1xuICAgICAgICByZXR1cm4gJCggdGhpcy5jdXJyZW50Rm9ybSApLmZpbmQoIFwiW25hbWU9J1wiICsgdGhpcy5lc2NhcGVDc3NNZXRhKCBuYW1lICkgKyBcIiddXCIgKTtcbiAgICAgIH0sXG5cbiAgICAgIGdldExlbmd0aDogZnVuY3Rpb24oIHZhbHVlLCBlbGVtZW50ICkge1xuICAgICAgICBzd2l0Y2ggKCBlbGVtZW50Lm5vZGVOYW1lLnRvTG93ZXJDYXNlKCkgKSB7XG4gICAgICAgICAgY2FzZSBcInNlbGVjdFwiOlxuICAgICAgICAgICAgcmV0dXJuICQoIFwib3B0aW9uOnNlbGVjdGVkXCIsIGVsZW1lbnQgKS5sZW5ndGg7XG4gICAgICAgICAgY2FzZSBcImlucHV0XCI6XG4gICAgICAgICAgICBpZiAoIHRoaXMuY2hlY2thYmxlKCBlbGVtZW50ICkgKSB7XG4gICAgICAgICAgICAgIHJldHVybiB0aGlzLmZpbmRCeU5hbWUoIGVsZW1lbnQubmFtZSApLmZpbHRlciggXCI6Y2hlY2tlZFwiICkubGVuZ3RoO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB2YWx1ZS5sZW5ndGg7XG4gICAgICB9LFxuXG4gICAgICBkZXBlbmQ6IGZ1bmN0aW9uKCBwYXJhbSwgZWxlbWVudCApIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVwZW5kVHlwZXNbIHR5cGVvZiBwYXJhbSBdID8gdGhpcy5kZXBlbmRUeXBlc1sgdHlwZW9mIHBhcmFtIF0oIHBhcmFtLCBlbGVtZW50ICkgOiB0cnVlO1xuICAgICAgfSxcblxuICAgICAgZGVwZW5kVHlwZXM6IHtcbiAgICAgICAgXCJib29sZWFuXCI6IGZ1bmN0aW9uKCBwYXJhbSApIHtcbiAgICAgICAgICByZXR1cm4gcGFyYW07XG4gICAgICAgIH0sXG4gICAgICAgIFwic3RyaW5nXCI6IGZ1bmN0aW9uKCBwYXJhbSwgZWxlbWVudCApIHtcbiAgICAgICAgICByZXR1cm4gISEkKCBwYXJhbSwgZWxlbWVudC5mb3JtICkubGVuZ3RoO1xuICAgICAgICB9LFxuICAgICAgICBcImZ1bmN0aW9uXCI6IGZ1bmN0aW9uKCBwYXJhbSwgZWxlbWVudCApIHtcbiAgICAgICAgICByZXR1cm4gcGFyYW0oIGVsZW1lbnQgKTtcbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgb3B0aW9uYWw6IGZ1bmN0aW9uKCBlbGVtZW50ICkge1xuICAgICAgICB2YXIgdmFsID0gdGhpcy5lbGVtZW50VmFsdWUoIGVsZW1lbnQgKTtcbiAgICAgICAgcmV0dXJuICEkLnZhbGlkYXRvci5tZXRob2RzLnJlcXVpcmVkLmNhbGwoIHRoaXMsIHZhbCwgZWxlbWVudCApICYmIFwiZGVwZW5kZW5jeS1taXNtYXRjaFwiO1xuICAgICAgfSxcblxuICAgICAgc3RhcnRSZXF1ZXN0OiBmdW5jdGlvbiggZWxlbWVudCApIHtcbiAgICAgICAgaWYgKCAhdGhpcy5wZW5kaW5nWyBlbGVtZW50Lm5hbWUgXSApIHtcbiAgICAgICAgICB0aGlzLnBlbmRpbmdSZXF1ZXN0Kys7XG4gICAgICAgICAgJCggZWxlbWVudCApLmFkZENsYXNzKCB0aGlzLnNldHRpbmdzLnBlbmRpbmdDbGFzcyApO1xuICAgICAgICAgIHRoaXMucGVuZGluZ1sgZWxlbWVudC5uYW1lIF0gPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9LFxuXG4gICAgICBzdG9wUmVxdWVzdDogZnVuY3Rpb24oIGVsZW1lbnQsIHZhbGlkICkge1xuICAgICAgICB0aGlzLnBlbmRpbmdSZXF1ZXN0LS07XG5cbiAgICAgICAgLy8gU29tZXRpbWVzIHN5bmNocm9uaXphdGlvbiBmYWlscywgbWFrZSBzdXJlIHBlbmRpbmdSZXF1ZXN0IGlzIG5ldmVyIDwgMFxuICAgICAgICBpZiAoIHRoaXMucGVuZGluZ1JlcXVlc3QgPCAwICkge1xuICAgICAgICAgIHRoaXMucGVuZGluZ1JlcXVlc3QgPSAwO1xuICAgICAgICB9XG4gICAgICAgIGRlbGV0ZSB0aGlzLnBlbmRpbmdbIGVsZW1lbnQubmFtZSBdO1xuICAgICAgICAkKCBlbGVtZW50ICkucmVtb3ZlQ2xhc3MoIHRoaXMuc2V0dGluZ3MucGVuZGluZ0NsYXNzICk7XG4gICAgICAgIGlmICggdmFsaWQgJiYgdGhpcy5wZW5kaW5nUmVxdWVzdCA9PT0gMCAmJiB0aGlzLmZvcm1TdWJtaXR0ZWQgJiYgdGhpcy5mb3JtKCkgKSB7XG4gICAgICAgICAgJCggdGhpcy5jdXJyZW50Rm9ybSApLnN1Ym1pdCgpO1xuXG4gICAgICAgICAgLy8gUmVtb3ZlIHRoZSBoaWRkZW4gaW5wdXQgdGhhdCB3YXMgdXNlZCBhcyBhIHJlcGxhY2VtZW50IGZvciB0aGVcbiAgICAgICAgICAvLyBtaXNzaW5nIHN1Ym1pdCBidXR0b24uIFRoZSBoaWRkZW4gaW5wdXQgaXMgYWRkZWQgYnkgYGhhbmRsZSgpYFxuICAgICAgICAgIC8vIHRvIGVuc3VyZSB0aGF0IHRoZSB2YWx1ZSBvZiB0aGUgdXNlZCBzdWJtaXQgYnV0dG9uIGlzIHBhc3NlZCBvblxuICAgICAgICAgIC8vIGZvciBzY3JpcHRlZCBzdWJtaXRzIHRyaWdnZXJlZCBieSB0aGlzIG1ldGhvZFxuICAgICAgICAgIGlmICggdGhpcy5zdWJtaXRCdXR0b24gKSB7XG4gICAgICAgICAgICAkKCBcImlucHV0OmhpZGRlbltuYW1lPSdcIiArIHRoaXMuc3VibWl0QnV0dG9uLm5hbWUgKyBcIiddXCIsIHRoaXMuY3VycmVudEZvcm0gKS5yZW1vdmUoKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB0aGlzLmZvcm1TdWJtaXR0ZWQgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIGlmICggIXZhbGlkICYmIHRoaXMucGVuZGluZ1JlcXVlc3QgPT09IDAgJiYgdGhpcy5mb3JtU3VibWl0dGVkICkge1xuICAgICAgICAgICQoIHRoaXMuY3VycmVudEZvcm0gKS50cmlnZ2VySGFuZGxlciggXCJpbnZhbGlkLWZvcm1cIiwgWyB0aGlzIF0gKTtcbiAgICAgICAgICB0aGlzLmZvcm1TdWJtaXR0ZWQgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgcHJldmlvdXNWYWx1ZTogZnVuY3Rpb24oIGVsZW1lbnQsIG1ldGhvZCApIHtcbiAgICAgICAgbWV0aG9kID0gdHlwZW9mIG1ldGhvZCA9PT0gXCJzdHJpbmdcIiAmJiBtZXRob2QgfHwgXCJyZW1vdGVcIjtcblxuICAgICAgICByZXR1cm4gJC5kYXRhKCBlbGVtZW50LCBcInByZXZpb3VzVmFsdWVcIiApIHx8ICQuZGF0YSggZWxlbWVudCwgXCJwcmV2aW91c1ZhbHVlXCIsIHtcbiAgICAgICAgICBvbGQ6IG51bGwsXG4gICAgICAgICAgdmFsaWQ6IHRydWUsXG4gICAgICAgICAgbWVzc2FnZTogdGhpcy5kZWZhdWx0TWVzc2FnZSggZWxlbWVudCwgeyBtZXRob2Q6IG1ldGhvZCB9IClcbiAgICAgICAgfSApO1xuICAgICAgfSxcblxuICAgICAgLy8gQ2xlYW5zIHVwIGFsbCBmb3JtcyBhbmQgZWxlbWVudHMsIHJlbW92ZXMgdmFsaWRhdG9yLXNwZWNpZmljIGV2ZW50c1xuICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgIHRoaXMucmVzZXRGb3JtKCk7XG5cbiAgICAgICAgJCggdGhpcy5jdXJyZW50Rm9ybSApXG4gICAgICAgICAgLm9mZiggXCIudmFsaWRhdGVcIiApXG4gICAgICAgICAgLnJlbW92ZURhdGEoIFwidmFsaWRhdG9yXCIgKVxuICAgICAgICAgIC5maW5kKCBcIi52YWxpZGF0ZS1lcXVhbFRvLWJsdXJcIiApXG4gICAgICAgICAgLm9mZiggXCIudmFsaWRhdGUtZXF1YWxUb1wiIClcbiAgICAgICAgICAucmVtb3ZlQ2xhc3MoIFwidmFsaWRhdGUtZXF1YWxUby1ibHVyXCIgKVxuICAgICAgICAgIC5maW5kKCBcIi52YWxpZGF0ZS1sZXNzVGhhbi1ibHVyXCIgKVxuICAgICAgICAgIC5vZmYoIFwiLnZhbGlkYXRlLWxlc3NUaGFuXCIgKVxuICAgICAgICAgIC5yZW1vdmVDbGFzcyggXCJ2YWxpZGF0ZS1sZXNzVGhhbi1ibHVyXCIgKVxuICAgICAgICAgIC5maW5kKCBcIi52YWxpZGF0ZS1sZXNzVGhhbkVxdWFsLWJsdXJcIiApXG4gICAgICAgICAgLm9mZiggXCIudmFsaWRhdGUtbGVzc1RoYW5FcXVhbFwiIClcbiAgICAgICAgICAucmVtb3ZlQ2xhc3MoIFwidmFsaWRhdGUtbGVzc1RoYW5FcXVhbC1ibHVyXCIgKVxuICAgICAgICAgIC5maW5kKCBcIi52YWxpZGF0ZS1ncmVhdGVyVGhhbkVxdWFsLWJsdXJcIiApXG4gICAgICAgICAgLm9mZiggXCIudmFsaWRhdGUtZ3JlYXRlclRoYW5FcXVhbFwiIClcbiAgICAgICAgICAucmVtb3ZlQ2xhc3MoIFwidmFsaWRhdGUtZ3JlYXRlclRoYW5FcXVhbC1ibHVyXCIgKVxuICAgICAgICAgIC5maW5kKCBcIi52YWxpZGF0ZS1ncmVhdGVyVGhhbi1ibHVyXCIgKVxuICAgICAgICAgIC5vZmYoIFwiLnZhbGlkYXRlLWdyZWF0ZXJUaGFuXCIgKVxuICAgICAgICAgIC5yZW1vdmVDbGFzcyggXCJ2YWxpZGF0ZS1ncmVhdGVyVGhhbi1ibHVyXCIgKTtcbiAgICAgIH1cblxuICAgIH0sXG5cbiAgICBjbGFzc1J1bGVTZXR0aW5nczoge1xuICAgICAgcmVxdWlyZWQ6IHsgcmVxdWlyZWQ6IHRydWUgfSxcbiAgICAgIGVtYWlsOiB7IGVtYWlsOiB0cnVlIH0sXG4gICAgICB1cmw6IHsgdXJsOiB0cnVlIH0sXG4gICAgICBkYXRlOiB7IGRhdGU6IHRydWUgfSxcbiAgICAgIGRhdGVJU086IHsgZGF0ZUlTTzogdHJ1ZSB9LFxuICAgICAgbnVtYmVyOiB7IG51bWJlcjogdHJ1ZSB9LFxuICAgICAgZGlnaXRzOiB7IGRpZ2l0czogdHJ1ZSB9LFxuICAgICAgY3JlZGl0Y2FyZDogeyBjcmVkaXRjYXJkOiB0cnVlIH1cbiAgICB9LFxuXG4gICAgYWRkQ2xhc3NSdWxlczogZnVuY3Rpb24oIGNsYXNzTmFtZSwgcnVsZXMgKSB7XG4gICAgICBpZiAoIGNsYXNzTmFtZS5jb25zdHJ1Y3RvciA9PT0gU3RyaW5nICkge1xuICAgICAgICB0aGlzLmNsYXNzUnVsZVNldHRpbmdzWyBjbGFzc05hbWUgXSA9IHJ1bGVzO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgJC5leHRlbmQoIHRoaXMuY2xhc3NSdWxlU2V0dGluZ3MsIGNsYXNzTmFtZSApO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICBjbGFzc1J1bGVzOiBmdW5jdGlvbiggZWxlbWVudCApIHtcbiAgICAgIHZhciBydWxlcyA9IHt9LFxuICAgICAgICBjbGFzc2VzID0gJCggZWxlbWVudCApLmF0dHIoIFwiY2xhc3NcIiApO1xuXG4gICAgICBpZiAoIGNsYXNzZXMgKSB7XG4gICAgICAgICQuZWFjaCggY2xhc3Nlcy5zcGxpdCggXCIgXCIgKSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYgKCB0aGlzIGluICQudmFsaWRhdG9yLmNsYXNzUnVsZVNldHRpbmdzICkge1xuICAgICAgICAgICAgJC5leHRlbmQoIHJ1bGVzLCAkLnZhbGlkYXRvci5jbGFzc1J1bGVTZXR0aW5nc1sgdGhpcyBdICk7XG4gICAgICAgICAgfVxuICAgICAgICB9ICk7XG4gICAgICB9XG4gICAgICByZXR1cm4gcnVsZXM7XG4gICAgfSxcblxuICAgIG5vcm1hbGl6ZUF0dHJpYnV0ZVJ1bGU6IGZ1bmN0aW9uKCBydWxlcywgdHlwZSwgbWV0aG9kLCB2YWx1ZSApIHtcblxuICAgICAgLy8gQ29udmVydCB0aGUgdmFsdWUgdG8gYSBudW1iZXIgZm9yIG51bWJlciBpbnB1dHMsIGFuZCBmb3IgdGV4dCBmb3IgYmFja3dhcmRzIGNvbXBhYmlsaXR5XG4gICAgICAvLyBhbGxvd3MgdHlwZT1cImRhdGVcIiBhbmQgb3RoZXJzIHRvIGJlIGNvbXBhcmVkIGFzIHN0cmluZ3NcbiAgICAgIGlmICggL21pbnxtYXh8c3RlcC8udGVzdCggbWV0aG9kICkgJiYgKCB0eXBlID09PSBudWxsIHx8IC9udW1iZXJ8cmFuZ2V8dGV4dC8udGVzdCggdHlwZSApICkgKSB7XG4gICAgICAgIHZhbHVlID0gTnVtYmVyKCB2YWx1ZSApO1xuXG4gICAgICAgIC8vIFN1cHBvcnQgT3BlcmEgTWluaSwgd2hpY2ggcmV0dXJucyBOYU4gZm9yIHVuZGVmaW5lZCBtaW5sZW5ndGhcbiAgICAgICAgaWYgKCBpc05hTiggdmFsdWUgKSApIHtcbiAgICAgICAgICB2YWx1ZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoIHZhbHVlIHx8IHZhbHVlID09PSAwICkge1xuICAgICAgICBydWxlc1sgbWV0aG9kIF0gPSB2YWx1ZTtcbiAgICAgIH0gZWxzZSBpZiAoIHR5cGUgPT09IG1ldGhvZCAmJiB0eXBlICE9PSBcInJhbmdlXCIgKSB7XG5cbiAgICAgICAgLy8gRXhjZXB0aW9uOiB0aGUganF1ZXJ5IHZhbGlkYXRlICdyYW5nZScgbWV0aG9kXG4gICAgICAgIC8vIGRvZXMgbm90IHRlc3QgZm9yIHRoZSBodG1sNSAncmFuZ2UnIHR5cGVcbiAgICAgICAgcnVsZXNbIG1ldGhvZCBdID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgYXR0cmlidXRlUnVsZXM6IGZ1bmN0aW9uKCBlbGVtZW50ICkge1xuICAgICAgdmFyIHJ1bGVzID0ge30sXG4gICAgICAgICRlbGVtZW50ID0gJCggZWxlbWVudCApLFxuICAgICAgICB0eXBlID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoIFwidHlwZVwiICksXG4gICAgICAgIG1ldGhvZCwgdmFsdWU7XG5cbiAgICAgIGZvciAoIG1ldGhvZCBpbiAkLnZhbGlkYXRvci5tZXRob2RzICkge1xuXG4gICAgICAgIC8vIFN1cHBvcnQgZm9yIDxpbnB1dCByZXF1aXJlZD4gaW4gYm90aCBodG1sNSBhbmQgb2xkZXIgYnJvd3NlcnNcbiAgICAgICAgaWYgKCBtZXRob2QgPT09IFwicmVxdWlyZWRcIiApIHtcbiAgICAgICAgICB2YWx1ZSA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCBtZXRob2QgKTtcblxuICAgICAgICAgIC8vIFNvbWUgYnJvd3NlcnMgcmV0dXJuIGFuIGVtcHR5IHN0cmluZyBmb3IgdGhlIHJlcXVpcmVkIGF0dHJpYnV0ZVxuICAgICAgICAgIC8vIGFuZCBub24tSFRNTDUgYnJvd3NlcnMgbWlnaHQgaGF2ZSByZXF1aXJlZD1cIlwiIG1hcmt1cFxuICAgICAgICAgIGlmICggdmFsdWUgPT09IFwiXCIgKSB7XG4gICAgICAgICAgICB2YWx1ZSA9IHRydWU7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gRm9yY2Ugbm9uLUhUTUw1IGJyb3dzZXJzIHRvIHJldHVybiBib29sXG4gICAgICAgICAgdmFsdWUgPSAhIXZhbHVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHZhbHVlID0gJGVsZW1lbnQuYXR0ciggbWV0aG9kICk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLm5vcm1hbGl6ZUF0dHJpYnV0ZVJ1bGUoIHJ1bGVzLCB0eXBlLCBtZXRob2QsIHZhbHVlICk7XG4gICAgICB9XG5cbiAgICAgIC8vICdtYXhsZW5ndGgnIG1heSBiZSByZXR1cm5lZCBhcyAtMSwgMjE0NzQ4MzY0NyAoIElFICkgYW5kIDUyNDI4OCAoIHNhZmFyaSApIGZvciB0ZXh0IGlucHV0c1xuICAgICAgaWYgKCBydWxlcy5tYXhsZW5ndGggJiYgLy0xfDIxNDc0ODM2NDd8NTI0Mjg4Ly50ZXN0KCBydWxlcy5tYXhsZW5ndGggKSApIHtcbiAgICAgICAgZGVsZXRlIHJ1bGVzLm1heGxlbmd0aDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHJ1bGVzO1xuICAgIH0sXG5cbiAgICBkYXRhUnVsZXM6IGZ1bmN0aW9uKCBlbGVtZW50ICkge1xuICAgICAgdmFyIHJ1bGVzID0ge30sXG4gICAgICAgICRlbGVtZW50ID0gJCggZWxlbWVudCApLFxuICAgICAgICB0eXBlID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoIFwidHlwZVwiICksXG4gICAgICAgIG1ldGhvZCwgdmFsdWU7XG5cbiAgICAgIGZvciAoIG1ldGhvZCBpbiAkLnZhbGlkYXRvci5tZXRob2RzICkge1xuICAgICAgICB2YWx1ZSA9ICRlbGVtZW50LmRhdGEoIFwicnVsZVwiICsgbWV0aG9kLmNoYXJBdCggMCApLnRvVXBwZXJDYXNlKCkgKyBtZXRob2Quc3Vic3RyaW5nKCAxICkudG9Mb3dlckNhc2UoKSApO1xuXG4gICAgICAgIC8vIENhc3QgZW1wdHkgYXR0cmlidXRlcyBsaWtlIGBkYXRhLXJ1bGUtcmVxdWlyZWRgIHRvIGB0cnVlYFxuICAgICAgICBpZiAoIHZhbHVlID09PSBcIlwiICkge1xuICAgICAgICAgIHZhbHVlID0gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubm9ybWFsaXplQXR0cmlidXRlUnVsZSggcnVsZXMsIHR5cGUsIG1ldGhvZCwgdmFsdWUgKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBydWxlcztcbiAgICB9LFxuXG4gICAgc3RhdGljUnVsZXM6IGZ1bmN0aW9uKCBlbGVtZW50ICkge1xuICAgICAgdmFyIHJ1bGVzID0ge30sXG4gICAgICAgIHZhbGlkYXRvciA9ICQuZGF0YSggZWxlbWVudC5mb3JtLCBcInZhbGlkYXRvclwiICk7XG5cbiAgICAgIGlmICggdmFsaWRhdG9yLnNldHRpbmdzLnJ1bGVzICkge1xuICAgICAgICBydWxlcyA9ICQudmFsaWRhdG9yLm5vcm1hbGl6ZVJ1bGUoIHZhbGlkYXRvci5zZXR0aW5ncy5ydWxlc1sgZWxlbWVudC5uYW1lIF0gKSB8fCB7fTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBydWxlcztcbiAgICB9LFxuXG4gICAgbm9ybWFsaXplUnVsZXM6IGZ1bmN0aW9uKCBydWxlcywgZWxlbWVudCApIHtcblxuICAgICAgLy8gSGFuZGxlIGRlcGVuZGVuY3kgY2hlY2tcbiAgICAgICQuZWFjaCggcnVsZXMsIGZ1bmN0aW9uKCBwcm9wLCB2YWwgKSB7XG5cbiAgICAgICAgLy8gSWdub3JlIHJ1bGUgd2hlbiBwYXJhbSBpcyBleHBsaWNpdGx5IGZhbHNlLCBlZy4gcmVxdWlyZWQ6ZmFsc2VcbiAgICAgICAgaWYgKCB2YWwgPT09IGZhbHNlICkge1xuICAgICAgICAgIGRlbGV0ZSBydWxlc1sgcHJvcCBdO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIHZhbC5wYXJhbSB8fCB2YWwuZGVwZW5kcyApIHtcbiAgICAgICAgICB2YXIga2VlcFJ1bGUgPSB0cnVlO1xuICAgICAgICAgIHN3aXRjaCAoIHR5cGVvZiB2YWwuZGVwZW5kcyApIHtcbiAgICAgICAgICAgIGNhc2UgXCJzdHJpbmdcIjpcbiAgICAgICAgICAgICAga2VlcFJ1bGUgPSAhISQoIHZhbC5kZXBlbmRzLCBlbGVtZW50LmZvcm0gKS5sZW5ndGg7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBcImZ1bmN0aW9uXCI6XG4gICAgICAgICAgICAgIGtlZXBSdWxlID0gdmFsLmRlcGVuZHMuY2FsbCggZWxlbWVudCwgZWxlbWVudCApO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCBrZWVwUnVsZSApIHtcbiAgICAgICAgICAgIHJ1bGVzWyBwcm9wIF0gPSB2YWwucGFyYW0gIT09IHVuZGVmaW5lZCA/IHZhbC5wYXJhbSA6IHRydWU7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQuZGF0YSggZWxlbWVudC5mb3JtLCBcInZhbGlkYXRvclwiICkucmVzZXRFbGVtZW50cyggJCggZWxlbWVudCApICk7XG4gICAgICAgICAgICBkZWxldGUgcnVsZXNbIHByb3AgXTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gKTtcblxuICAgICAgLy8gRXZhbHVhdGUgcGFyYW1ldGVyc1xuICAgICAgJC5lYWNoKCBydWxlcywgZnVuY3Rpb24oIHJ1bGUsIHBhcmFtZXRlciApIHtcbiAgICAgICAgcnVsZXNbIHJ1bGUgXSA9ICQuaXNGdW5jdGlvbiggcGFyYW1ldGVyICkgJiYgcnVsZSAhPT0gXCJub3JtYWxpemVyXCIgPyBwYXJhbWV0ZXIoIGVsZW1lbnQgKSA6IHBhcmFtZXRlcjtcbiAgICAgIH0gKTtcblxuICAgICAgLy8gQ2xlYW4gbnVtYmVyIHBhcmFtZXRlcnNcbiAgICAgICQuZWFjaCggWyBcIm1pbmxlbmd0aFwiLCBcIm1heGxlbmd0aFwiIF0sIGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoIHJ1bGVzWyB0aGlzIF0gKSB7XG4gICAgICAgICAgcnVsZXNbIHRoaXMgXSA9IE51bWJlciggcnVsZXNbIHRoaXMgXSApO1xuICAgICAgICB9XG4gICAgICB9ICk7XG4gICAgICAkLmVhY2goIFsgXCJyYW5nZWxlbmd0aFwiLCBcInJhbmdlXCIgXSwgZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBwYXJ0cztcbiAgICAgICAgaWYgKCBydWxlc1sgdGhpcyBdICkge1xuICAgICAgICAgIGlmICggJC5pc0FycmF5KCBydWxlc1sgdGhpcyBdICkgKSB7XG4gICAgICAgICAgICBydWxlc1sgdGhpcyBdID0gWyBOdW1iZXIoIHJ1bGVzWyB0aGlzIF1bIDAgXSApLCBOdW1iZXIoIHJ1bGVzWyB0aGlzIF1bIDEgXSApIF07XG4gICAgICAgICAgfSBlbHNlIGlmICggdHlwZW9mIHJ1bGVzWyB0aGlzIF0gPT09IFwic3RyaW5nXCIgKSB7XG4gICAgICAgICAgICBwYXJ0cyA9IHJ1bGVzWyB0aGlzIF0ucmVwbGFjZSggL1tcXFtcXF1dL2csIFwiXCIgKS5zcGxpdCggL1tcXHMsXSsvICk7XG4gICAgICAgICAgICBydWxlc1sgdGhpcyBdID0gWyBOdW1iZXIoIHBhcnRzWyAwIF0gKSwgTnVtYmVyKCBwYXJ0c1sgMSBdICkgXTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gKTtcblxuICAgICAgaWYgKCAkLnZhbGlkYXRvci5hdXRvQ3JlYXRlUmFuZ2VzICkge1xuXG4gICAgICAgIC8vIEF1dG8tY3JlYXRlIHJhbmdlc1xuICAgICAgICBpZiAoIHJ1bGVzLm1pbiAhPSBudWxsICYmIHJ1bGVzLm1heCAhPSBudWxsICkge1xuICAgICAgICAgIHJ1bGVzLnJhbmdlID0gWyBydWxlcy5taW4sIHJ1bGVzLm1heCBdO1xuICAgICAgICAgIGRlbGV0ZSBydWxlcy5taW47XG4gICAgICAgICAgZGVsZXRlIHJ1bGVzLm1heDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIHJ1bGVzLm1pbmxlbmd0aCAhPSBudWxsICYmIHJ1bGVzLm1heGxlbmd0aCAhPSBudWxsICkge1xuICAgICAgICAgIHJ1bGVzLnJhbmdlbGVuZ3RoID0gWyBydWxlcy5taW5sZW5ndGgsIHJ1bGVzLm1heGxlbmd0aCBdO1xuICAgICAgICAgIGRlbGV0ZSBydWxlcy5taW5sZW5ndGg7XG4gICAgICAgICAgZGVsZXRlIHJ1bGVzLm1heGxlbmd0aDtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gcnVsZXM7XG4gICAgfSxcblxuICAgIC8vIENvbnZlcnRzIGEgc2ltcGxlIHN0cmluZyB0byBhIHtzdHJpbmc6IHRydWV9IHJ1bGUsIGUuZy4sIFwicmVxdWlyZWRcIiB0byB7cmVxdWlyZWQ6dHJ1ZX1cbiAgICBub3JtYWxpemVSdWxlOiBmdW5jdGlvbiggZGF0YSApIHtcbiAgICAgIGlmICggdHlwZW9mIGRhdGEgPT09IFwic3RyaW5nXCIgKSB7XG4gICAgICAgIHZhciB0cmFuc2Zvcm1lZCA9IHt9O1xuICAgICAgICAkLmVhY2goIGRhdGEuc3BsaXQoIC9cXHMvICksIGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHRyYW5zZm9ybWVkWyB0aGlzIF0gPSB0cnVlO1xuICAgICAgICB9ICk7XG4gICAgICAgIGRhdGEgPSB0cmFuc2Zvcm1lZDtcbiAgICAgIH1cbiAgICAgIHJldHVybiBkYXRhO1xuICAgIH0sXG5cbiAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL2pRdWVyeS52YWxpZGF0b3IuYWRkTWV0aG9kL1xuICAgIGFkZE1ldGhvZDogZnVuY3Rpb24oIG5hbWUsIG1ldGhvZCwgbWVzc2FnZSApIHtcbiAgICAgICQudmFsaWRhdG9yLm1ldGhvZHNbIG5hbWUgXSA9IG1ldGhvZDtcbiAgICAgICQudmFsaWRhdG9yLm1lc3NhZ2VzWyBuYW1lIF0gPSBtZXNzYWdlICE9PSB1bmRlZmluZWQgPyBtZXNzYWdlIDogJC52YWxpZGF0b3IubWVzc2FnZXNbIG5hbWUgXTtcbiAgICAgIGlmICggbWV0aG9kLmxlbmd0aCA8IDMgKSB7XG4gICAgICAgICQudmFsaWRhdG9yLmFkZENsYXNzUnVsZXMoIG5hbWUsICQudmFsaWRhdG9yLm5vcm1hbGl6ZVJ1bGUoIG5hbWUgKSApO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL2pRdWVyeS52YWxpZGF0b3IubWV0aG9kcy9cbiAgICBtZXRob2RzOiB7XG5cbiAgICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvcmVxdWlyZWQtbWV0aG9kL1xuICAgICAgcmVxdWlyZWQ6IGZ1bmN0aW9uKCB2YWx1ZSwgZWxlbWVudCwgcGFyYW0gKSB7XG5cbiAgICAgICAgLy8gQ2hlY2sgaWYgZGVwZW5kZW5jeSBpcyBtZXRcbiAgICAgICAgaWYgKCAhdGhpcy5kZXBlbmQoIHBhcmFtLCBlbGVtZW50ICkgKSB7XG4gICAgICAgICAgcmV0dXJuIFwiZGVwZW5kZW5jeS1taXNtYXRjaFwiO1xuICAgICAgICB9XG4gICAgICAgIGlmICggZWxlbWVudC5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpID09PSBcInNlbGVjdFwiICkge1xuXG4gICAgICAgICAgLy8gQ291bGQgYmUgYW4gYXJyYXkgZm9yIHNlbGVjdC1tdWx0aXBsZSBvciBhIHN0cmluZywgYm90aCBhcmUgZmluZSB0aGlzIHdheVxuICAgICAgICAgIHZhciB2YWwgPSAkKCBlbGVtZW50ICkudmFsKCk7XG4gICAgICAgICAgcmV0dXJuIHZhbCAmJiB2YWwubGVuZ3RoID4gMDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIHRoaXMuY2hlY2thYmxlKCBlbGVtZW50ICkgKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0TGVuZ3RoKCB2YWx1ZSwgZWxlbWVudCApID4gMDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdmFsdWUgIT09IHVuZGVmaW5lZCAmJiB2YWx1ZSAhPT0gbnVsbCAmJiB2YWx1ZS5sZW5ndGggPiAwO1xuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9lbWFpbC1tZXRob2QvXG4gICAgICBlbWFpbDogZnVuY3Rpb24oIHZhbHVlLCBlbGVtZW50ICkge1xuXG4gICAgICAgIC8vIEZyb20gaHR0cHM6Ly9odG1sLnNwZWMud2hhdHdnLm9yZy9tdWx0aXBhZ2UvZm9ybXMuaHRtbCN2YWxpZC1lLW1haWwtYWRkcmVzc1xuICAgICAgICAvLyBSZXRyaWV2ZWQgMjAxNC0wMS0xNFxuICAgICAgICAvLyBJZiB5b3UgaGF2ZSBhIHByb2JsZW0gd2l0aCB0aGlzIGltcGxlbWVudGF0aW9uLCByZXBvcnQgYSBidWcgYWdhaW5zdCB0aGUgYWJvdmUgc3BlY1xuICAgICAgICAvLyBPciB1c2UgY3VzdG9tIG1ldGhvZHMgdG8gaW1wbGVtZW50IHlvdXIgb3duIGVtYWlsIHZhbGlkYXRpb25cbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9uYWwoIGVsZW1lbnQgKSB8fCAvXlthLXpBLVowLTkuISMkJSYnKitcXC89P15fYHt8fX4tXStAW2EtekEtWjAtOV0oPzpbYS16QS1aMC05LV17MCw2MX1bYS16QS1aMC05XSk/KD86XFwuW2EtekEtWjAtOV0oPzpbYS16QS1aMC05LV17MCw2MX1bYS16QS1aMC05XSk/KSokLy50ZXN0KCB2YWx1ZSApO1xuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy91cmwtbWV0aG9kL1xuICAgICAgdXJsOiBmdW5jdGlvbiggdmFsdWUsIGVsZW1lbnQgKSB7XG5cbiAgICAgICAgLy8gQ29weXJpZ2h0IChjKSAyMDEwLTIwMTMgRGllZ28gUGVyaW5pLCBNSVQgbGljZW5zZWRcbiAgICAgICAgLy8gaHR0cHM6Ly9naXN0LmdpdGh1Yi5jb20vZHBlcmluaS83MjkyOTRcbiAgICAgICAgLy8gc2VlIGFsc28gaHR0cHM6Ly9tYXRoaWFzYnluZW5zLmJlL2RlbW8vdXJsLXJlZ2V4XG4gICAgICAgIC8vIG1vZGlmaWVkIHRvIGFsbG93IHByb3RvY29sLXJlbGF0aXZlIFVSTHNcbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9uYWwoIGVsZW1lbnQgKSB8fCAvXig/Oig/Oig/Omh0dHBzP3xmdHApOik/XFwvXFwvKSg/OlxcUysoPzo6XFxTKik/QCk/KD86KD8hKD86MTB8MTI3KSg/OlxcLlxcZHsxLDN9KXszfSkoPyEoPzoxNjlcXC4yNTR8MTkyXFwuMTY4KSg/OlxcLlxcZHsxLDN9KXsyfSkoPyExNzJcXC4oPzoxWzYtOV18MlxcZHwzWzAtMV0pKD86XFwuXFxkezEsM30pezJ9KSg/OlsxLTldXFxkP3wxXFxkXFxkfDJbMDFdXFxkfDIyWzAtM10pKD86XFwuKD86MT9cXGR7MSwyfXwyWzAtNF1cXGR8MjVbMC01XSkpezJ9KD86XFwuKD86WzEtOV1cXGQ/fDFcXGRcXGR8MlswLTRdXFxkfDI1WzAtNF0pKXwoPzooPzpbYS16XFx1MDBhMS1cXHVmZmZmMC05XS0qKSpbYS16XFx1MDBhMS1cXHVmZmZmMC05XSspKD86XFwuKD86W2EtelxcdTAwYTEtXFx1ZmZmZjAtOV0tKikqW2EtelxcdTAwYTEtXFx1ZmZmZjAtOV0rKSooPzpcXC4oPzpbYS16XFx1MDBhMS1cXHVmZmZmXXsyLH0pKS4/KSg/OjpcXGR7Miw1fSk/KD86Wy8/I11cXFMqKT8kL2kudGVzdCggdmFsdWUgKTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvZGF0ZS1tZXRob2QvXG4gICAgICBkYXRlOiAoIGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgY2FsbGVkID0gZmFsc2U7XG5cbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCB2YWx1ZSwgZWxlbWVudCApIHtcbiAgICAgICAgICBpZiAoICFjYWxsZWQgKSB7XG4gICAgICAgICAgICBjYWxsZWQgPSB0cnVlO1xuICAgICAgICAgICAgaWYgKCB0aGlzLnNldHRpbmdzLmRlYnVnICYmIHdpbmRvdy5jb25zb2xlICkge1xuICAgICAgICAgICAgICBjb25zb2xlLndhcm4oXG4gICAgICAgICAgICAgICAgXCJUaGUgYGRhdGVgIG1ldGhvZCBpcyBkZXByZWNhdGVkIGFuZCB3aWxsIGJlIHJlbW92ZWQgaW4gdmVyc2lvbiAnMi4wLjAnLlxcblwiICtcbiAgICAgICAgICAgICAgICBcIlBsZWFzZSBkb24ndCB1c2UgaXQsIHNpbmNlIGl0IHJlbGllcyBvbiB0aGUgRGF0ZSBjb25zdHJ1Y3Rvciwgd2hpY2hcXG5cIiArXG4gICAgICAgICAgICAgICAgXCJiZWhhdmVzIHZlcnkgZGlmZmVyZW50bHkgYWNyb3NzIGJyb3dzZXJzIGFuZCBsb2NhbGVzLiBVc2UgYGRhdGVJU09gXFxuXCIgK1xuICAgICAgICAgICAgICAgIFwiaW5zdGVhZCBvciBvbmUgb2YgdGhlIGxvY2FsZSBzcGVjaWZpYyBtZXRob2RzIGluIGBsb2NhbGl6YXRpb25zL2BcXG5cIiArXG4gICAgICAgICAgICAgICAgXCJhbmQgYGFkZGl0aW9uYWwtbWV0aG9kcy5qc2AuXCJcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25hbCggZWxlbWVudCApIHx8ICEvSW52YWxpZHxOYU4vLnRlc3QoIG5ldyBEYXRlKCB2YWx1ZSApLnRvU3RyaW5nKCkgKTtcbiAgICAgICAgfTtcbiAgICAgIH0oKSApLFxuXG4gICAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL2RhdGVJU08tbWV0aG9kL1xuICAgICAgZGF0ZUlTTzogZnVuY3Rpb24oIHZhbHVlLCBlbGVtZW50ICkge1xuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25hbCggZWxlbWVudCApIHx8IC9eXFxkezR9W1xcL1xcLV0oMD9bMS05XXwxWzAxMl0pW1xcL1xcLV0oMD9bMS05XXxbMTJdWzAtOV18M1swMV0pJC8udGVzdCggdmFsdWUgKTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvbnVtYmVyLW1ldGhvZC9cbiAgICAgIG51bWJlcjogZnVuY3Rpb24oIHZhbHVlLCBlbGVtZW50ICkge1xuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25hbCggZWxlbWVudCApIHx8IC9eKD86LT9cXGQrfC0/XFxkezEsM30oPzosXFxkezN9KSspPyg/OlxcLlxcZCspPyQvLnRlc3QoIHZhbHVlICk7XG4gICAgICB9LFxuXG4gICAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL2RpZ2l0cy1tZXRob2QvXG4gICAgICBkaWdpdHM6IGZ1bmN0aW9uKCB2YWx1ZSwgZWxlbWVudCApIHtcbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9uYWwoIGVsZW1lbnQgKSB8fCAvXlxcZCskLy50ZXN0KCB2YWx1ZSApO1xuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9taW5sZW5ndGgtbWV0aG9kL1xuICAgICAgbWlubGVuZ3RoOiBmdW5jdGlvbiggdmFsdWUsIGVsZW1lbnQsIHBhcmFtICkge1xuICAgICAgICB2YXIgbGVuZ3RoID0gJC5pc0FycmF5KCB2YWx1ZSApID8gdmFsdWUubGVuZ3RoIDogdGhpcy5nZXRMZW5ndGgoIHZhbHVlLCBlbGVtZW50ICk7XG4gICAgICAgIHJldHVybiB0aGlzLm9wdGlvbmFsKCBlbGVtZW50ICkgfHwgbGVuZ3RoID49IHBhcmFtO1xuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9tYXhsZW5ndGgtbWV0aG9kL1xuICAgICAgbWF4bGVuZ3RoOiBmdW5jdGlvbiggdmFsdWUsIGVsZW1lbnQsIHBhcmFtICkge1xuICAgICAgICB2YXIgbGVuZ3RoID0gJC5pc0FycmF5KCB2YWx1ZSApID8gdmFsdWUubGVuZ3RoIDogdGhpcy5nZXRMZW5ndGgoIHZhbHVlLCBlbGVtZW50ICk7XG4gICAgICAgIHJldHVybiB0aGlzLm9wdGlvbmFsKCBlbGVtZW50ICkgfHwgbGVuZ3RoIDw9IHBhcmFtO1xuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9yYW5nZWxlbmd0aC1tZXRob2QvXG4gICAgICByYW5nZWxlbmd0aDogZnVuY3Rpb24oIHZhbHVlLCBlbGVtZW50LCBwYXJhbSApIHtcbiAgICAgICAgdmFyIGxlbmd0aCA9ICQuaXNBcnJheSggdmFsdWUgKSA/IHZhbHVlLmxlbmd0aCA6IHRoaXMuZ2V0TGVuZ3RoKCB2YWx1ZSwgZWxlbWVudCApO1xuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25hbCggZWxlbWVudCApIHx8ICggbGVuZ3RoID49IHBhcmFtWyAwIF0gJiYgbGVuZ3RoIDw9IHBhcmFtWyAxIF0gKTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvbWluLW1ldGhvZC9cbiAgICAgIG1pbjogZnVuY3Rpb24oIHZhbHVlLCBlbGVtZW50LCBwYXJhbSApIHtcbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9uYWwoIGVsZW1lbnQgKSB8fCB2YWx1ZSA+PSBwYXJhbTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvbWF4LW1ldGhvZC9cbiAgICAgIG1heDogZnVuY3Rpb24oIHZhbHVlLCBlbGVtZW50LCBwYXJhbSApIHtcbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9uYWwoIGVsZW1lbnQgKSB8fCB2YWx1ZSA8PSBwYXJhbTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvcmFuZ2UtbWV0aG9kL1xuICAgICAgcmFuZ2U6IGZ1bmN0aW9uKCB2YWx1ZSwgZWxlbWVudCwgcGFyYW0gKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm9wdGlvbmFsKCBlbGVtZW50ICkgfHwgKCB2YWx1ZSA+PSBwYXJhbVsgMCBdICYmIHZhbHVlIDw9IHBhcmFtWyAxIF0gKTtcbiAgICAgIH0sXG5cbiAgICAgIC8vIGh0dHBzOi8vanF1ZXJ5dmFsaWRhdGlvbi5vcmcvc3RlcC1tZXRob2QvXG4gICAgICBzdGVwOiBmdW5jdGlvbiggdmFsdWUsIGVsZW1lbnQsIHBhcmFtICkge1xuICAgICAgICB2YXIgdHlwZSA9ICQoIGVsZW1lbnQgKS5hdHRyKCBcInR5cGVcIiApLFxuICAgICAgICAgIGVycm9yTWVzc2FnZSA9IFwiU3RlcCBhdHRyaWJ1dGUgb24gaW5wdXQgdHlwZSBcIiArIHR5cGUgKyBcIiBpcyBub3Qgc3VwcG9ydGVkLlwiLFxuICAgICAgICAgIHN1cHBvcnRlZFR5cGVzID0gWyBcInRleHRcIiwgXCJudW1iZXJcIiwgXCJyYW5nZVwiIF0sXG4gICAgICAgICAgcmUgPSBuZXcgUmVnRXhwKCBcIlxcXFxiXCIgKyB0eXBlICsgXCJcXFxcYlwiICksXG4gICAgICAgICAgbm90U3VwcG9ydGVkID0gdHlwZSAmJiAhcmUudGVzdCggc3VwcG9ydGVkVHlwZXMuam9pbigpICksXG4gICAgICAgICAgZGVjaW1hbFBsYWNlcyA9IGZ1bmN0aW9uKCBudW0gKSB7XG4gICAgICAgICAgICB2YXIgbWF0Y2ggPSAoIFwiXCIgKyBudW0gKS5tYXRjaCggLyg/OlxcLihcXGQrKSk/JC8gKTtcbiAgICAgICAgICAgIGlmICggIW1hdGNoICkge1xuICAgICAgICAgICAgICByZXR1cm4gMDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gTnVtYmVyIG9mIGRpZ2l0cyByaWdodCBvZiBkZWNpbWFsIHBvaW50LlxuICAgICAgICAgICAgcmV0dXJuIG1hdGNoWyAxIF0gPyBtYXRjaFsgMSBdLmxlbmd0aCA6IDA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB0b0ludCA9IGZ1bmN0aW9uKCBudW0gKSB7XG4gICAgICAgICAgICByZXR1cm4gTWF0aC5yb3VuZCggbnVtICogTWF0aC5wb3coIDEwLCBkZWNpbWFscyApICk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB2YWxpZCA9IHRydWUsXG4gICAgICAgICAgZGVjaW1hbHM7XG5cbiAgICAgICAgLy8gV29ya3Mgb25seSBmb3IgdGV4dCwgbnVtYmVyIGFuZCByYW5nZSBpbnB1dCB0eXBlc1xuICAgICAgICAvLyBUT0RPIGZpbmQgYSB3YXkgdG8gc3VwcG9ydCBpbnB1dCB0eXBlcyBkYXRlLCBkYXRldGltZSwgZGF0ZXRpbWUtbG9jYWwsIG1vbnRoLCB0aW1lIGFuZCB3ZWVrXG4gICAgICAgIGlmICggbm90U3VwcG9ydGVkICkge1xuICAgICAgICAgIHRocm93IG5ldyBFcnJvciggZXJyb3JNZXNzYWdlICk7XG4gICAgICAgIH1cblxuICAgICAgICBkZWNpbWFscyA9IGRlY2ltYWxQbGFjZXMoIHBhcmFtICk7XG5cbiAgICAgICAgLy8gVmFsdWUgY2FuJ3QgaGF2ZSB0b28gbWFueSBkZWNpbWFsc1xuICAgICAgICBpZiAoIGRlY2ltYWxQbGFjZXMoIHZhbHVlICkgPiBkZWNpbWFscyB8fCB0b0ludCggdmFsdWUgKSAlIHRvSW50KCBwYXJhbSApICE9PSAwICkge1xuICAgICAgICAgIHZhbGlkID0gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25hbCggZWxlbWVudCApIHx8IHZhbGlkO1xuICAgICAgfSxcblxuICAgICAgLy8gaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9lcXVhbFRvLW1ldGhvZC9cbiAgICAgIGVxdWFsVG86IGZ1bmN0aW9uKCB2YWx1ZSwgZWxlbWVudCwgcGFyYW0gKSB7XG5cbiAgICAgICAgLy8gQmluZCB0byB0aGUgYmx1ciBldmVudCBvZiB0aGUgdGFyZ2V0IGluIG9yZGVyIHRvIHJldmFsaWRhdGUgd2hlbmV2ZXIgdGhlIHRhcmdldCBmaWVsZCBpcyB1cGRhdGVkXG4gICAgICAgIHZhciB0YXJnZXQgPSAkKCBwYXJhbSApO1xuICAgICAgICBpZiAoIHRoaXMuc2V0dGluZ3Mub25mb2N1c291dCAmJiB0YXJnZXQubm90KCBcIi52YWxpZGF0ZS1lcXVhbFRvLWJsdXJcIiApLmxlbmd0aCApIHtcbiAgICAgICAgICB0YXJnZXQuYWRkQ2xhc3MoIFwidmFsaWRhdGUtZXF1YWxUby1ibHVyXCIgKS5vbiggXCJibHVyLnZhbGlkYXRlLWVxdWFsVG9cIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkKCBlbGVtZW50ICkudmFsaWQoKTtcbiAgICAgICAgICB9ICk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHZhbHVlID09PSB0YXJnZXQudmFsKCk7XG4gICAgICB9LFxuXG4gICAgICAvLyBodHRwczovL2pxdWVyeXZhbGlkYXRpb24ub3JnL3JlbW90ZS1tZXRob2QvXG4gICAgICByZW1vdGU6IGZ1bmN0aW9uKCB2YWx1ZSwgZWxlbWVudCwgcGFyYW0sIG1ldGhvZCApIHtcbiAgICAgICAgaWYgKCB0aGlzLm9wdGlvbmFsKCBlbGVtZW50ICkgKSB7XG4gICAgICAgICAgcmV0dXJuIFwiZGVwZW5kZW5jeS1taXNtYXRjaFwiO1xuICAgICAgICB9XG5cbiAgICAgICAgbWV0aG9kID0gdHlwZW9mIG1ldGhvZCA9PT0gXCJzdHJpbmdcIiAmJiBtZXRob2QgfHwgXCJyZW1vdGVcIjtcblxuICAgICAgICB2YXIgcHJldmlvdXMgPSB0aGlzLnByZXZpb3VzVmFsdWUoIGVsZW1lbnQsIG1ldGhvZCApLFxuICAgICAgICAgIHZhbGlkYXRvciwgZGF0YSwgb3B0aW9uRGF0YVN0cmluZztcblxuICAgICAgICBpZiAoICF0aGlzLnNldHRpbmdzLm1lc3NhZ2VzWyBlbGVtZW50Lm5hbWUgXSApIHtcbiAgICAgICAgICB0aGlzLnNldHRpbmdzLm1lc3NhZ2VzWyBlbGVtZW50Lm5hbWUgXSA9IHt9O1xuICAgICAgICB9XG4gICAgICAgIHByZXZpb3VzLm9yaWdpbmFsTWVzc2FnZSA9IHByZXZpb3VzLm9yaWdpbmFsTWVzc2FnZSB8fCB0aGlzLnNldHRpbmdzLm1lc3NhZ2VzWyBlbGVtZW50Lm5hbWUgXVsgbWV0aG9kIF07XG4gICAgICAgIHRoaXMuc2V0dGluZ3MubWVzc2FnZXNbIGVsZW1lbnQubmFtZSBdWyBtZXRob2QgXSA9IHByZXZpb3VzLm1lc3NhZ2U7XG5cbiAgICAgICAgcGFyYW0gPSB0eXBlb2YgcGFyYW0gPT09IFwic3RyaW5nXCIgJiYgeyB1cmw6IHBhcmFtIH0gfHwgcGFyYW07XG4gICAgICAgIG9wdGlvbkRhdGFTdHJpbmcgPSAkLnBhcmFtKCAkLmV4dGVuZCggeyBkYXRhOiB2YWx1ZSB9LCBwYXJhbS5kYXRhICkgKTtcbiAgICAgICAgaWYgKCBwcmV2aW91cy5vbGQgPT09IG9wdGlvbkRhdGFTdHJpbmcgKSB7XG4gICAgICAgICAgcmV0dXJuIHByZXZpb3VzLnZhbGlkO1xuICAgICAgICB9XG5cbiAgICAgICAgcHJldmlvdXMub2xkID0gb3B0aW9uRGF0YVN0cmluZztcbiAgICAgICAgdmFsaWRhdG9yID0gdGhpcztcbiAgICAgICAgdGhpcy5zdGFydFJlcXVlc3QoIGVsZW1lbnQgKTtcbiAgICAgICAgZGF0YSA9IHt9O1xuICAgICAgICBkYXRhWyBlbGVtZW50Lm5hbWUgXSA9IHZhbHVlO1xuICAgICAgICAkLmFqYXgoICQuZXh0ZW5kKCB0cnVlLCB7XG4gICAgICAgICAgbW9kZTogXCJhYm9ydFwiLFxuICAgICAgICAgIHBvcnQ6IFwidmFsaWRhdGVcIiArIGVsZW1lbnQubmFtZSxcbiAgICAgICAgICBkYXRhVHlwZTogXCJqc29uXCIsXG4gICAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgICBjb250ZXh0OiB2YWxpZGF0b3IuY3VycmVudEZvcm0sXG4gICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oIHJlc3BvbnNlICkge1xuICAgICAgICAgICAgdmFyIHZhbGlkID0gcmVzcG9uc2UgPT09IHRydWUgfHwgcmVzcG9uc2UgPT09IFwidHJ1ZVwiLFxuICAgICAgICAgICAgICBlcnJvcnMsIG1lc3NhZ2UsIHN1Ym1pdHRlZDtcblxuICAgICAgICAgICAgdmFsaWRhdG9yLnNldHRpbmdzLm1lc3NhZ2VzWyBlbGVtZW50Lm5hbWUgXVsgbWV0aG9kIF0gPSBwcmV2aW91cy5vcmlnaW5hbE1lc3NhZ2U7XG4gICAgICAgICAgICBpZiAoIHZhbGlkICkge1xuICAgICAgICAgICAgICBzdWJtaXR0ZWQgPSB2YWxpZGF0b3IuZm9ybVN1Ym1pdHRlZDtcbiAgICAgICAgICAgICAgdmFsaWRhdG9yLnJlc2V0SW50ZXJuYWxzKCk7XG4gICAgICAgICAgICAgIHZhbGlkYXRvci50b0hpZGUgPSB2YWxpZGF0b3IuZXJyb3JzRm9yKCBlbGVtZW50ICk7XG4gICAgICAgICAgICAgIHZhbGlkYXRvci5mb3JtU3VibWl0dGVkID0gc3VibWl0dGVkO1xuICAgICAgICAgICAgICB2YWxpZGF0b3Iuc3VjY2Vzc0xpc3QucHVzaCggZWxlbWVudCApO1xuICAgICAgICAgICAgICB2YWxpZGF0b3IuaW52YWxpZFsgZWxlbWVudC5uYW1lIF0gPSBmYWxzZTtcbiAgICAgICAgICAgICAgdmFsaWRhdG9yLnNob3dFcnJvcnMoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIGVycm9ycyA9IHt9O1xuICAgICAgICAgICAgICBtZXNzYWdlID0gcmVzcG9uc2UgfHwgdmFsaWRhdG9yLmRlZmF1bHRNZXNzYWdlKCBlbGVtZW50LCB7IG1ldGhvZDogbWV0aG9kLCBwYXJhbWV0ZXJzOiB2YWx1ZSB9ICk7XG4gICAgICAgICAgICAgIGVycm9yc1sgZWxlbWVudC5uYW1lIF0gPSBwcmV2aW91cy5tZXNzYWdlID0gbWVzc2FnZTtcbiAgICAgICAgICAgICAgdmFsaWRhdG9yLmludmFsaWRbIGVsZW1lbnQubmFtZSBdID0gdHJ1ZTtcbiAgICAgICAgICAgICAgdmFsaWRhdG9yLnNob3dFcnJvcnMoIGVycm9ycyApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcHJldmlvdXMudmFsaWQgPSB2YWxpZDtcbiAgICAgICAgICAgIHZhbGlkYXRvci5zdG9wUmVxdWVzdCggZWxlbWVudCwgdmFsaWQgKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sIHBhcmFtICkgKTtcbiAgICAgICAgcmV0dXJuIFwicGVuZGluZ1wiO1xuICAgICAgfVxuICAgIH1cblxuICB9ICk7XG5cbi8vIEFqYXggbW9kZTogYWJvcnRcbi8vIHVzYWdlOiAkLmFqYXgoeyBtb2RlOiBcImFib3J0XCJbLCBwb3J0OiBcInVuaXF1ZXBvcnRcIl19KTtcbi8vIGlmIG1vZGU6XCJhYm9ydFwiIGlzIHVzZWQsIHRoZSBwcmV2aW91cyByZXF1ZXN0IG9uIHRoYXQgcG9ydCAocG9ydCBjYW4gYmUgdW5kZWZpbmVkKSBpcyBhYm9ydGVkIHZpYSBYTUxIdHRwUmVxdWVzdC5hYm9ydCgpXG5cbiAgdmFyIHBlbmRpbmdSZXF1ZXN0cyA9IHt9LFxuICAgIGFqYXg7XG5cbi8vIFVzZSBhIHByZWZpbHRlciBpZiBhdmFpbGFibGUgKDEuNSspXG4gIGlmICggJC5hamF4UHJlZmlsdGVyICkge1xuICAgICQuYWpheFByZWZpbHRlciggZnVuY3Rpb24oIHNldHRpbmdzLCBfLCB4aHIgKSB7XG4gICAgICB2YXIgcG9ydCA9IHNldHRpbmdzLnBvcnQ7XG4gICAgICBpZiAoIHNldHRpbmdzLm1vZGUgPT09IFwiYWJvcnRcIiApIHtcbiAgICAgICAgaWYgKCBwZW5kaW5nUmVxdWVzdHNbIHBvcnQgXSApIHtcbiAgICAgICAgICBwZW5kaW5nUmVxdWVzdHNbIHBvcnQgXS5hYm9ydCgpO1xuICAgICAgICB9XG4gICAgICAgIHBlbmRpbmdSZXF1ZXN0c1sgcG9ydCBdID0geGhyO1xuICAgICAgfVxuICAgIH0gKTtcbiAgfSBlbHNlIHtcblxuICAgIC8vIFByb3h5IGFqYXhcbiAgICBhamF4ID0gJC5hamF4O1xuICAgICQuYWpheCA9IGZ1bmN0aW9uKCBzZXR0aW5ncyApIHtcbiAgICAgIHZhciBtb2RlID0gKCBcIm1vZGVcIiBpbiBzZXR0aW5ncyA/IHNldHRpbmdzIDogJC5hamF4U2V0dGluZ3MgKS5tb2RlLFxuICAgICAgICBwb3J0ID0gKCBcInBvcnRcIiBpbiBzZXR0aW5ncyA/IHNldHRpbmdzIDogJC5hamF4U2V0dGluZ3MgKS5wb3J0O1xuICAgICAgaWYgKCBtb2RlID09PSBcImFib3J0XCIgKSB7XG4gICAgICAgIGlmICggcGVuZGluZ1JlcXVlc3RzWyBwb3J0IF0gKSB7XG4gICAgICAgICAgcGVuZGluZ1JlcXVlc3RzWyBwb3J0IF0uYWJvcnQoKTtcbiAgICAgICAgfVxuICAgICAgICBwZW5kaW5nUmVxdWVzdHNbIHBvcnQgXSA9IGFqYXguYXBwbHkoIHRoaXMsIGFyZ3VtZW50cyApO1xuICAgICAgICByZXR1cm4gcGVuZGluZ1JlcXVlc3RzWyBwb3J0IF07XG4gICAgICB9XG4gICAgICByZXR1cm4gYWpheC5hcHBseSggdGhpcywgYXJndW1lbnRzICk7XG4gICAgfTtcbiAgfVxuICByZXR1cm4gJDtcbn0pKTtcbiIsIi8qIGVzbGludC1kaXNhYmxlIG5vLXVuZGVmaW5lZCxuby1wYXJhbS1yZWFzc2lnbixuby1zaGFkb3cgKi9cblxuLyoqXG4gKiBUaHJvdHRsZSBleGVjdXRpb24gb2YgYSBmdW5jdGlvbi4gRXNwZWNpYWxseSB1c2VmdWwgZm9yIHJhdGUgbGltaXRpbmdcbiAqIGV4ZWN1dGlvbiBvZiBoYW5kbGVycyBvbiBldmVudHMgbGlrZSByZXNpemUgYW5kIHNjcm9sbC5cbiAqXG4gKiBAcGFyYW0gIHtOdW1iZXJ9ICAgIGRlbGF5XG4gKiBBIHplcm8tb3ItZ3JlYXRlciBkZWxheSBpbiBtaWxsaXNlY29uZHMuXG4gKiBGb3IgZXZlbnQgY2FsbGJhY2tzLCB2YWx1ZXMgYXJvdW5kIDEwMCBvciAyNTAgKG9yIGV2ZW4gaGlnaGVyKSBhcmUgbW9zdCB1c2VmdWwuXG4gKiBAcGFyYW0gIHtCb29sZWFufSAgIFtub1RyYWlsaW5nXSAgIE9wdGlvbmFsLCBkZWZhdWx0cyB0byBmYWxzZS5cbiAqIElmIG5vVHJhaWxpbmcgaXMgdHJ1ZSwgY2FsbGJhY2sgd2lsbCBvbmx5IGV4ZWN1dGUgZXZlcnkgYGRlbGF5YCBtaWxsaXNlY29uZHMgd2hpbGUgdGhlXG4gKiB0aHJvdHRsZWQtZnVuY3Rpb24gaXMgYmVpbmcgY2FsbGVkLiBJZiBub1RyYWlsaW5nIGlzIGZhbHNlIG9yIHVuc3BlY2lmaWVkLCBjYWxsYmFjayB3aWxsIGJlIGV4ZWN1dGVkIG9uZSBmaW5hbCB0aW1lXG4gKiBhZnRlciB0aGUgbGFzdCB0aHJvdHRsZWQtZnVuY3Rpb24gY2FsbC4gKEFmdGVyIHRoZSB0aHJvdHRsZWQtZnVuY3Rpb24gaGFzIG5vdCBiZWVuIGNhbGxlZCBmb3IgYGRlbGF5YCBtaWxsaXNlY29uZHMsXG4gKiB0aGUgaW50ZXJuYWwgY291bnRlciBpcyByZXNldClcbiAqIEBwYXJhbSAge0Z1bmN0aW9ufSAgY2FsbGJhY2tcbiAqIEEgZnVuY3Rpb24gdG8gYmUgZXhlY3V0ZWQgYWZ0ZXIgZGVsYXkgbWlsbGlzZWNvbmRzLiBUaGUgYHRoaXNgIGNvbnRleHQgYW5kIGFsbCBhcmd1bWVudHMgYXJlIHBhc3NlZCB0aHJvdWdoLCBhcy1pcyxcbiAqIHRvIGBjYWxsYmFja2Agd2hlbiB0aGUgdGhyb3R0bGVkLWZ1bmN0aW9uIGlzIGV4ZWN1dGVkLlxuICogQHBhcmFtICB7Qm9vbGVhbn0gICBbZGVib3VuY2VNb2RlXVxuICogSWYgYGRlYm91bmNlTW9kZWAgaXMgdHJ1ZSAoYXQgYmVnaW4pLCBzY2hlZHVsZSBgY2xlYXJgIHRvIGV4ZWN1dGUgYWZ0ZXIgYGRlbGF5YCBtcy5cbiAqIElmIGBkZWJvdW5jZU1vZGVgIGlzIGZhbHNlIChhdCBlbmQpLCBzY2hlZHVsZSBgY2FsbGJhY2tgIHRvIGV4ZWN1dGUgYWZ0ZXIgYGRlbGF5YCBtcy5cbiAqXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn0gIEEgbmV3LCB0aHJvdHRsZWQsIGZ1bmN0aW9uLlxuICovXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAoZGVsYXksIG5vVHJhaWxpbmcsIGNhbGxiYWNrLCBkZWJvdW5jZU1vZGUpIHtcbiAgLypcbiAgICogQWZ0ZXIgd3JhcHBlciBoYXMgc3RvcHBlZCBiZWluZyBjYWxsZWQsIHRoaXMgdGltZW91dCBlbnN1cmVzIHRoYXRcbiAgICogYGNhbGxiYWNrYCBpcyBleGVjdXRlZCBhdCB0aGUgcHJvcGVyIHRpbWVzIGluIGB0aHJvdHRsZWAgYW5kIGBlbmRgXG4gICAqIGRlYm91bmNlIG1vZGVzLlxuICAgKi9cbiAgbGV0IHRpbWVvdXRJRDtcbiAgbGV0IGNhbmNlbGxlZCA9IGZhbHNlO1xuXG4gIC8vIEtlZXAgdHJhY2sgb2YgdGhlIGxhc3QgdGltZSBgY2FsbGJhY2tgIHdhcyBleGVjdXRlZC5cbiAgbGV0IGxhc3RFeGVjID0gMDtcblxuICAvLyBGdW5jdGlvbiB0byBjbGVhciBleGlzdGluZyB0aW1lb3V0XG4gIGZ1bmN0aW9uIGNsZWFyRXhpc3RpbmdUaW1lb3V0KCkge1xuICAgIGlmICh0aW1lb3V0SUQpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aW1lb3V0SUQpO1xuICAgIH1cbiAgfVxuXG4gIC8vIEZ1bmN0aW9uIHRvIGNhbmNlbCBuZXh0IGV4ZWNcbiAgZnVuY3Rpb24gY2FuY2VsKCkge1xuICAgIGNsZWFyRXhpc3RpbmdUaW1lb3V0KCk7XG4gICAgY2FuY2VsbGVkID0gdHJ1ZTtcbiAgfVxuXG4gIC8vIGBub1RyYWlsaW5nYCBkZWZhdWx0cyB0byBmYWxzeS5cbiAgaWYgKHR5cGVvZiBub1RyYWlsaW5nICE9PSAnYm9vbGVhbicpIHtcbiAgICBkZWJvdW5jZU1vZGUgPSBjYWxsYmFjaztcbiAgICBjYWxsYmFjayA9IG5vVHJhaWxpbmc7XG4gICAgbm9UcmFpbGluZyA9IHVuZGVmaW5lZDtcbiAgfVxuXG4gIC8qXG4gICAqIFRoZSBgd3JhcHBlcmAgZnVuY3Rpb24gZW5jYXBzdWxhdGVzIGFsbCBvZiB0aGUgdGhyb3R0bGluZyAvIGRlYm91bmNpbmdcbiAgICogZnVuY3Rpb25hbGl0eSBhbmQgd2hlbiBleGVjdXRlZCB3aWxsIGxpbWl0IHRoZSByYXRlIGF0IHdoaWNoIGBjYWxsYmFja2BcbiAgICogaXMgZXhlY3V0ZWQuXG4gICAqL1xuICBmdW5jdGlvbiB3cmFwcGVyKC4uLmFyZ3MpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICBjb25zdCBlbGFwc2VkID0gRGF0ZS5ub3coKSAtIGxhc3RFeGVjO1xuXG4gICAgaWYgKGNhbmNlbGxlZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIEV4ZWN1dGUgYGNhbGxiYWNrYCBhbmQgdXBkYXRlIHRoZSBgbGFzdEV4ZWNgIHRpbWVzdGFtcC5cbiAgICBmdW5jdGlvbiBleGVjKCkge1xuICAgICAgbGFzdEV4ZWMgPSBEYXRlLm5vdygpO1xuICAgICAgY2FsbGJhY2suYXBwbHkoc2VsZiwgYXJncyk7XG4gICAgfVxuXG4gICAgLypcbiAgICAgKiBJZiBgZGVib3VuY2VNb2RlYCBpcyB0cnVlIChhdCBiZWdpbikgdGhpcyBpcyB1c2VkIHRvIGNsZWFyIHRoZSBmbGFnXG4gICAgICogdG8gYWxsb3cgZnV0dXJlIGBjYWxsYmFja2AgZXhlY3V0aW9ucy5cbiAgICAgKi9cbiAgICBmdW5jdGlvbiBjbGVhcigpIHtcbiAgICAgIHRpbWVvdXRJRCA9IHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBpZiAoZGVib3VuY2VNb2RlICYmICF0aW1lb3V0SUQpIHtcbiAgICAgIC8qXG4gICAgICAgKiBTaW5jZSBgd3JhcHBlcmAgaXMgYmVpbmcgY2FsbGVkIGZvciB0aGUgZmlyc3QgdGltZSBhbmRcbiAgICAgICAqIGBkZWJvdW5jZU1vZGVgIGlzIHRydWUgKGF0IGJlZ2luKSwgZXhlY3V0ZSBgY2FsbGJhY2tgLlxuICAgICAgICovXG4gICAgICBleGVjKCk7XG4gICAgfVxuXG4gICAgY2xlYXJFeGlzdGluZ1RpbWVvdXQoKTtcblxuICAgIGlmIChkZWJvdW5jZU1vZGUgPT09IHVuZGVmaW5lZCAmJiBlbGFwc2VkID4gZGVsYXkpIHtcbiAgICAgIC8qXG4gICAgICAgKiBJbiB0aHJvdHRsZSBtb2RlLCBpZiBgZGVsYXlgIHRpbWUgaGFzIGJlZW4gZXhjZWVkZWQsIGV4ZWN1dGVcbiAgICAgICAqIGBjYWxsYmFja2AuXG4gICAgICAgKi9cbiAgICAgIGV4ZWMoKTtcbiAgICB9IGVsc2UgaWYgKG5vVHJhaWxpbmcgIT09IHRydWUpIHtcbiAgICAgIC8qXG4gICAgICAgKiBJbiB0cmFpbGluZyB0aHJvdHRsZSBtb2RlLCBzaW5jZSBgZGVsYXlgIHRpbWUgaGFzIG5vdCBiZWVuXG4gICAgICAgKiBleGNlZWRlZCwgc2NoZWR1bGUgYGNhbGxiYWNrYCB0byBleGVjdXRlIGBkZWxheWAgbXMgYWZ0ZXIgbW9zdFxuICAgICAgICogcmVjZW50IGV4ZWN1dGlvbi5cbiAgICAgICAqXG4gICAgICAgKiBJZiBgZGVib3VuY2VNb2RlYCBpcyB0cnVlIChhdCBiZWdpbiksIHNjaGVkdWxlIGBjbGVhcmAgdG8gZXhlY3V0ZVxuICAgICAgICogYWZ0ZXIgYGRlbGF5YCBtcy5cbiAgICAgICAqXG4gICAgICAgKiBJZiBgZGVib3VuY2VNb2RlYCBpcyBmYWxzZSAoYXQgZW5kKSwgc2NoZWR1bGUgYGNhbGxiYWNrYCB0b1xuICAgICAgICogZXhlY3V0ZSBhZnRlciBgZGVsYXlgIG1zLlxuICAgICAgICovXG4gICAgICB0aW1lb3V0SUQgPSBzZXRUaW1lb3V0KGRlYm91bmNlTW9kZSA/IGNsZWFyIDogZXhlYywgZGVib3VuY2VNb2RlID09PSB1bmRlZmluZWQgPyBkZWxheSAtIGVsYXBzZWQgOiBkZWxheSk7XG4gICAgfVxuICB9XG5cbiAgd3JhcHBlci5jYW5jZWwgPSBjYW5jZWw7XG5cbiAgLy8gUmV0dXJuIHRoZSB3cmFwcGVyIGZ1bmN0aW9uLlxuICByZXR1cm4gd3JhcHBlcjtcbn1cbiJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN2SkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDUEE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTs7Ozs7Ozs7Ozs7O0FDSEE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBOzs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakJBOzs7Ozs7Ozs7Ozs7QUNIQTtBQUFBO0FBQUE7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFyREE7Ozs7Ozs7Ozs7O0FDSkE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDdEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUFBO0FBSUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWxEQTs7Ozs7Ozs7Ozs7O0FDTEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNSQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFuREE7Ozs7Ozs7Ozs7OztBQ0hBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQVBBO0FBUEE7Ozs7Ozs7Ozs7OztBQ0pBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBRUE7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBekRBOzs7Ozs7Ozs7Ozs7QUNGQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFiQTtBQWtCQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFEQTtBQWpDQTtBQXNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFWQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBSUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBYkE7QUFrQkE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBRkE7QUFMQTtBQVVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBS0E7QUFLQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBYkE7QUFpQkE7QUFDQTtBQUNBO0FBRkE7QUF0QkE7QUEyQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBRkE7QUFMQTtBQVVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUdBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5TUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEvREE7QUFrRUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7O0FDeEVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQU1BO0FBQ0E7QUFEQTtBQXBCQTtBQXlCQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ3RDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdEJBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdEZBO0FBQUE7QUFBQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2ZBO0FBQUE7QUFBQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2JBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBckJBO0FBdUJBO0FBQ0E7QUFDQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFyTUE7QUFDQTtBQXdNQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFDQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbEZBO0FBcUZBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQW1CQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUUE7QUFSQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFNQTtBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQUE7QUFFQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUtBO0FBTEE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQU9BO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSEE7QUFLQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBa0JBO0FBbnhCQTtBQXV4QkE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFSQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFOQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFlQTtBQUNBO0FBaEJBO0FBQUE7QUFxQkE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUE1QkE7QUE4QkE7QUFDQTtBQTdOQTtBQTVsQ0E7QUErekNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3huREE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkE7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0EiLCJzb3VyY2VSb290IjoiIn0=